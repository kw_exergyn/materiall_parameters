
June23 - ran initial optimization using:
	X4OptParams.xls
	.\Data\inputData.mat - this used test data from TRP57 Isothermal at 1100MPa
Optimization was interrupted but best case seemed reasonable and was used. 
	Optimization data stored in ".\materiall_parameters\SMA_X4_Model\Optimisation_Data\Opt_Data_2023-06-13_14-20.mat"
	Parameters stored in ".\x4_01\System Parameters\NiTiParameters_v1x0.xlsx" as "X4_optJun23"
	

July23 - revisited optimization:
	X4OptParams_Jul23.xls - adjusted some parameter boundaries based on results from previous optimization
	.\Data\runCompanionModel_TRP57_1000MPa.mat - this used test data from TRP57 Isothermal at 1000MPa
	Optimization data stored in ".\materiall_parameters\SMA_X4_Model\Optimisation_Data\Optimisation_2023-07-25_09-56\Opt_Data_2023-07-25_09-56.mat"
	Showed similar results as before.
	Parameters for use in model saved to "C:\Users\DarraghClabby\localRepos\x4_01\System Parameters\NiTiParameters_v1x1.xlsx"

Aug23 - last optimization didn't show much improvement & model still underestimated test data. 
	thought that a lot of the parameters were obsolete relative to latest SMA model
	... but actually not the case. They were still used to update model parameters using:
		C:\Users\DarraghClabby\localRepos\x4_01\Materials\X4_NiTi_Param_04.m
	The only obsolete parameters were the ones for minor loops (y, C_) so remove these.
	Other parameters used were fine. 
	Results:
		- C:\Users\DarraghClabby\localRepos\materiall_parameters\SMA_X4_Model\Optimisation_Data\Optimisation_2023-08-17_19-03
			10000 trials completed but cost never got below ~38. Mainly due to heat. 
			However, time traces of strain & temperature look pretty good. 
			Note: noise on simulated heat is due to low resolution (0.1degC) of inlet temperature measurement (propagates to heat via deltaT)
		- Adjust limits on CA1 & EA1 since results from previous suggests optimum may below previously set limits
		  Also reintroduced shape parameters (N1 to N4) and implemented filtering on TT101 to TT104 (same as already implemented for TT1x0, TT1x1)
		  Still no major improvement, saved to : Optimisation_2023-08-18_12-19

Sep23 - restrict cost calculation by screening heat based on strain velocity. 
	based on params from previous optimization:
		- M.Hcur: reduce limits from (0.002, 0.02) to (0.001, 0.01)
		- M.MS: increase limits from (200, 400) to (300, 500)
		- M.CA1: increase limits from (5, 10)*1e6 to (8, 12)*1e6
		- M.EA1: increase limits from (3, 6)*1e10 to (5, 8)*1e10
		- R.A2S: reduce limits from (150, 290) to (100, 250)
		- R.EA2: increase limits from (4, 7)*1e10 to (5, 8)*1e10
		- M.N1: increase limits from (0.05, 0.4) to (0.25, 0.5)... increase further: (0.3, 0.6)
		- M.N2: reduce limits from (0.1, 0.4) to (0.01, 0.2)
		- R.N2: reduce limits from (0.1, 0.6) to (0.01, 0.2)
	Optimisation_2023-09-08_16-23: Ran for above alterations to limits (X4OptParams_Sep23), without srain screening, and for only one cycle (since needed to run locally). 
	Results look ok but maybe could be improved: increase M.N1 range & implement strain screen
	
	After doing a load of analysis on isothermals from CDR02 it looks like thermocouples are rubbish. 
	Tests to this point calculated latent heat using sensorType = 'C', i.e. TCs with bias correction relative to PT100
	Ditch this and use PT100 only. Also, use only stack 1 since it experiences constant inlet temperature and therefore more reliable
	Retain previous arameter limits, except: 
		- M.MS: revert back to (200, 400)
		- M.N1: further increase (0.4, 0.7)
	results: Optimisation_2023-09-25_08-15
	... looks rubbish!