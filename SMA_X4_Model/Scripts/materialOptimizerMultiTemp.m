clearvars
close all
proj = matlab.project.rootProject;
addpath(genpath('C:\Users\DarraghClabby\localRepos\x4_01'));

rootPath = [char(proj.RootFolder) filesep 'SMA_X4_Model' filesep 'Optimisation_Data' filesep 'OptTmp' filesep];
optFile = 'X4OptParams.xls';
costWeight = [1 1 1]/3;% [strain heat slh]
% screenParams.on = true; screenParams.pctThr = 0.05; screenParams.timeWin = 24;
crashRestart =  boolean(exist([rootPath 'checkfile.mat'], 'file'));

inputData.rootPath = 'C:\Users\DarraghClabby\localRepos\x4_01\simData\isothermalSims\TRP-121_X4Isothermals\';
inputData.subFolders = {'C2T15' 'C2T25' 'C2T35'};%{'C2T25'};%

screenParams.heat.pctThr = 0.05; 
screenParams.strain.pctThrVel = 0.05; 
screenParams.timeWin = 6; % 12;%
screenParams.timeLims = 150;
screenParams.strainZeroInterval = [145 155];

%% Load Data
for subFolder = inputData.subFolders
    load([inputData.rootPath subFolder{1} filesep subFolder{1} '.mat'])
    inputData.(subFolder{1}).Inputs = Inputs;
    inputData.(subFolder{1}).P = P;

    inputData.(subFolder{1}).P.NiTi.RPhase = 0;
end

%% Set Opt Params
Opt = readOptParams([rootPath optFile], P, {});
Opt.Folder = [char(proj.RootFolder) Opt.Folder];
if ~strcmp(Opt.Folder(end), filesep)
    Opt.Folder = [Opt.Folder filesep];
end
Opt.intcon = [];

%% Load Model
mdlName = P.Sim.model;
load_system(mdlName);

%% Prepare Data (OptTmp renamed later based on startTime)
startTime = datestr(now,'YYYY-mm-DD_HH-MM');

%% Set up Parallel pool
% c = parpool('Processes');
c = parpool('cluster R2023a');
files = dependencies.fileDependencyAnalysis(mdlName);
c.addAttachedFiles(files);


%% Cost function Handle
costFcnHandle = @(x) costFunMultiTemp(mdlName, Opt, inputData, x, costWeight, screenParams);%, timeScreen);

%% Run Optimization
crashFile = [Opt.Folder 'OptTmp' filesep 'checkfile.mat'];
opts = optimoptions( 'surrogateopt', 'Display', 'iter', ...
                        'MinSurrogatePoints', Opt.minRandom, ...
                        'MaxFunctionEvaluations', Opt.MaxEvals, ...
                        'PlotFcn', 'surrogateoptplot', ...
                        'UseParallel', true, ...
                        'MinSampleDistance', Opt.MinSampleDistance, ...
                        'ObjectiveLimit', Opt.ObjTarget, ...
                        'CheckpointFile', crashFile);%, ...
                        %'InitialPoints', Opt.InitArray );
if crashRestart
    [paramsOpt,fval,exitflag,output,trials] = surrogateopt(crashFile, opts);
else
    [paramsOpt,fval,exitflag,output,trials] = surrogateopt(costFcnHandle, Opt.lb, Opt.ub, Opt.intcon, Opt.A, Opt.b, Opt.Aeq, Opt.beq, opts);
end

%% Save Results
save([Opt.Folder 'OptTmp' filesep 'optData.mat'],'Opt','paramsOpt','fval','exitflag','output','trials', 'inputData', 'screenParams')

%% Plot Results
Plot_Optimisation_dscatter
saveas(gcf, [Opt.Folder 'OptTmp' filesep 'params.fig'])
saveas(gcf, [Opt.Folder 'OptTmp' filesep 'params.png'])

% Move from OptTmp
if ~exist([ char(proj.RootFolder) filesep 'SMA_X4_Model' filesep 'Optimisation_Data' filesep 'Optimisation_' startTime])
    Opt.Folder = [Opt.Folder 'Optimisation_' startTime];
    movefile([char(proj.RootFolder) filesep 'SMA_X4_Model' filesep 'Optimisation_Data' filesep 'OptTmp'], Opt.Folder)

    [~, fTmp, eTmp] = fileparts([Opt.Folder filesep optFile]);
    movefile([Opt.Folder filesep optFile], [Opt.Folder filesep fTmp '_' startTime eTmp])
end
