
clearvars
close all

optFailed = true;
while optFailed
    try         
        disp(['Running materialOptimizerMultiTemp'])
        delete(gcp('nocreate'))
        materialOptimizerMultiTemp
        optFailed = false;
    catch
        optFailed = true;
    end
end
