close all
SetGPStyle(1);
subplot = @(m,n,p) subtightplotGP(m, n, p);

clear h
h = gobjects(0);
a = gobjects(0);
b = gobjects(0);

ncol = round(sqrt(Opt.nD));
nrow = ceil(sqrt(Opt.nD));


[cost, idx] = min(trials.Fval);

h(end+1) = figure('name','Optimisation Results','NumberTitle','off','units','normalized','outerposition',[0 0.02 1 0.98]);
hold on
for i = 1:Opt.nD
    subplot(ncol,nrow, i)
    hold on
%     plot(trials.X(:,i)*Opt.Scale(i),trials.Fval,'o')
    dscatter(trials.X(:,i)*Opt.Scale(i),trials.Fval)
    scatter(trials.X(idx,i)*Opt.Scale(i),trials.Fval(idx),'o','filled')
    set(gca, 'YScale', 'log')
    title(Opt.DimNames{i},'interpreter','none')
    xlabel(Opt.Units{i})
    ylabel('Cost')
%     ylim([0.03 1.0])
end

%% Display Optimal Parameters
disp('_______________________')
disp('Optimal Parameters')
disp('-----------------------')
for i = 1:Opt.nD
    disp([Opt.DimNames{i} char(9) '=' char(9) num2str(paramsOpt(i)*Opt.Scale(i)) '; % [' Opt.Units{i} ']'])
end
disp('_______________________')

disp('_______________________')
disp('Optimal Parameters for Initial Conditions')
disp('-----------------------')
disp(['Opt.InitArray(1) =' char(9) num2str(paramsOpt(1)*Opt.Scale(1)) '; % [' Opt.Units{1} ']'])
for i = 2:Opt.nD
    disp(['Opt.InitArray(end+1) =' char(9) num2str(paramsOpt(i)*Opt.Scale(i)) '; % [' Opt.Units{i} ']'])
end
disp('_______________________')

disp('_______________________')
disp('Optimal Parameters for Excel')
disp('-----------------------')
array = [paramsOpt(1)*Opt.Scale(1)];
strings = [Opt.DimNames{1}];
for i = 2:Opt.nD
    strings = [strings char(9) Opt.DimNames{i}];
    array = [array paramsOpt(i)*Opt.Scale(i)];
end
disp(strings)
disp(num2str(array))
disp('_______________________')

% SaveFigures(1,h)