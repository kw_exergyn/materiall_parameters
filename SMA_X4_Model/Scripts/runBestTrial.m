
clearvars
close all
% cd('C:\Users\DarraghClabby\localRepos\materiall_parameters\SMA_X4_Model\Optimisation_Data\2023')
proj = matlab.project.rootProject;

% filePath = char([proj.RootFolder + "\SMA_X4_Model\Optimisation_Data\OptTmp\"]);
filePath = char([proj.RootFolder + "\SMA_X4_Model\Optimisation_Data\Opt_24-02Feb\Optimisation_2024-02-09_08-39\"]);
% inputData = 'inputDataTRP131C2_1380to1520_CoreParams.mat'; %'testDataTRP125C2_1880to2360.mat';%
optData = 'optData.mat';
paramsSheet = 'X4OptParams_~.xls';
costType = 'both'; % 'strain'; % 'heat'; 
strainScreen.on = true; strainScreen.pctThr = 0.05; strainScreen.timeWin = 6;

savePlot = true;
saveParamsXls = true;

addpath(genpath('C:\Users\DarraghClabby\localRepos\x4_01'));

if exist('inputData', 'var')
    load([filePath inputData], 'P')
end
load([filePath optData])
mdlName = P.Sim.model; %P.Comp.model; %'CM06_Stack';

% P = optNiTiParams(P, Opt, stats);
P = optNiTiParams(P, Opt, trials);

% scaleFactor = 1.6;
% P.NiTi.M.CA1 = scaleFactor*P.NiTi.M.CA1;
% % P.NiTi.R.CA1 = scaleFactor*P.NiTi.M.CA1;
P = X4_NiTi_Param_04(P, false);

optVals = [];
for iDim = 1:length(Opt.DimNames)
    eval(['tmp = ' Opt.DimNames{iDim} ';']);
    optVals = [optVals; tmp];
end

% Run Model
try
    out = sim(mdlName);
catch
    cd([proj.RootFolder + "\SMA_X4_Model\Optimisation_Data\2023"])
    out = sim(mdlName);
    cd(proj.RootFolder)
end

% Calculate Cost
% cost = calcCost(out, Inputs, Opt, P);
cost = calcCost(out, Inputs, P, costType, strainScreen);
if savePlot
    saveas(gcf, [filePath 'strainTempTrace.fig'])
    saveas(gcf, [filePath 'strainTempTrace.png'])
end

% Latent Heat
% energyOut.test = timeseries(cumsum(Inputs.heat.avg.Data).*(P.Sim.tStep/(0.5*P.Core.SMA.Mass)), Inputs.heat.avg.Time);
energyOut.test = timeseries(cumsum(Inputs.heat.stack1.Data).*(P.Sim.tStep/(0.5*P.Core.SMA.Mass)), Inputs.heat.stack1.Time);

out.FDS.sensors.TT102_shift = ShiftTempMeasurement(-P.Core.Stack.FluidVol, out.FDS.sensors.mDot, out.FDS.sensors.TT102, 0);
energyOut.sim = (out.FDS.sensors.mDot)*(P.MDb.Water.Cp/1000)*(out.FDS.sensors.TT102_shift - out.FDS.sensors.TT101);
energyOut.sim.Data = cumsum(energyOut.sim.Data).*(P.Sim.tStep/(0.5*P.Core.SMA.Mass));

figure();
hold on
plot(energyOut.test, 'displayname', 'Test')
plot(energyOut.sim, 'displayname', 'Sim')
xlabel('time [s]')
ylabel('Specific Latent Heat [kJ/kg]')
title({['Input Data: ' inputData]; ['Opt Data: ' optData]}, 'Interpreter', 'none')
grid on
legend() 

% [~, fTmp, ~] = fileparts(inputData);
% fNames = {fTmp};
% [~, fTmp, ~] = fileparts(optData);
% fNames = [fNames; fTmp];
% title({['Input Data: ' fNames{1}]; ['Opt Data: ' fNames{2}]}, 'Interpreter', 'none')

if savePlot
    saveas(gcf, [filePath 'latentHeatTrace.fig'])
    saveas(gcf, [filePath 'latentHeatTrace.png'])
end

if saveParamsXls
    tag = split(filePath, '\');
    iTag = 0; while isempty(tag{end-iTag}); iTag = iTag + 1; end
    paramsSheet = strrep(paramsSheet, '~', tag{end-iTag}(end-15:end));

    tmp = readtable([filePath paramsSheet], 'FileType', 'spreadsheet', 'sheet', 'Params01');
    if ismember('Best', tmp.Properties.VariableNames)
        warning(['Column named "Best" already exists in ' paramsSheet '. Current "Best" parameters will therefore not be saved. To save them, first delete the column "Best" from ' paramsSheet])
    else
        t = table(optVals, 'VariableNames', {'Best'});
        col = char('A' + length(tmp.Properties.VariableNames));
        writetable(t, [filePath paramsSheet], 'FileType', 'spreadsheet', 'sheet', 'Params01', 'Range', [col '1']);
    end
end


