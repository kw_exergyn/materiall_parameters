% ref: C:\Users\DarraghClabby\localRepos\x4_01\Subsystems\+SimscapeCustomBlocks\+SMA\SMA_GP03.ssc

%% Material Strain (Decomposition of strains)
% Disp/Plength == Sigma*S + Alpha*(Temp - T_0) + (sign(Sigma)*DispTM);
Disp = Plength*(Sigma*S + Alpha*(Temp - T_0) + (sign(Sigma)*DispTM));

% Disp      displacement (variable)
% Plength   stack length (constant)
% Sigma     stress = f/CSA, where f is variable, CSA is constant
% S         mixed compliance (inverse of Young's modulus) 
%           S = SA + Marvol*(SM - SA). SA, SM constant, Marvol variable
% Alpha     thermal expansion coefficient
% Temp      material temperature (variable)
% T_0       reference temperature
% DispTM    Martensitic trans strain DispTM = Hcur*Marvol

% neglect thermal expansion & expand S & DispTM
Disp = Plength*(Sigma*(SA + Marvol*(SM - SA)) + (sign(Sigma)*Hcur*Marvol));

% rewrite in terms of Marvol
Marvol = ((Disp/Plength) - Sigma*SA)/(Sigma*(SM - SA) + sign(Sigma)*Hcur);


%% Thermal Power Absorbed/Rejected
% Q == Smass*((Temp*Alpha*Dsigma + DMarvol*(MBound + Temp*Sigma*(ExpanM - ExpanA) + Dens*MDent*Temp))/Dens)*Qscale;
Q = Smass*((Temp*Alpha*Dsigma + DMarvol*(MBound + Temp*Sigma*(ExpanM - ExpanA) + Dens*MDent*Temp))/Dens)*Qscale;

% neglect thermal expansion & set Qscale = 1
Q = Smass*((DMarvol*(MBound + Dens*MDent*Temp))/Dens);

% Smass     stack mass (constant)
% DMarvol   time derivative of martensitic volume (variable)
% Dens      SMA density (constant)
% MBound    MYhys*(-(max(0, TransDirc))); 
%               MYhys = MYo *(1- Mk*exp(-(My/(MCn + (MCn == 0)))*abs(MarR)))
%                   MYo     Initial Hystersis Energy (constant) see: M.Y0 in C:\Users\DarraghClabby\localRepos\x4_01\Materials\X4_NiTi_Param_06.m
%                   Mk      Minor loop active (1) or disabled (0)
%                   for minor loop disabled: MYhys = MYo
%               TransDirc   transformation direction (mode chart). Values
%               TransDirc = (-1, 1), so that -(max(0, TransDirc)) = (0, -1)
% MDent     Phase Entropy Change (constant) see Delta_entr in C:\Users\DarraghClabby\localRepos\x4_01\Materials\X4_NiTi_Param_06.m

% MYo = M.Y0 = (0.5*M.DeltaEntDensity*((M.MS -  M.AF))- M.A3) 
%            = (0.5*M.Delta_entr*Dens*((M.MS -  M.AF))- M.A3)
%   Delta_entr = -(2*CA*CM*(Hcur + (CalS*Delta_S)))/(Dens*(CA+CM));
%       CalS    constant, 1Pa ???
%       Delta_S difference between martensite & austenite compliance: SM - SA
%       Delta_entr = -2*CA*CM*(Hcur + SM - SA)/(Dens*(CA+CM));
%   A3

MYo = ((MS -  AF)*(-CA*CM*(Hcur + (SM - SA)))/(CA+CM)) - A3;

A3 =    (-0.25*(Delta_entr*Dens*(MF - MS)))*(1+(1/(N1+1))-(1/(N2+1))) + ...
        ( 0.25*(Delta_entr*Dens*(AS - AF)))*(1+(1/(N3+1))-(1/(N4+1)))
    =   0.25*Delta_entr*Dens*( (MS - MF)*(1+(1/(N1+1))-(1/(N2+1))) + (AS - AF)*(1+(1/(N3+1))-(1/(N4+1))) )
