

simIn(1:10, 1) = Simulink.SimulationInput(mdlName);
for iSim = 1:10
    for iParam = 1:length(trials.X(:, 1))
        eval([Opt.DimNames{iParam} ' = ' num2str(trials.X(iParam, 1)*Opt.Scale(iParam)) ';'])        
        P = X4_NiTi_Param_04(P, false);
        simIn(iSim, 1) = simIn(iSim, 1).setVariable('P',P);
    end
end

c = parpool('cluster R2023a');
files = dependencies.fileDependencyAnalysis(mdlName);
c.addAttachedFiles(files);

simOut = parsim(simIn, 'TransferBaseWorkspaceVariables', 'on','ShowSimulationManager','on');
% simOut = parsim(simIn,'ShowSimulationManager','on');