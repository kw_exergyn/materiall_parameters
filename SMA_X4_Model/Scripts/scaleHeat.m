
clearvars
close all
% cd('C:\Users\DarraghClabby\localRepos\materiall_parameters\SMA_X4_Model\Optimisation_Data\2023')
proj = matlab.project.rootProject;

filePath = char([proj.RootFolder + "\SMA_X4_Model\Optimisation_Data\Optimisation_2023-11-29_13-38\"]);
inputData = 'inputDataTRP131C2_1380to1520_CoreParams.mat'; %'testDataTRP125C2_1880to2360.mat';%
optData = 'optData.mat';
% paramsSheet = 'X4OptParams_~.xls';
% costType = 'both'; % 'strain'; % 'heat'; 
% strainScreen.on = true; strainScreen.pctThr = 0.05; strainScreen.timeWin = 6;

% savePlot = true;
% saveParamsXls = true;

addpath(genpath('C:\Users\DarraghClabby\localRepos\x4_01'));

load([filePath inputData], 'P')
load([filePath optData])
mdlName = P.Sim.model; %P.Comp.model; %'CM06_Stack';

P = optNiTiParams(P, Opt, trials);


CA1_base = P.NiTi.M.CA1;
CA2_base = P.NiTi.R.CA2;

figure();
scaleFactor = [0.8:0.05:1.2];
for iScale = 1:length(scaleFactor)
    disp(['Scale Factor ' num2str(iScale) ' of ' num2str(length(scaleFactor)) ])
    P.NiTi.M.CA1 = scaleFactor(iScale)*CA1_base;
    P.NiTi.R.CA1 = scaleFactor(iScale)*CA2_base;
    P = X4_NiTi_Param_04(P, false);
    out = sim(mdlName);

    out.FDS.sensors.TT102_shift = ShiftTempMeasurement(-P.Core.Stack.FluidVol, out.FDS.sensors.mDot, out.FDS.sensors.TT102, 0);
    energyOut.sim = (out.FDS.sensors.mDot)*(P.MDb.Water.Cp/1000)*(out.FDS.sensors.TT102_shift - out.FDS.sensors.TT101);
    energyOut.sim.Data = cumsum(energyOut.sim.Data).*(P.Sim.tStep/(0.5*P.Core.SMA.Mass));
    [maxHeat(iScale), iMax] = max(energyOut.sim.Data);

    simStrain = (out.Hyd.sensors.Disp*-1000).*(100/mean(Inputs.refLength, 'all'));

    subplot(1, 3, 1)
    hold on
    plot(energyOut.sim, 'displayname', ['CA*' num2str(scaleFactor(iScale))])
    grid on
    legend() 
    xlabel('time [s]')
    ylabel('Specific Latent Heat [kJ/kg]')

    subplot(1, 3, 2)
    hold on
    plot(simStrain)
    grid on
    xlabel('time [s]')
    ylabel('Strain [%]')
end

p = polyfit(maxHeat, scaleFactor, 2);
px = [8:0.1:12];

subplot(1, 3, 3)
hold on
plot(maxHeat, scaleFactor, 'marker', 'o', 'LineStyle', 'none')
plot(px, polyval(p, px))
grid on
ylabel('CA Scale Factor')
xlabel('Specific Heat Rejected [kJ/kg]')


% title({['Input Data: ' inputData]; ['Opt Data: ' optData]}, 'Interpreter', 'none')
