
clearvars
close all
proj = matlab.project.rootProject;

% inputData = 'C:\Users\DarraghClabby\localRepos\materiall_parameters\SMA_X4_Model\Data\inputData.mat';
% optData = 'C:\Users\DarraghClabby\localRepos\materiall_parameters\SMA_X4_Model\Optimisation_Data\Optimisation_2023-06-09_13-53.mat';
% optData = 'C:\Users\DarraghClabby\localRepos\materiall_parameters\SMA_X4_Model\Optimisation_Data\Opt_Data_2023-06-12_17-06.mat';
% optData = 'C:\Users\DarraghClabby\localRepos\materiall_parameters\SMA_X4_Model\Optimisation_Data\Opt_Data_2023-06-13_14-20.mat';

% inputData = 'C:\Users\DarraghClabby\localRepos\materiall_parameters\SMA_X4_Model\Data\runCompanionModel_TRP57_1000MPa_Jul23.mat';
% optData = 'C:\Users\DarraghClabby\localRepos\materiall_parameters\SMA_X4_Model\Optimisation_Data\Opt_Data_2023-07-25_09-56.mat';

inputData = 'C:\Users\DarraghClabby\localRepos\materiall_parameters\SMA_X4_Model\Optimisation_Data\Optimisation_2023-08-18_12-19\inputData.mat';
optData = 'C:\Users\DarraghClabby\localRepos\materiall_parameters\SMA_X4_Model\Optimisation_Data\Optimisation_2023-08-18_12-19\optData.mat';

addpath(genpath('C:\Users\DarraghClabby\localRepos\x4_01'));

load(inputData, 'P')
load(optData)
mdlName = P.Comp.model; %'CM06_Stack';

% P = optNiTiParams(P, Opt, stats);


nD = length(Opt.DimNames);
scaleVec = [1:0.05:1.4];
costHeat = scaleVec*0;
costStrain = scaleVec*0;
for iScale = 1:length(scaleVec)
    P = optNiTiParams(P, Opt, trials);
    for i = 1:nD
        if strcmp(Opt.DimNames{i}, 'P.NiTi.M.CA1')
            eval([strrep(Opt.DimNames{i}, 'P.', 'tmp.') ' = ' Opt.DimNames{i} '*scaleVec(iScale);']);
        else
            eval([strrep(Opt.DimNames{i}, 'P.', 'tmp.') ' = ' Opt.DimNames{i} ';']);
        end
    end
    P.NiTi = tmp.NiTi;
    %     P.NiTi.MinorR = 0;
    P = X4_NiTi_Param_04(P, false);

% optVals = [];
% for iDim = 1:length(Opt.DimNames)
%     eval(['tmp = ' Opt.DimNames{iDim} ';']);
%     optVals = [optVals; tmp];
% end

    disp(['Running Simulatoion ' num2str(iScale) ' of ' num2str(length(scaleVec))])
    out = sim(mdlName);
    cost = calcCost(out, Inputs, Opt, P);
    costHeat(iScale) = cost.heat;
    costStrain(iScale) = cost.strain;
end

figure(); 
hold on
plot(scaleVec, [costHeat' costStrain'])

% energyOut.test = timeseries(cumsum(Inputs.heat.avg.Data).*(P.Sim.tStep/(0.5*P.Core.SMA.Mass)), Inputs.heat.avg.Time);
% 
% out.FDS.sensors.TT102_shift = ShiftTempMeasurement(-P.Core.Stack.FluidVol, out.FDS.sensors.mDot, out.FDS.sensors.TT102, 0);
% energyOut.sim = (out.FDS.sensors.mDot)*(P.MDb.Water.Cp/1000)*(out.FDS.sensors.TT102_shift - out.FDS.sensors.TT101);
% energyOut.sim.Data = cumsum(energyOut.sim.Data).*(P.Sim.tStep/(0.5*P.Core.SMA.Mass));
% 
% figure();
% hold on
% plot(energyOut.test, 'displayname', 'Test')
% plot(energyOut.sim, 'displayname', 'Sim')
% xlabel('time [s]')
% ylabel('Specific Latent Heat [kJ/kg]')
% grid on
% legend() 
% 
% [~, fTmp, ~] = fileparts(inputData);
% fNames = {fTmp};
% [~, fTmp, ~] = fileparts(optData);
% fNames = [fNames; fTmp];
% title({['Input Data: ' fNames{1}]; ['Opt Data: ' fNames{2}]}, 'Interpreter', 'none')


