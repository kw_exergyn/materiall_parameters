
clearvars; 
close all

filePath = 'C:\Users\DarraghClabby\localRepos\materiall_parameters\SMA_X4_Model\Optimisation_Data\Optimisation_2024-01-18_17-35\Qscale\';
fileNames = {'C2T151x035.mat' 'C2T251x035.mat' 'C2T351x035.mat'};
testNames = {'Temperature = 15^{\circ}C' 'Temperature = 25^{\circ}C' 'Temperature = 35^{\circ}C'};

nFiles = length(fileNames);
figure('Position', [-1800 400 1700 400])
for iFile = 1:nFiles
    load([filePath fileNames{iFile}]); 


    slh.meas = calcIsothermal(Inputs, out, P, screenParams, false);
    slh.sim = calcIsothermal(Inputs, out, P, screenParams, true);
    
    %% Measured
    measStrain = Inputs.strain.stack1;
    measOffset = measStrain.resample([screenParams.strainZeroInterval(1):median(diff(measStrain.Time)):screenParams.strainZeroInterval(2)]).mean;
    measStrain.Data = measStrain.Data - measOffset;
    measStress = Inputs.stress;
    
    %% Simulated
    simStrain = (out.Hyd.sensors.Disp*-1000).*(100/mean(Inputs.refLength, 'all'));
    simOffset = simStrain.resample([screenParams.strainZeroInterval(1):median(diff(simStrain.Time)):screenParams.strainZeroInterval(2)]).mean;
    simStrain.Data = simStrain.Data - simOffset;
    simStress = out.Stack1.MidPlates.Chan1.Stress./1e6;

    %% Plot
    ah(iFile) = subplot(1, nFiles, iFile);
    hold on; 
    plot(measStrain.Data, measStress.Data, 'displayname', 'test')
    plot(simStrain.Data, simStress.Data, 'displayname', 'sim')
    grid on
    xlabel('Strain [%]')
    ylabel('Stress [MPa]')
    title(testNames{iFile})
    text(0.5, 0.25, ...
        {'                            Test | Sim', ...
            ['Heating [kJ/kg]:   ', num2str(slh.meas.heatOut, '%.2f') ' | ' num2str(slh.sim.heatOut, '%.2f')], ...
            ['Cooling [kJ/kg]:   ', num2str(slh.meas.heatIn, '%.2f') ' | ' num2str(slh.sim.heatIn, '%.2f')], ...
            ['Work [kJ/kg]:       ', num2str(slh.meas.work, '%.2f') ' | ' num2str(slh.sim.work, '%.2f')]...
            ['COP:                 ', num2str(slh.meas.heatOut/slh.meas.work, '%.2f') ' | ' num2str(slh.sim.heatOut/slh.sim.work, '%.2f')]...
            ['EER:                 ', num2str(slh.meas.heatIn/slh.meas.work, '%.2f') ' | ' num2str(slh.sim.heatIn/slh.sim.work, '%.2f')]...
            },...
        'units', 'normalized', 'BackgroundColor', [1 1 1], 'LineStyle','-', 'EdgeColor', [0 0 0])
    if iFile == 1; legend('Location', 'northwest'); end

end

linkaxes(ah, 'xy')