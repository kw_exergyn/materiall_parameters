
clearvars
close all
proj = matlab.project.rootProject;

Qscale = 1.035; %[0.95 0.975 1:0.01:1.03 1.035 1.04 1.05 1.1];% [0.95:0.025:1.1];
savePlots = true;
costWeight = [0 1]; % [strain heat]

filePath = char([proj.RootFolder + "\SMA_X4_Model\Optimisation_Data\Optimisation_2024-01-18_17-35\"]);%char([proj.RootFolder + "\SMA_X4_Model\Optimisation_Data\OptTmp\"]); %
optData = 'optData.mat';
paramsSheet = 'X4OptParams_~.xls';

addpath(genpath('C:\Users\DarraghClabby\localRepos\x4_01'));

load([filePath optData])
fldNames = setdiff(fieldnames(inputData), {'rootPath', 'subFolders'});
P = inputData.(fldNames{1}).P;

mdlName = P.Sim.model; 
[P, optCost, optTrial] = optNiTiParams(P, Opt, trials);
eval(['P = ' P.NiTi.script '(P, false);']);

optVals = [];
for iDim = 1:length(Opt.DimNames)
    eval(['tmp = ' Opt.DimNames{iDim} ';']);
    optVals = [optVals; tmp];
end
Opt.DimNames = [Opt.DimNames; {'P.NiTi.M.Qscale'}];
Opt.Scale = [Opt.Scale; 1];

cost = Qscale*0;
% parfor iQ = 1:length(Qscale)
for iQ = 1:length(Qscale)
    % optVals = [optVals; Qscale(iQ)];
    
    disp(['Calculating cost for Qscale = ' num2str( Qscale(iQ)) ' (' num2str(iQ) ' of ' num2str(length(Qscale)) ')'])
    % cost(iQ) = costFunMultiTemp(mdlName, Opt, inputData, optVals./Opt.Scale, costWeight, screenParams);
    cost(iQ) = costFunMultiTemp(mdlName, Opt, inputData, [optVals; Qscale(iQ)]./Opt.Scale, costWeight, screenParams);
    
    for iFig = 1:length(findobj('type','figure'))    
        aTmp = get(figure(iFig),'children'); 
        aTmp(end).Title.String = fldNames{iFig};
        if savePlots
            saveas(gcf, [filePath 'Qscale\' fldNames{iFig} '_' strrep(num2str(Qscale(iQ)), '.', 'x') '.png']);
            saveas(gcf, [filePath 'Qscale\' fldNames{iFig} '_' strrep(num2str(Qscale(iQ)), '.', 'x') '.fig']);
        end
    end    
    close all

end

% figure()
% plot(Qscale, cost, 'Marker','o', 'LineStyle','-')
% grid on
% xlabel('Qscale')
% ylabel('Heat Cost')




