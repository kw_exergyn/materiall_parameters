
clearvars
close all
% cd('C:\Users\DarraghClabby\localRepos\materiall_parameters\SMA_X4_Model\Optimisation_Data\2023')
proj = matlab.project.rootProject;

filePath = char([proj.RootFolder + "\SMA_X4_Model\Optimisation_Data\Opt_24-02Feb\Optimisation_2024-02-09_08-39\"]);%char([proj.RootFolder + "\SMA_X4_Model\Optimisation_Data\OptTmp\"]); %
% inputData = 'inputDataTRP131C2_1380to1520_CoreParams.mat'; %'testDataTRP125C2_1880to2360.mat';%
optData = 'optData.mat';
paramsSheet = 'X4OptParams_~.xls';
costWeight = [1 1 1]/3; % [strain heat slh]

savePlots = true;
saveParamsXls = true;

addpath(genpath('C:\Users\DarraghClabby\localRepos\x4_01'));

load([filePath optData])
% screenParams.timeWin = 6;

fldNames = setdiff(fieldnames(inputData), {'rootPath', 'subFolders'});
P = inputData.(fldNames{1}).P;

mdlName = P.Sim.model; 

[P, optCost, optTrial] = optNiTiParams(P, Opt, trials);
eval(['P = ' P.NiTi.script '(P, false);']);

optVals = [];
for iDim = 1:length(Opt.DimNames)
    eval(['tmp = ' Opt.DimNames{iDim} ';']);
    optVals = [optVals; tmp];
end

cost = costFunMultiTemp(mdlName, Opt, inputData, optVals./Opt.Scale, costWeight, screenParams);%, strainScreen, timeScreen);

% add titles to figures
for iFig = 1:length(findobj('type','figure'))    
    aTmp = get(figure(iFig),'children'); 
    aTmp(end).Title.String = fldNames{iFig};
    if savePlots
        saveas(gcf, [filePath fldNames{iFig} '.png']);
        saveas(gcf, [filePath fldNames{iFig} '.fig']);
    end
end

% write opt values to spreadsheet
if saveParamsXls
    tag = split(filePath, '\');
    iTag = 0; while isempty(tag{end-iTag}); iTag = iTag + 1; end
    paramsSheet = strrep(paramsSheet, '~', tag{end-iTag}(end-15:end));
    
    tmp = readtable([filePath paramsSheet], 'FileType', 'spreadsheet', 'sheet', 'Params01');
    tmp.Value = optVals;%table(optVals, 'VariableNames', {'Value'});
    % writetable(tmp, [filePath paramsSheet], 'FileType', 'spreadsheet', 'sheet', 'Params01');

    iWrite = [char('A' + find(strcmp(tmp.Properties.VariableNames,{'Value'})) - 1) '2'];
    writematrix(tmp.Value, [filePath paramsSheet], 'FileType', 'spreadsheet', 'sheet', 'Params01', 'Range', iWrite);
end



