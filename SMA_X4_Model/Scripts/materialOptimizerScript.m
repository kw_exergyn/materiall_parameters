clearvars
close all
proj = matlab.project.rootProject;
addpath(genpath('C:\Users\DarraghClabby\localRepos\x4_01'));

% define the input files
% optFile = [ char(proj.RootFolder) filesep 'SMA_X4_Model' filesep 'X4OptParams.xls'];
% inputData = [ char(proj.RootFolder) filesep 'SMA_X4_Model' filesep 'Data' filesep 'inputData.mat'];

% optFile = [ char(proj.RootFolder) filesep 'SMA_X4_Model' filesep 'X4OptParams_Jul23.xls'];
% inputData = [ char(proj.RootFolder) filesep 'SMA_X4_Model' filesep 'Data' filesep 'runCompanionModel_TRP57_1000MPa.mat'];

% optFile = [ char(proj.RootFolder) filesep 'SMA_X4_Model' filesep 'X4OptParams_Aug23.xls'];
% % inputData = [ char(proj.RootFolder) filesep 'SMA_X4_Model' filesep 'Data' filesep 'runCompanionModel_TRP57_1000MPa_Aug23.mat'];
% inputData = [ char(proj.RootFolder) filesep 'SMA_X4_Model' filesep 'Optimisation_Data' filesep 'OptTmp' filesep 'inputData.mat'];

% optFile = [ char(proj.RootFolder) filesep 'SMA_X4_Model' filesep 'X4OptParams_Sep23.xls'];
% inputData = [ char(proj.RootFolder) filesep 'SMA_X4_Model' filesep 'Optimisation_Data' filesep 'OptTmp' filesep 'inputData_160sPT100.mat'];

% optFile = [ char(proj.RootFolder) filesep 'SMA_X4_Model' filesep 'X4OptParams_Sep23.xls'];
% inputData = [ char(proj.RootFolder) filesep 'SMA_X4_Model' filesep 'Optimisation_Data' filesep 'OptTmp' filesep 'inputDataTRP131C2.mat'];
% tOffset = [1 5]; % interval overwhich bias between temperature signals will be calculated [start end]

rootPath = [char(proj.RootFolder) filesep 'SMA_X4_Model' filesep 'Optimisation_Data' filesep 'OptTmp' filesep];
optFile = 'X4OptParams.xls';
inputData = 'inputDataTRP131C2_1380to1520_CoreParams.mat';
tOffset = [1 5]; % interval overwhich bias between temperature signals will be calculated [start end]
costType = 'both'; % 'strain'; % 'heat'; 
strainScreen.on = true; strainScreen.pctThr = 0.05; strainScreen.timeWin = 6;

%% Load Data
load([rootPath inputData], 'P', 'Inputs')%, 'coresToRun')

%% Set Opt Params
Opt = readOptParams([rootPath optFile], P, {});%{'General01' 'Params01' 'Ineq'});
% Opt.dt = P.Sim.tStep;
Opt.Folder = [char(proj.RootFolder) Opt.Folder];
Opt.intcon = [];

% Opt.Aeq = [];
% Opt.beq = [];
% 
% Opt.nCons = 10;
% Opt.A = zeros(Opt.nCons, Opt.nD);
% Opt.b = zeros(Opt.nCons, 1);
% Opt.A(1, 3) = 1;    Opt.A(1, 5) = -1;   % 1: MS <= A1F
% Opt.A(2, 2) = 1;    Opt.A(2, 4) = -1;   % 2: MF <= A1S
% Opt.A(3, 2) = 1;    Opt.A(3, 5) = -1;   % 3: MF <= A1F
% Opt.A(4, 11) = 1;   Opt.A(4, 13) = -1;  % 4: RS <= A2F
% Opt.A(5, 10) = 1;   Opt.A(5, 12) = -1;  % 5: RF <= A2S
% Opt.A(6, 10) = 1;   Opt.A(6, 13) = -1;  % 6: RF <= A2F
% 
% c = 0.4;
% Opt.A(7, 7) = 1;    Opt.A(7, 15) = -(1+c);  % 7: EA1 <= (1+c)EA2
% Opt.A(8, 15) = 1-c; Opt.A(8, 7) = -1;       % 8: (1-c)EA2 <= EA1
% Opt.A(9, 7) = 1;    Opt.A(9, 8) = -(1+c);   % 9: EA1 <= (1+c)EM
% Opt.A(10, 8) = 1-c; Opt.A(10, 7) = -1;      % 10: (1-c)EM <= EA1
% 
% Opt.A(11, 1) = -1;   Opt.A(11, 9) = 1;        % 9: R.Hcur <= M.Hcur

% Opt.A = [];
% Opt.b = [];

%% Load Model
mdlName = P.Sim.model;%P.Comp.model;
load_system(mdlName);

%% Set FDS Initial Temperature
dt = median(diff(Inputs.TT101.Time));
offsetRange = round(tOffset/dt);%round([5 45]/dt);
nFilt = round(1/dt);

% set initial temperature to measurement from TT101 over initial interval (defined by offsetRange)
P.FDS.Init.Temp = mean(Inputs.TT101.Data(offsetRange(1):offsetRange(2)));

%% Calc Measured Data
% Strain [%]
for iStk = 1:2
    Inputs.strain.(['stack' num2str(iStk)]) = Inputs.(['Cr' num2str(P.Sim.coresToRun) 'Stck'  num2str(iStk) 'Strain']);
    % nSample = length(Inputs.strain.(['stack' num2str(iStk)]).Data);
    % strainOffset = prctile(Inputs.strain.(['stack' num2str(iStk)]).Data(round(nSample*0.5):end), 99);
    % strainOffset = mean(Inputs.strain.(['stack' num2str(iStk)]).Data(offsetRange(1):offsetRange(2)));
    % Inputs.strain.(['stack' num2str(iStk)]) = Inputs.strain.(['stack' num2str(iStk)]) - strainOffset;
    Inputs.strain.(['stack' num2str(iStk)]).Data(Inputs.strain.(['stack' num2str(iStk)]).Data > 0) = 0;
end
% Inputs.strain.stack1 = Inputs.(['Cr' num2str(coresToRun) 'Stck1Strain']);
% Inputs.strain.stack2 = Inputs.(['Cr' num2str(coresToRun) 'Stck2Strain']);
Inputs.strain.avg = 0.5*(Inputs.strain.stack1 + Inputs.strain.stack2);

% Heat [kW]
stackVol =  P.Core.Stack.FluidVol; % [L]
interStackVol = (P.FDS.Core1.StackConnector.Length*1000)*(pi/4)*(P.FDS.Core1.StackConnector.ID^2)*1e-6; % [L]
interSensorVols = [0 stackVol  interStackVol stackVol]; % [L]
for iSensor = 1:4
    Inputs.(['TT10' num2str(iSensor) '_shift']) = ShiftTempMeasurement(-sum(interSensorVols(1:iSensor)), Inputs.FT200, Inputs.(['TT10' num2str(iSensor)]), 0);
    if iSensor > 1
        tempOffset = mean(Inputs.(['TT10' num2str(iSensor) '_shift']).Data(offsetRange(1):offsetRange(2))  - ...
                      Inputs.TT101_shift.Data(offsetRange(1):offsetRange(2)));
        Inputs.(['TT10' num2str(iSensor) '_shift']) = Inputs.(['TT10' num2str(iSensor) '_shift']) - tempOffset;
    end
end
Inputs.heat.stack1 = (Inputs.FT200)*(P.MDb.Water.Cp/1000)*(Inputs.TT102_shift - Inputs.TT101_shift);
Inputs.heat.stack2 = (Inputs.FT200)*(P.MDb.Water.Cp/1000)*(Inputs.TT104_shift - Inputs.TT103_shift);
Inputs.heat.stack1.Data = conv(Inputs.heat.stack1.Data, ones(nFilt, 1)/nFilt, 'same');
Inputs.heat.stack2.Data = conv(Inputs.heat.stack2.Data, ones(nFilt, 1)/nFilt, 'same');
Inputs.heat.avg = 0.5*(Inputs.heat.stack1 + Inputs.heat.stack2);


%% Prepare temp Data store
startTime = datestr(now,'YYYY-mm-DD_HH-MM');
if ~exist([ char(proj.RootFolder) filesep 'SMA_X4_Model' filesep 'Optimisation_Data' filesep 'Optimisation_' startTime])
    Opt.Folder = [Opt.Folder 'Optimisation_' startTime];
    movefile([char(proj.RootFolder) filesep 'SMA_X4_Model' filesep 'Optimisation_Data' filesep 'OptTmp'], Opt.Folder)
    
    [~, fTmp, eTmp] = fileparts([Opt.Folder filesep optFile]);
    movefile([Opt.Folder filesep optFile], [Opt.Folder filesep fTmp '_' startTime eTmp])
end

% filename = ['Opt_Data_' startTime '.mat'];
% for i = 1:Opt.nD
%     fieldName = strrep(Opt.DimNames{i},'.','_');
%     stats.(fieldName) = Opt.InitArray(i);
% end
% stats.costStrain = NaN;
% stats.costHeat = NaN;
% stats.costTotal = NaN;
% save([Opt.Folder filesep filename],'stats','Opt','Inputs','-v7.3')


%% Set up Parallel pool
% c = parpool('Processes');
c = parpool('cluster R2023a');
files = dependencies.fileDependencyAnalysis(mdlName);
c.addAttachedFiles(files);

%% Cost function Handle
% costFcnHandle = @(x) costFun(P,filename, mdlName, Opt, Inputs, x);
% costFcnHandle = @(x) costFun(P, mdlName, Opt, Inputs, x, costType);
costFcnHandle = @(x) costFun(P, mdlName, Opt, Inputs, x, costType, strainScreen);

%% Run Optimization
try
    opts = optimoptions( 'surrogateopt', 'Display', 'iter', ...
                            'MinSurrogatePoints', Opt.minRandom, ...
                            'MaxFunctionEvaluations', Opt.MaxEvals, ...
                            'PlotFcn', 'surrogateoptplot', ...
                            'UseParallel', true, ...
                            'MinSampleDistance', Opt.MinSampleDistance, ...
                            'ObjectiveLimit', Opt.ObjTarget);%, ...
                            %'InitialPoints', Opt.InitArray );
    % [paramsOpt,fval,exitflag,output,trials] = surrogateopt( costFcnHandle, Opt.lb, Opt.ub, Opt.intcon, opts);
    [paramsOpt,fval,exitflag,output,trials] = surrogateopt( costFcnHandle, Opt.lb, Opt.ub, Opt.intcon, Opt.A, Opt.b, Opt.Aeq, Opt.beq, opts);
    % set_param(mdlName, 'FastRestart', 'on' );
catch MExc
    MExc.throwAsCaller();
    set_param( mdlName, 'FastRestart', 'off' );
end

%% Save Results
save([Opt.Folder filesep 'optData.mat'],'Opt','paramsOpt','fval','exitflag','output','trials', 'Inputs')

%% Plot Results
Plot_Optimisation_dscatter
saveas(gcf, [Opt.Folder filesep 'params.fig'])
saveas(gcf, [Opt.Folder filesep 'params.png'])
