% function cost = calcCost(out, Inputs, Opt, P, costType, strainScreen)
function cost = calcCost(out, Inputs, P, costWeight, screenParams)%strainScreen, timeScreen)
    
    if ~exist('costWeight', 'var')
        costWeight = [0.5 0.5];%'both';
    end

    % if ~exist('strainScreen', 'var')
    %     strainScreen.on = false;
    % end

    % if ~exist('timeScreen', 'var')
    %     timeScreen = 0;
    % end
    % % iTrim = round(10/P.Sim.tStep);
    
    %% Measured
    measStrain = Inputs.strain.stack1;
    measOffset = measStrain.resample([screenParams.strainZeroInterval(1):median(diff(measStrain.Time)):screenParams.strainZeroInterval(2)]).mean;
    measStrain.Data = measStrain.Data - measOffset;

    measHeat = Inputs.heat.stack1; 

    %% Simulated
    simStrain = (out.Hyd.sensors.Disp*-1000).*(100/mean(Inputs.refLength, 'all'));
    % simStrain = out.Stack1.MidPlates.Chan1.Strain.*100;
    simOffset = simStrain.resample([screenParams.strainZeroInterval(1):median(diff(simStrain.Time)):screenParams.strainZeroInterval(2)]).mean;
    simStrain.Data = simStrain.Data - simOffset;

    out.FDS.sensors.TT102_shift = ShiftTempMeasurement(-P.Core.Stack.FluidVol, out.FDS.sensors.mDot, out.FDS.sensors.TT102, 0);
    simHeat = out.FDS.sensors.mDot*(P.MDb.Water.Cp/1000)*(out.FDS.sensors.TT102_shift - out.FDS.sensors.TT101);
    
    %% Screen

    nWin = round(screenParams.timeWin/median(diff(measStrain.Time)));
    
    strainScreen = movmean([0; diff(measStrain.Data)], nWin);
    strainThr = screenParams.strain.pctThrVel*max(abs(strainScreen));
    strainScreen(abs(strainScreen) < strainThr) = 0;
    strainScreen(strainScreen ~= 0) = 1;
    strainScreen = movmean(strainScreen, nWin);
    strainScreen(strainScreen ~= 0) = 1;

    heatScreen = measHeat.Data;
    heatThr = screenParams.heat.pctThr*max(abs(heatScreen));
    heatScreen(abs(heatScreen) < heatThr) = 0;
    heatScreen(heatScreen ~= 0) = 1;
    heatScreen = movmean(heatScreen, nWin);
    heatScreen(heatScreen ~= 0) = 1;

    strainScreen(measStrain.Time < screenParams.timeLims(1)) = 0;
    heatScreen(measHeat.Time < screenParams.timeLims(1)) = 0;
    if length(screenParams.timeLims) > 1
        strainScreen(measStrain.Time > screenParams.timeLims(2)) = 0;
        heatScreen(measHeat.Time > screenParams.timeLims(2)) = 0;
    end

    %% Cost
    
    measSlh = Inputs.slh;
    slhTmp = calcIsothermal(Inputs, out, P, screenParams, true);
    simSlh.heat = slhTmp.heatOut;
    simSlh.cool = slhTmp.heatIn;
    simSlh.work = slhTmp.work; 
    clear slhTmp

    % strainError = rms((measStrain.Data(strainScreen>0) - simStrain.Data(strainScreen>0)), 'omitnan');
    % strainError = abs(measSlh.work - simSlh.work);

    % heatError = rms((measHeat.Data(heatScreen>0) - simHeat.Data(heatScreen>0)), 'omitnan'); % rms traces
    % heatError = abs(rms(measHeat.Data(heatScreen>0), 'omitnan') - rms(simHeat.Data(heatScreen>0), 'omitnan')); % rms total
    % heatError = 0.5*(abs((measSlh.heat - simSlh.heat)/measSlh.heat) + abs((measSlh.cool - simSlh.cool)/measSlh.cool)); % abs normalized error separated into heating & cooling
    % heatError = 0.5*(abs(measSlh.heat - simSlh.heat) + abs(measSlh.cool - simSlh.cool)); % abs error separated into heating & cooling
    % heatError = (abs(measSlh.heat - simSlh.heat) + abs(measSlh.cool - simSlh.cool) + abs(measSlh.work - simSlh.work))/3;

    strainError = rms((measStrain.Data(strainScreen>0) - simStrain.Data(strainScreen>0)), 'omitnan')/rms(measStrain.Data(strainScreen>0), 'omitnan');
    heatError = rms((measHeat.Data(heatScreen>0) - simHeat.Data(heatScreen>0)), 'omitnan')/rms(measHeat.Data(heatScreen>0), 'omitnan');
    % slhError = abs(simSlh.heat - (simSlh.cool + simSlh.work))/measSlh.heat;
    slhError = abs((measSlh.heat - measSlh.cool) - (simSlh.heat - simSlh.cool))/(measSlh.heat);

    cost.strain = 100*costWeight(1)*strainError;
    cost.heat = 100*costWeight(2)*heatError;
    cost.slh = 100*costWeight(3)*slhError;
    cost.total = cost.strain + cost.heat + cost.slh;

    % cost.strain = strainError;
    % cost.heat = heatError;
    % cost.total = cost.strain*cost.heat;

    %% Specific Latent Heat & Work
    % try
    %     slh.meas = calcIsothermal(Inputs, out, P, screenParams, false);
    %     slh.sim = calcIsothermal(Inputs, out, P, screenParams, true);
    % end

    %% Figures
    figure('Position', [700 100 550 750])
    ah(1) = subplot(3, 1, 1);%subplot(3, 2, 1:2);%subplot(4, 1, 1);
    hold on
    plot(measStrain.Time(strainScreen>0), measStrain.Data(strainScreen>0), 'DisplayName', 'Measured (Screened)', 'linestyle', 'none', 'marker', '.');
    plot(simStrain.Time(strainScreen>0), simStrain.Data(strainScreen>0), 'DisplayName', 'Simulated (Screened)', 'linestyle', 'none', 'marker', '.');
    plot(measStrain, 'DisplayName', 'Measured (Raw)', 'linestyle', ':');
    plot(simStrain, 'DisplayName', 'Simulated (Raw)', 'linestyle', ':');
    % plot(measStrain.Time(screenVec>0), measStrain.Data(screenVec>0), 'DisplayName', 'Measured');
    % plot(simStrain.Time(screenVec>0), simStrain.Data(screenVec>0), 'DisplayName', 'Simulated');
    grid on    
    subtitle({['Total Cost: ' num2str(cost.total)] ['Strain Cost: ' num2str(cost.strain)]})
    ylabel('Strain [%]')
    hold off
    legend('Location', 'northwest')

    ah(2) = subplot(3, 1, 2);%subplot(3, 2, 3:4);%subplot(4, 1, 2);
    hold on
    plot(measStrain.Time(heatScreen>0), measHeat.Data(heatScreen>0), 'DisplayName', 'Measured (Screened)', 'linestyle', 'none', 'marker', '.');
    plot(simStrain.Time(heatScreen>0), simHeat.Data(heatScreen>0), 'DisplayName', 'Simulated (Screened)', 'linestyle', 'none', 'marker', '.');
    plot(measHeat, 'DisplayName', 'Measured (Raw)', 'linestyle', ':');
    plot(simHeat, 'DisplayName', 'Simulated (Raw)', 'linestyle', ':');
    % plot(measHeat.Time(screenVec>0), measHeat.Data(screenVec>0), 'DisplayName', 'Measured');
    % plot(simHeat.Time(screenVec>0), simHeat.Data(screenVec>0), 'DisplayName', 'Simulated');
    grid on
    subtitle(['Heat Cost: ' num2str(cost.heat)])
    ylabel('Heat [kW]')
    hold off

    ah(3) = subplot(3, 1, 3);%subplot(3, 2, 5);%subplot(4, 1, 3);
    hold on;
    plot(measStrain.Data(strainScreen > 0), Inputs.stress.Data(strainScreen > 0))
    plot(simStrain.Data(strainScreen > 0), out.Stack1.MidPlates.Chan1.Stress.Data(strainScreen > 0)./1e6)
    % plot(measStrain.Data, -1*Inputs.Cr2Stress.Data)
    % plot(simStrain.Data, -1*Inputs.Cr2Stress.Data)
    grid on
    xlabel('Strain [%]')
    ylabel('Stress [MPa]')
    subtitle(['SLH Cost: ' num2str(cost.slh)])
    try
        text(0.025, 0.9, ...
            {'                            Test | Sim', ...
                ['Heating [kJ/kg]:   ', num2str(measSlh.heat, '%.2f') ' | ' num2str(simSlh.heat, '%.2f')], ...
                ['Cooling [kJ/kg]:   ', num2str(measSlh.cool, '%.2f') ' | ' num2str(simSlh.cool, '%.2f')], ...
                ['Net Heat [kJ/kg]:   ', num2str(measSlh.heat - measSlh.cool, '%.2f') ' | ' num2str(simSlh.heat - simSlh.cool, '%.2f')]...
                ['Work [kJ/kg]:       ', num2str(measSlh.work, '%.2f') ' | ' num2str(simSlh.work, '%.2f')]...
                ['COP:                 ', num2str(measSlh.heat/measSlh.work, '%.2f') ' | ' num2str(simSlh.heat/simSlh.work, '%.2f')]...
                ['EER:                 ', num2str(measSlh.cool/measSlh.work, '%.2f') ' | ' num2str(simSlh.cool/simSlh.work, '%.2f')]...
                },...
            'units', 'normalized', 'BackgroundColor', [1 1 1], 'LineStyle','-', 'EdgeColor', [0 0 0], 'FontSize',8)
        % annotation('textbox', [0.6, 0.15, 0.1, 0.1], 'String', ...
        %     {'                      Test|Sim', ...
        %     ['Heating Out:  ', num2str(slh.meas.heatOut, '%.2f') '|' num2str(slh.sim.heatOut, '%.2f')], ...
        %     ['Cooling Out:  ', num2str(slh.meas.heatIn, '%.2f') '|' num2str(slh.sim.heatIn, '%.2f')], ...
        %     ['Net Heat:     ', num2str(slh.meas.heatOut - slh.meas.heatIn, '%.2f') '|' num2str(slh.sim.heatOut - slh.sim.heatIn, '%.2f')], ...
        %     ['Work:         ', num2str(slh.meas.work, '%.2f') '|' num2str(slh.sim.work, '%.2f')]...
        %     }, 'FitBoxToText','on')
    end

    linkaxes(ah(1:2), 'x')
end

    