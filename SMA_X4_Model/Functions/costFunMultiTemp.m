function cost = costFunMultiTemp(mdlName, Opt, inputData, Array, costWeight, screenParams)%strainScreen, timeScreen)
    
    tempFlds = setxor(fieldnames(inputData), {'rootPath' 'subFolders'});
    nTemps = length(tempFlds);
    cost = 0;
    for iTemp = 1:nTemps
        P = inputData.(tempFlds{iTemp}).P;
        Inputs = inputData.(tempFlds{iTemp}).Inputs;
        
        % nitiTmp.R = P.NiTi.RPhase;
        nitiTmp.MinorM = P.NiTi.MinorM;
        % nitiTmp.MinorR = P.NiTi.MinorR;
        nitiTmp.script = P.NiTi.script;

        nD = length(Opt.DimNames);
        P = rmfield(P, 'NiTi');
        for i = 1:nD
            eval([Opt.DimNames{i} ' = Array(i) * Opt.Scale(i);']);
        end
        % P.NiTi.RPhase = nitiTmp.R;
        P.NiTi.MinorM = nitiTmp.MinorM;
        % P.NiTi.MinorR = nitiTmp.MinorR;
        % P = X4_NiTi_Param_04(P, false);
        P.NiTi.T0 = 273.15 + Inputs.TT101.mean;
        P.NiTi.script = nitiTmp.script;
        P = X4_NiTi_Param_06(P, false);
        % eval(['P = ' P.NiTi.script '(P, false);']) % this would be nicer but seems to cause error since the script is not transferred to workers. Should retry with script added to project (may not work since it is in X4 project)
    
        %% Create Simulink Workspace Files
        simIn = Simulink.SimulationInput(mdlName);
        simIn = simIn.setVariable('P',P);
        simIn = simIn.setVariable('Inputs',Inputs);

        try
            disp(['Simulating ' tempFlds{iTemp}])
            out = sim(simIn);
            % C = calcCost(out, Inputs, Opt, P, costType);
            C = calcCost(out, Inputs, P, costWeight, screenParams);%strainScreen, timeScreen);
            cost = cost + C.total;
        catch
            C.strain = NaN;
            C.heat = NaN;
            C.total = NaN;
            cost = cost + 300;
        end
        clear P Inputs
    end
    cost = cost/nTemps;
end