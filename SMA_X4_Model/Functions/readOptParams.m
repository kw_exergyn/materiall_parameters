
function Opt = readOptParams(fileName, P, SheetNames)
    % fileName = 'C:\Users\DarraghClabby\localRepos\materiall_parameters\SMA_X4_Model\X4OptParams.xls';
    % SheetNames = {'General01' 'Params01'};
    if isempty(SheetNames)
        SheetNames = sheetnames(fileName);
    end

    for iSheet = 1:length(SheetNames)
        if strcmp(SheetNames{iSheet}(1), '%')
            continue
        end
        [~, ~, params] = xlsread(fileName, SheetNames{iSheet}); 
        nParams = length(params(:, 1));
        nFields = length(params(1, :));
        if contains(SheetNames{iSheet}, 'General')
            for iParam = 1:nParams
                if strcmp(params{iParam, 1}(1), '%')
                    continue
                end
                Opt.(params{iParam, 1}) = params{iParam, 2};
            end

        elseif contains(SheetNames{iSheet}, 'Params')
            for iParam = 1:nParams
                paramName = params{iParam, 1};
                if strcmp(paramName(1), '%')
                    continue
                end

                for iField = 1:2
                    if iParam == 1
                        eval(['Opt.' params{1, iField} ' = {};' ])
                    else
                        eval(['Opt.' params{1, iField} ' = [Opt.' params{1, iField} '; {''' params{iParam, iField}  '''}];'])
                    end
                end
        
                for iField = 3:nFields
                    if isnan(params{1, iField})
                        continue
                    end
                    if iParam == 1
                        eval(['Opt.' params{1, iField} ' = [];' ])
                    else
                        try
                            eval(['Opt.' params{1, iField} ' = [Opt.' params{1, iField} ';  params{iParam, iField}  ];'])
                        catch
                            eval(['Opt.' params{1, iField} ' = {Opt.' params{1, iField} ';  params{iParam, iField}  };'])
                        end
                    end
                end
        
                if iParam == 1
                    Opt.InitArray = [];
                else
                    try
                        eval(['initVal = ' Opt.DimNames{end} ';'])
%                     catch
%                         warning(['Value for ' Opt.DimNames{end} ' not found. Set to NaN'])
%                         initVal = NaN;
                    end
                    Opt.InitArray = [Opt.InitArray; initVal];
                end
            end
            Opt.InitArray = Opt.InitArray./Opt.Scale;
            Opt.nD = length(Opt.DimNames);

        elseif ismember(SheetNames{iSheet}, {'Ineq' 'Eq'})
            % Opt.Ineq = params(1, 2:end);
            % Opt.A = cell2mat(params(2:end-1, 2:end));
            % Opt.b = cell2mat(params(end, 2:end))';

            desc = [];
            A = [];
            b = [];
            for iCon = 2:length(params(1, :))
                if ~strcmp(params{1, iCon}(1), '%')
                    desc = [desc; {[params{1, iCon} '<=' num2str(params{end, iCon})]}];
                    A = [A cell2mat(params(2:end-1, iCon))];
                    b = [b cell2mat(params(end, iCon))];
                end
            end

            if strcmp(SheetNames{iSheet}, 'Ineq')
                Opt.A = A';
                Opt.b = b';
                Opt.Ineq = desc;
            else
                Opt.Aeq = A';
                Opt.beq = b';
                Opt.Eq = regexprep(desc, '<', '');
            end

        % elseif strcmp(SheetNames{iSheet}, 'Eq')
        %     Opt.Eq = params(1, 2:end);
        %     Opt.Aeq = cell2mat(params(2:end-1, 2:end));
        %     Opt.beq = cell2mat(params(end, 2:end))';
        end
    
        if ~isfield(Opt, 'A')
            Opt.A = [];
            Opt.b = [];
        end

        if ~isfield(Opt, 'Aeq')
            Opt.Aeq = [];
            Opt.beq = [];
        end
    end
end