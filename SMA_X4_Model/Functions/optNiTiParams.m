function [P, nBest, iBest] = optNiTiParams(P, Opt, stats)
    nD = length(Opt.DimNames);
    niTiScript = P.NiTi.script;
    P = rmfield(P, 'NiTi');

    if isfield(stats, 'Fval')
        [nBest, iBest] = min(stats.Fval);
        for iD = 1:nD
            disp([Opt.DimNames{iD} ' = ' num2str(stats.X(iBest, iD)*Opt.Scale(iD))])
            eval([Opt.DimNames{iD} ' = stats.X(iBest, iD)*Opt.Scale(iD);']);
        end
    else
        costTmp = {stats.costTotal};
        costTmp(find(~cellfun('isempty', costTmp) == 0)) = {NaN}; % replace ant empty elements with NaN
        [nBest, iBest] = min(cell2mat(costTmp));
        for iD = 1:nD
            fieldName = strrep(Opt.DimNames{iD}, '.', '_');
            disp([Opt.DimNames{iD} ' = ' num2str(stats(iBest).(fieldName)*Opt.Scale(iD))])
            eval([Opt.DimNames{iD} ' = stats(iBest).(fieldName)*Opt.Scale(iD);']);
        end
    end
    eval(['P = ' niTiScript '(P, false);']);
    P.NiTi.script = niTiScript;
end