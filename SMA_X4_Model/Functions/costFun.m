function cost = costFun(P, mdlName, Opt, Inputs, Array, costType, strainScreen)
    
    nD = length(Opt.DimNames);
    P = rmfield(P, 'NiTi');
    for i = 1:nD
        eval([Opt.DimNames{i} ' = Array(i) * Opt.Scale(i);']);
    end
%     P.NiTi.MinorR = 0;
    P = X4_NiTi_Param_04(P, false);

    %% Create Simulink Workspace Files
    simIn = Simulink.SimulationInput(mdlName);
    simIn = simIn.setVariable('P',P);
    simIn = simIn.setVariable('Inputs',Inputs);

    try
        out = sim(simIn);
        % C = calcCost(out, Inputs, Opt, P, costType);
        C = calcCost(out, Inputs, P, costType, strainScreen);
        cost = C.total;
    catch
        C.strain = NaN;
        C.heat = NaN;
        C.total = NaN;
        cost = 300;
    end

    % for i = 1:Opt.nD
    %     fieldName = strrep(Opt.DimNames{i},'.','_');
    %     stats.(fieldName) = Array(i);
    % end
    % stats.costStrain = C.strain;
    % stats.costHeat = C.heat;
    % stats.costTotal = C.total;


    % try
    %     out = sim(simIn);
    % catch
    %     cost = 300;
    %     C.strain = NaN;
    %     C.heat = NaN;
    %     C.total = NaN;
    % 
    %     for i = 1:Opt.nD
    %         fieldName = strrep(Opt.DimNames{i},'.','_');
    %         stats.(fieldName) = Array(i);
    %     end
    %     stats.costStrain = C.strain;
    %     stats.costHeat = C.heat;
    %     stats.costTotal = C.total;
    %     return
    % end
    % 
    % try
    %     C = calcCost(out, Inputs, Opt, P);
    % catch
    %     cost = 400;
    %     C.strain = NaN;
    %     C.heat = NaN;
    %     C.total = NaN;
    % 
    %     for i = 1:Opt.nD
    %         fieldName = strrep(Opt.DimNames{i},'.','_');
    %         stats.(fieldName) = Array(i);
    %     end
    %     stats.costStrain = C.strain;
    %     stats.costHeat = C.heat;
    %     stats.costTotal = C.total;
    %     return
    % end

    
% %     if success
%         for i = 1:Opt.nD
%             fieldName = strrep(Opt.DimNames{i},'.','_');
%             stats.(fieldName) = Array(i);
%         end
%         stats.costStrain = C.strain;
%         stats.costHeat = C.heat;
%         stats.costTotal = C.total;
% 
%         % try
%         %     m = matfile([Opt.Folder filesep filename],'Writable',true);
%         %     m.stats(1,end+1) = stats;
%         % catch
%         %     pause(rand*10)
%         %     try
%         %         m = matfile([Opt.Folder filesep filename],'Writable',true);
%         %         m.stats(1,end+1) = stats;
%         %     catch
%         %         disp('Failed to write to log');
%         %         disp(stats);
%         %     end
%         % end
% %     else
% %         disp('Simulation Failed')
% %         cost = 300;
% %     end
end