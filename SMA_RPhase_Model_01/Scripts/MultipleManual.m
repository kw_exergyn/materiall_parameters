% close all
clear all
%% Setup
% Get Them parameters
[O,P] =  getDefaultParamsRPhase(0);

startTime = datestr(now,'YYYY-mm-DD_HH-MM');
Proj = matlab.project.rootProject;

%% Get Model, save it and set fast restart on.
% mdlName = 'MaterialOptimisation';
mdlName = 'MaterialOptimisationRphase_04b';
load_system(mdlName);

if string( get_param( mdlName, 'Dirty') ) == "on"
    save_system( mdlName );
end

set_param( mdlName, 'FastRestart', 'off' );

% % Material Parameters From Keith
% P.NiTi.Offset   =   0;
% P.NiTi.M.Hcur	=	0.0397; % [m/m]
% P.NiTi.M.MS	    =	288.7501; % [K]
% P.NiTi.M.MF	    =	211.83; % [K]
% P.NiTi.M.A1S	=	239.289; % [K]
% P.NiTi.M.A1F	=	295.2221; % [K]
% P.NiTi.M.N1	    =	0.28538; % []
% P.NiTi.M.N2	    =	0.20438; % []
% P.NiTi.M.N3	    =	0.13846; % []
% P.NiTi.M.N4	    =	0.26217; % []
% P.NiTi.M.CA1	=	8312570.7589; % [Pa/K]
% P.NiTi.M.EA1	=	35339022934.3185; % [Pa]
% P.NiTi.M.EM	    =	56164209514.7298; % [Pa]
% P.NiTi.M.y	    =	4.0696; % []
% P.NiTi.M.C_	    =	0.69225; % []
% P.NiTi.R.Hcur	=	0.01602; % [m/m]
% P.NiTi.R.RS	    =	279.7565; % [K]
% P.NiTi.R.RF	    =	166.4423; % [K]
% P.NiTi.R.A2S	=	224.5687; % [K]
% P.NiTi.R.A2F	=	372.0992; % [K]
% P.NiTi.R.N1	    =	0.29296; % []
% P.NiTi.R.N2	    =	0.20715; % []
% P.NiTi.R.N3	    =	0.18774; % []
% P.NiTi.R.N4	    =	0.15198; % []
% P.NiTi.R.CA2	=	6859823.8482; % [Pa/K]
% P.NiTi.R.EA2	=	35266469134.7681; % [Pa]
% P.NiTi.R.ER	    =	64965503305.1278; % [Pa]

% P.NiTi.M.Hcur	=	0.038388; % [m/m]
% P.NiTi.M.MS	=	278.2181; % [K]
% P.NiTi.M.MF	=	228.1164; % [K]
% P.NiTi.M.A1S	=	236.9718; % [K]
% P.NiTi.M.A1F	=	294.6182; % [K]
% P.NiTi.M.N1	=	0.21217; % []
% P.NiTi.M.N2	=	0.1547; % []
% P.NiTi.M.N3	=	0.22547; % []
% P.NiTi.M.N4	=	0.10068; % []
% P.NiTi.M.CA1	=	8724516.1299; % [Pa/K]
% P.NiTi.M.EA1	=	47843837817.9683; % [Pa]
% P.NiTi.M.EM	=	68598021961.711; % [Pa]
% P.NiTi.M.y	=	3.0462; % []
% P.NiTi.M.C_	=	0.9673; % []
% P.NiTi.R.Hcur	=	0.014782; % [m/m]
% P.NiTi.R.RS	=	277.9116; % [K]
% P.NiTi.R.RF	=	201.7048; % [K]
% P.NiTi.R.A2S	=	272.0832; % [K]
% P.NiTi.R.A2F	=	373.8465; % [K]
% P.NiTi.R.N1	=	0.2496; % []
% P.NiTi.R.N2	=	0.19607; % []
% P.NiTi.R.N3	=	0.17158; % []
% P.NiTi.R.N4	=	0.13266; % []
% P.NiTi.R.CA2	=	6026665.4808; % [Pa/K]
% P.NiTi.R.EA2	=	37890307787.6744; % [Pa]
% P.NiTi.R.ER	=	63622881627.7014; % [Pa]

% P.NiTi.M.Hcur	=	0.03507; % [m/m]
% P.NiTi.M.MS	=	296.4685; % [K]
% P.NiTi.M.MF	=	213.5719; % [K]
% P.NiTi.M.A1S	=	242.5743; % [K]
% P.NiTi.M.A1F	=	299.5685; % [K]
% P.NiTi.M.N1	=	0.25267; % []
% P.NiTi.M.N2	=	0.11727; % []
% P.NiTi.M.N3	=	0.27051; % []
% P.NiTi.M.N4	=	0.17031; % []
% P.NiTi.M.CA1	=	9840306.4748; % [Pa/K]
% P.NiTi.M.EA1	=	68202594826.2177; % [Pa]
% P.NiTi.M.EM	=	44555966878.332; % [Pa]
% P.NiTi.M.y	=	5.5807; % []
% P.NiTi.M.C_	=	0.66835; % []

% P.NiTi.M.Hcur	=	0.038147; % [m/m]
% P.NiTi.M.MS	    =	296.7436; % [K]
% P.NiTi.M.MF	    =	209.3997; % [K]
% P.NiTi.M.A1S	=	243.8011; % [K]
% P.NiTi.M.A1F	=	296.9361; % [K]
% P.NiTi.M.N1	    =	0.2715; % []
% P.NiTi.M.N2	    =	0.18517; % []
% P.NiTi.M.N3	    =	0.23192; % []
% P.NiTi.M.N4	    =	0.20395; % []
% P.NiTi.M.CA1	=	9035687.5612; % [Pa/K]
% P.NiTi.M.EA1	=	64031125084.2086; % [Pa]
% P.NiTi.M.EM	    =	55059528472.8628; % [Pa]
% P.NiTi.M.y	    =	5.4099; % []
% P.NiTi.M.C_	    =	0.46764; % []

% P.NiTi.M.Hcur	=	0.038147; % [m/m]
% P.NiTi.M.MS	    =	297; % [K]
% P.NiTi.M.MF	    =	209.3997; % [K]
% P.NiTi.M.A1S	=	243.8011; % [K]
% P.NiTi.M.A1F	=	297; % [K]
% P.NiTi.M.N1	    =	0.2715; % []
% P.NiTi.M.N2	    =	0.18517; % []
% P.NiTi.M.N3	    =	0.23192; % []
% P.NiTi.M.N4	    =	0.20395; % []
% P.NiTi.M.CA1	=	9035687.5612; % [Pa/K]
% P.NiTi.M.EA1	=	64031125084.2086; % [Pa]
% P.NiTi.M.EM	    =	55059528472.8628; % [Pa]
% P.NiTi.M.y	    =	5.4099; % []
% P.NiTi.M.C_	    =	0.46764; % []

% M & R Tuned on 30
% P.NiTi.M.Hcur	=	0.041736; % [m/m]
% P.NiTi.M.MS	=	309.9566; % [K]
% P.NiTi.M.MF	=	187.0984; % [K]
% P.NiTi.M.A1S	=	221.1802; % [K]
% P.NiTi.M.A1F	=	311.2351; % [K]
% P.NiTi.M.N1	=	0.20375; % []
% P.NiTi.M.N2	=	0.28158; % []
% P.NiTi.M.N3	=	0.12559; % []
% P.NiTi.M.N4	=	0.23406; % []
% P.NiTi.M.CA1	=	7670253.091; % [Pa/K]
% P.NiTi.M.EA1	=	43985945569.3229; % [Pa]
% P.NiTi.M.EM	=	60084502804.614; % [Pa]
% P.NiTi.M.y	=	2.6694; % []
% P.NiTi.M.C_	=	0.62081; % []
% P.NiTi.M.CalS	=	1.8718; % [Pa]
% P.NiTi.R.Hcur	=	0.010978; % [m/m]
% P.NiTi.R.RS	=	286.2837; % [K]
% P.NiTi.R.RF	=	212.1689; % [K]
% P.NiTi.R.A2S	=	236.7264; % [K]
% P.NiTi.R.A2F	=	399.8979; % [K]
% P.NiTi.R.N1	=	0.25091; % []
% P.NiTi.R.N2	=	0.2148; % []
% P.NiTi.R.N3	=	0.20786; % []
% P.NiTi.R.N4	=	0.22344; % []
% P.NiTi.R.CA2	=	7533656.0932; % [Pa/K]
% P.NiTi.R.EA2	=	83535631682.7955; % [Pa]
% P.NiTi.R.ER	=	51098788588.126; % [Pa]
% P.NiTi.R.y	=	3.2518; % []
% P.NiTi.R.C_	=	0.19465; % []
% P.NiTi.R.CalS	=	1.548; % [Pa]

% % M & R Tuned on 0, 30 and 60
% P.NiTi.M.Hcur	=	0.044977; % [m/m]
% P.NiTi.M.MS	=	291.0723; % [K]
% P.NiTi.M.MF	=	212.0401; % [K]
% P.NiTi.M.A1S	=	236.6645; % [K]
% P.NiTi.M.A1F	=	291.1775; % [K]
% P.NiTi.M.N1	=	0.1791; % []
% P.NiTi.M.N2	=	0.25008; % []
% P.NiTi.M.N3	=	0.17665; % []
% P.NiTi.M.N4	=	0.1878; % []
% P.NiTi.M.CA1	=	7238101.4947; % [Pa/K]
% P.NiTi.M.EA1	=	31999146305.6479; % [Pa]
% P.NiTi.M.EM	=	56510421985.0289; % [Pa]
% P.NiTi.M.y	=	2.2297; % []
% P.NiTi.M.C_	=	0.78539; % []
% P.NiTi.M.CalS	=	1.6534; % [Pa]
% P.NiTi.R.Hcur	=	0.019382; % [m/m]
% P.NiTi.R.RS	=	327.7755; % [K]
% P.NiTi.R.RF	=	163.4552; % [K]
% P.NiTi.R.A2S	=	286.8613; % [K]
% P.NiTi.R.A2F	=	401.1961; % [K]
% P.NiTi.R.N1	=	0.20153; % []
% P.NiTi.R.N2	=	0.2925; % []
% P.NiTi.R.N3	=	0.2771; % []
% P.NiTi.R.N4	=	0.22812; % []
% P.NiTi.R.CA2	=	10683096.4365; % [Pa/K]
% P.NiTi.R.EA2	=	50514378075.7229; % [Pa]
% P.NiTi.R.ER	=	82887028240.0378; % [Pa]
% P.NiTi.R.y	=	3.6282; % []
% P.NiTi.R.C_	=	0.82336; % []
% P.NiTi.R.CalS	=	0.88388; % [Pa]

% M on 10, 30 and 60
% P.NiTi.M.Hcur	=	0.041347; % [m/m]
% P.NiTi.M.MS	    =	286.8613; % [K]
% P.NiTi.M.MF	    =	202.1604; % [K]
% P.NiTi.M.A1S	=	240.978; % [K]
% P.NiTi.M.A1F	=	292.1055; % [K]
% P.NiTi.M.N1	    =	0.34987; % []
% P.NiTi.M.N2	    =	0.21361; % []
% P.NiTi.M.N3	    =	0.12305; % []
% P.NiTi.M.N4	    =	0.19798; % []
% P.NiTi.M.CA1	=	7776151.8655; % [Pa/K]
% P.NiTi.M.EA1	=	69694991830.1243; % [Pa]
% P.NiTi.M.EM	    =	69959778024.7042; % [Pa]
% P.NiTi.M.y	    =	2.6066; % []
% P.NiTi.M.C_	    =	0.2463; % []
% P.NiTi.M.CalS	=	0.60553; % [Pa]

% % % M on 10, 30 and 60
% % P.NiTi.M.Hcur	    =	0.05; % [m/m] Higher numbers increase strain and latent heat. Smaller numbers decrease strain and latent heat
% % P.NiTi.M.MS	    =	291.8261; % [K]
% % P.NiTi.M.MF	    =	199.364; % [K]
% % P.NiTi.M.A1S	    =	242.7665; % [K]
% % P.NiTi.M.A1F	    =	294.0543; % [K]
% % P.NiTi.M.N1	    =	0.4; % [] As below but at the low strain, 0 makes hysteresis bigger, higher numbers change the slope of the load and make hysteresis smaller
% % P.NiTi.M.N2	    =	0.4; % [] Small numbers make the tails thinner and lower hysteresis, large numbers make the hysteresis bigger at high strain
% P.NiTi.M.N3	        =	0; % [] Significant affect at low temperatures. Lower seems better, should maybe be 0?
% % P.NiTi.M.N4	    =	0.4; % []   Stress at which the unload strain reduction happens at. Higher N4 = higher stress = lower hystersis
% % P.NiTi.M.CA1	    =	5809774.5685; % [Pa/K] Higher numbers seem to shift the curve down, smaller shift it up
% P.NiTi.M.EA1	    =	55563925393; % [Pa]    Change for each Temp, changes strain at peak stress. Higher EA1 higher strain
% P.NiTi.M.EM	        =	57038941026; % [Pa]    Higher = steeper slopes at end, lower = shallower slope
% % P.NiTi.M.y	        =	500.2514; % []                  Minor improvement at large numbers, Minor loop parameter and we're not in minor loops, so probably best left alone.
% % P.NiTi.M.C_	    =	8.9346; % []                No affect
% % P.NiTi.M.CalS	=	1.8787; % [Pa]                  No affect

% Plan: Optimise 30degC all parameters exceptL: N3 set to 0, Cal_S disable,
% C_ Disable, y disable. Consider fixing EM based on manual tuning.
% Then fix all parameters except Hcur and EA1 and re-tune for each temp
% 
% P.NiTi.M.Hcur	=	0.038; % [m/m]
% % P.NiTi.M.MS	    =	291.7181; % [K]
% P.NiTi.M.MF	    =	190; % [K]
% P.NiTi.M.A1S	=	235; % [K]
% P.NiTi.M.A1F	=	296.5682; % [K]
% P.NiTi.M.N1	    =	0.35318; % []
% P.NiTi.M.N2	    =	0.22109; % []
% P.NiTi.M.N4	    =	0.3; % []
% P.NiTi.M.CA1	=	7000000; % [Pa/K]
% P.NiTi.M.EA1	=	90000000000; % [Pa]
% P.NiTi.M.EM	    =	67451451884.1273; % [Pa]

% P.NiTi.M.Hcur	=	0.042516; % [m/m]
% P.NiTi.M.MF	    =	204.8659; % [K]
% P.NiTi.M.A1S	=	245.4448; % [K]
% P.NiTi.M.A1F	=	297.7656; % [K]
% P.NiTi.M.N1	    =	0.27633; % []
% P.NiTi.M.N2	    =	0.22536; % []
% P.NiTi.M.N4	    =	0.28949; % []
% P.NiTi.M.CA1	=	8144234.0501; % [Pa/K]
% P.NiTi.M.EA1	=	65039785082.2305; % [Pa]
% P.NiTi.M.EM	    =	73590225576.9158; % [Pa]

% P.NiTi.M.Hcur	=	0.042516; % [m/m]
% P.NiTi.M.MF	    =	204.8659; % [K]
% P.NiTi.M.A1S	=	245.4448; % [K]
% P.NiTi.M.A1F	=	297.7656; % [K]
% P.NiTi.M.N1	    =	0.28; % []
% P.NiTi.M.N2	    =	0.22536; % []
% P.NiTi.M.N4	    =	0.28; % []
% P.NiTi.M.CA1	=	8144234.0501; % [Pa/K]
% P.NiTi.M.EA1	=	7e10; % [Pa] Youngs Modulus
% P.NiTi.M.EM	    =	7e10; % [Pa]
% 
% P.NiTi.M.MS     = P.NiTi.M.A1F-1;
% P.NiTi.M.N3	    =	0;

% % M Tuned for 40°C
% P.NiTi.M.Hcur	=	0.041587; % [m/m]
% P.NiTi.M.MS	    =	291.7302; % [K]
% P.NiTi.M.MF	    =	204.7261; % [K]
% P.NiTi.M.A1S	=	245.5436; % [K]
% P.NiTi.M.A1F	=	292.3067; % [K]
% P.NiTi.M.N1	    =	0.3848; % []
% % P.NiTi.M.N1	    =	0.5; % []
% P.NiTi.M.N2	    =	0.18231; % []
% P.NiTi.M.N3	    =	0.28992; % []
% % P.NiTi.M.N3	    =	0.5; % []
% P.NiTi.M.N4	    =	0.28722; % []
% P.NiTi.M.CA1	=	8316685.4859; % [Pa/K]
% P.NiTi.M.EA1	=	76318359375; % [Pa]
% P.NiTi.M.EM	    =	81441895182.0883; % [Pa]

% M Tuned for 40°C
% P.NiTi.M.Hcur	=	0.041464; % [m/m]
% P.NiTi.M.MS	    =	292.2937; % [K]
% P.NiTi.M.MF	    =	204.987; % [K]
% P.NiTi.M.A1S	=	245.6583; % [K]
% P.NiTi.M.A1F	=	292.3443; % [K]
% P.NiTi.M.N1	    =	0.35546; % []
% P.NiTi.M.N2	    =	0.1768; % []
% P.NiTi.M.N3	    =	0.3322; % []
% P.NiTi.M.N4	    =	0.3156; % []
% P.NiTi.M.CA1	=	8336576.0484; % [Pa/K]
% P.NiTi.M.EA1	=	75875402277.2119; % [Pa]
% P.NiTi.M.EM	    =	81752681190.3678; % [Pa]

% M Tuned for 10,20, 30, 40, 50, 60
P.NiTi.M.Hcur	=	0.0414; % [m/m]
P.NiTi.M.MS	    =	291.6361; % [K]
P.NiTi.M.MF	    =	202.4635; % [K]
P.NiTi.M.A1S	=	245.3244; % [K]
P.NiTi.M.A1F	=	292.1571; % [K]
P.NiTi.M.N1	    =	0.39593; % []
P.NiTi.M.N2	    =	0.18703; % []
P.NiTi.M.N3	    =	0.30069; % []
P.NiTi.M.N4	    =	0.28661; % []
P.NiTi.M.CA1	=	8313567.8006; % [Pa/K]
P.NiTi.M.EA1	=	77637631021.9768; % [Pa]
P.NiTi.M.EM	    =	81487562437.6532; % [Pa]


% % Manual Tune from above
% P.NiTi.M.Hcur	=	0.0415; % [m/m]
% P.NiTi.M.MS	    =	291.6361; % [K]
% P.NiTi.M.MF	    =	202.4635; % [K]
% P.NiTi.M.A1S	=	245.3244; % [K]
% P.NiTi.M.A1F	=	292.1571; % [K]
% P.NiTi.M.N1	    =	0.5; % []
% P.NiTi.M.N2	    =	0.18703; % []
% P.NiTi.M.N3	    =	0.2; % []
% P.NiTi.M.N4	    =	0.28661; % []
% P.NiTi.M.CA1	=	8e6; % [Pa/K]
% P.NiTi.M.EA1	=	7e10; % [Pa]
% P.NiTi.M.EM	    =	7.5e10; % [Pa]

%     P_NiTi_M_Hcur: 4.1587
%       P_NiTi_M_MS: 29.1730
%       P_NiTi_M_MF: 20.4726
%      P_NiTi_M_A1S: 24.5544
%      P_NiTi_M_A1F: 29.2307
%       P_NiTi_M_N1: 0.3848
%       P_NiTi_M_N2: 0.1823
%       P_NiTi_M_N3: 0.2899
%       P_NiTi_M_N4: 0.2872
%      P_NiTi_M_CA1: 8.3167
%      P_NiTi_M_EA1: 7.6318
%       P_NiTi_M_EM: 8.1442
% 
% P.NiTi.R.Hcur	=	0.026989; % [m/m]
% P.NiTi.R.RS	=	250.0892; % [K]
% P.NiTi.R.RF	=	188.7252; % [K]
% P.NiTi.R.A2S	=	157.3695; % [K]
% P.NiTi.R.A2F	=	325.3697; % [K]
% P.NiTi.R.N1	=	0.15931; % []
% P.NiTi.R.N2	=	0.13765; % []
% P.NiTi.R.N3	=	0.13062; % []
% P.NiTi.R.N4	=	0.15932; % []
% P.NiTi.R.CA2	=	15550459.9456; % [Pa/K]
% P.NiTi.R.EA2	=	27780394492.7815; % [Pa]
% P.NiTi.R.ER	=	45435567237.0937; % [Pa]
% P.NiTi.R.y	=	3.0323; % []
% P.NiTi.R.C_	=	0.97133; % []
% P.NiTi.R.CalS	=	1.2349; % [Pa]

% P.NiTi.R.ER = P.NiTi.M.EA1;
P.NiTi.M.CM = P.NiTi.M.CA1;
P.NiTi.R.CR = P.NiTi.R.CA2;

%% Load Data
% D.Temps = [0 10 20 30 40 50 60];
% Test.Heat = [13.1 15.2 16.5 17 17.6 17.2 17.1];    % kJ/kg
% Test.Cool = [12.0 14.3 15.6 16.1 16.8 16.4 16.3];

D.Temps = [10 20 30 40 50];
Test.Heat = [15.2 16.5 17 17.6 17.2];    % kJ/kg
Test.Cool = [14.3 15.6 16.1 16.8 16.4];

% D.Temps = [30];
% Test.Heat = [17];    % kJ/kg
% Test.Cool = [16.1];

nTemps = length(D.Temps);

Opt.StrainOffset = -0.25;
Opt.nCat = 2;

tic
for i = 1:nTemps
    D.FileName{i} = [num2str(D.Temps(i)) 'deg'];
    D.Name{i} = [num2str(D.Temps(i)) '°C'];
    load([D.FileName{i} '.mat'])
    D.T{i} = TDs;
    Opt.EnergyDensityTargetHeat = Test.Heat(i);
    Opt.EnergyDensityTargetCool = Test.Cool(i);
    Opt.dt = mean(diff(TDs.Temp.Time));

    vel = [0; diff(TDs.Displacement.Data/1000)]/Opt.dt;
    pwr = vel.*TDs.Force.Data;
    energy = cumsum(pwr)*Opt.dt;
    specEnergy = energy/(P.Stack.Mass*P.Stack.nSeg)*1e-3;

    Test.MechWorkIn(i) = max(specEnergy);
    Test.MechNetWork(i) = specEnergy(end);
    Test.MechWorkOut(i) = Test.MechWorkIn(i)-Test.MechNetWork(i);

    time = linspace(0, TDs.Temp.Time(end)*Opt.nCat, length(TDs.Temp.Time)*Opt.nCat)';

    Inputs.Temp = timeseries(repmat(TDs.Temp.Data,Opt.nCat,1),time);
    Inputs.Temp.Data(1:end) = mean(Inputs.Temp.Data);

    Inputs.Force = timeseries(repmat(TDs.Force.Data,Opt.nCat,1),time);
    Inputs.Force.Data(1:20) = -100;

    Inputs.TransDir = timeseries(repmat(TDs.TransDir.Data,Opt.nCat,1),time);


    %% Get Parameters
    P.Sim.logStep = Opt.dt;
    P.Sim.tStep = Opt.dt;                   % Time Step [s]
    % P.Sim.tStep = 0.01;
    P.Sim.maxTime = TDs.Temp.Time(end)*2;
    P.showPlots = 0;

    P.FDS.InitTemp = mean(TDs.Temp.Data(1:10))-273.15;
    P.FDS.Tmid = P.FDS.InitTemp;
    P.FDS.AmbientTemp = 16;
    P.FDS.FluidPressure = 4;
    P.NiTi.ForceInt = 0;

    RPhaseMat

    name = 'sb_Core_P';
    tempStruct = Simulink.Bus.createObject(P);
    eval([name ' = evalin(''base'',tempStruct.busName);']);

    name = 'sb_Core_O';
    tempStruct = Simulink.Bus.createObject(O);
    eval([name ' = evalin(''base'',tempStruct.busName);']);

    name = 'sb_Core_Inputs';
    tempStruct = Simulink.Bus.createObject(Inputs);
    eval([name ' = evalin(''base'',tempStruct.busName);']);


    %% Create Simulink Workspace Files
    simIn = Simulink.SimulationInput(mdlName);
    simIn = simIn.setVariable('P',P);
    simIn = simIn.setVariable('O',O);
    simIn = simIn.setVariable('Inputs',Inputs);
    simIn = simIn.setVariable('sb_Core_P',sb_Core_P);
    simIn = simIn.setVariable('sb_Core_O',sb_Core_O);

    out = sim(simIn);
    P.DSC= 0;
    [Cost(i,1),Cost(i,2),Cost(i,3),Cost(i,4),SimResult.Heat(i),SimResult.Cool(i),SimResult.MechWorkIn(i),SimResult.MechWorkOut(i),SimResult.MechNetWork(i)]  = costCalcMaterialRPhase(out, TDs, Opt,P);

    SimOut(i) = out;  
end
toc

%% Post Process
Test.ThermNetWork = Test.Heat - Test.Cool;
Test.COPTherm = Test.Heat./Test.ThermNetWork;
Test.COPMech = Test.Heat./Test.MechNetWork;

SimResult.ThermNetWork = SimResult.Heat - SimResult.Cool;
SimResult.COPTherm = SimResult.Heat./SimResult.ThermNetWork;
SimResult.COPMech = SimResult.Heat./SimResult.MechNetWork;

disp(mean(Cost(:,1)))

%% Plot Data
close all
SetGPStyle(1);
h = gobjects(0);
a = gobjects(0);
cols = lines(nTemps);

h(end+1) = figure('Name','057_Ex_I_Ni51p6Ti_S053 - Sim vs Test','units','normalized','outerposition',[0 0.02 1.0 0.98],'DefaultAxesFontSize',10);

t = tiledlayout(2,3);
t.TileSpacing = 'compact';
t.Padding = 'compact';


for i = 1:nTemps
    out = SimOut(i);

    % Combined the three NiTi pieces into one strucutre (hopefully)
    P.Stack.Mass = P.Stack.Mass*P.Stack.nSeg;
    P.Stack.Height = P.Stack.Height*P.Stack.nSeg;
    
    vars = fields(out.NiTi1);
    nv = length(vars);
    out.NiTi = out.NiTi1;
    for j = 1:nv
        var = vars{j};    
        for k = 2:P.Stack.nSeg
            niti = ['NiTi' num2str(k)];
            out.NiTi.(var).Data = out.NiTi.(var).Data + out.(niti).(var).Data;
        end
        out.NiTi.(var).Data = out.NiTi.(var).Data/P.Stack.nSeg;
    end

    TDs = D.T{i};

    ida = length(TDs.Temp.Data);
    
    a(end+1) = nexttile(i);
    hold on
    plot(TDs.Strain.Data,TDs.Stress.Data ./1e6, 'DisplayName', 'From Jan','color',cols(i,:))
    plot(((out.NiTi.Strain.Data(ida:end) - (out.NiTi.Strain.Data(ida))) .*100)+Opt.StrainOffset,out.Stress.Data(ida:end) ./1e6,'linestyle','--','DisplayName','Sim 1','color',cols(i,:))
    

    legend('location','northwest')
    ylim([-1e3,0])
    xlim([-6.5,0])
    xlabel('Strain [%]')
    ylabel('Stress [MPa]')
    title(D.Name{i})

    % uitable(a(end),'Data', [Test.Heat(i) Test.Cool(i)],'ColumnName','Heat','Position', [1220,149,378,128],'FontSize',14);

end

% linkaxes(a,'xy')


% uitable('Data', Results(2:end,:), 'ColumnName', Results(1,:) ,'Position', [1220,149,378,128],'FontSize',14);

