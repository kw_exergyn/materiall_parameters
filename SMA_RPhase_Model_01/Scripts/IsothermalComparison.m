clear
close all

periods = [70 500];
temps = 0:10:60;

np = length(periods);
nt = length(temps);


for i = 1:np
    period = periods(i);
    % periodS = [num2str(p)]
    for j = 1:nt
        temp = temps(j);
        filename = ['Isothermal_' num2str(period) 's_' num2str(temp) 'deg.mat'];
        load(filename)
        Data{i,j} = TDs;

        mass = 1.2921e-04;
        dt = mean(diff(TDs.Temp.Time));
        vel = [0; diff(TDs.Displacement.Data/1000)]/dt;
        pwr = vel.*TDs.Force.Data;
        energy = cumsum(pwr)*dt;
        specEnergy = energy/(mass)*1e-3;
    
        Test.MechWorkIn(i,j) = max(specEnergy);
        Test.MechNetWork(i,j) = specEnergy(end);
        Test.MechWorkOut(i,j) = Test.MechWorkIn(i,j)-Test.MechNetWork(i,j);
    end
end

%% Plots
close all
SetGPStyle(1);
h = gobjects(0);
a = gobjects(0);
cols = lines(nt);
   
h(end+1) = figure('Name','057_Ex_I_Ni51p6Ti_S053 - Sim vs Test','units','normalized','outerposition',[0 0.02 1.0 0.98],'DefaultAxesFontSize',10);

t = tiledlayout(3,3);
t.TileSpacing = 'compact';
t.Padding = 'compact';


for j = 1:nt
    a(end+1) = nexttile;
    hold on
    for i = 1:np
        plot(Data{i,j}.Strain.Data, Data{i,j}.Stress.Data*1e-6,'DisplayName',['Test Length = ' num2str(periods(i)) 's'])
    end

    legend('location','northwest')
    ylim([-1e3,0])
    xlim([-6.5,0])
    xlabel('Strain [%]')
    ylabel('Stress [MPa]')
    title(['Temp = ' num2str(temps(j)) '°C'])
end

linkaxes(a,'xy')
