clear all
close all

pctconfig('hostname','82.41.74.125','portrange',[21000 22000]);
pctconfig('allserversocketsincluster', false);

% Get Them parameters
[O,P] =  getDefaultParamsRPhase(0);


startTime = datestr(now,'YYYY-mm-DD_HH-MM');
Proj = matlab.project.rootProject;

%% Get Model, save it and set fast restart on.
% mdlName = 'MaterialOptimisation';
mdlName = 'MaterialOptimisationRphase_04b';
load_system(mdlName);

if string( get_param( mdlName, 'Dirty') ) == "on"
    save_system( mdlName );
end
 set_param( mdlName, 'FastRestart', 'off' );

pool = gcp('nocreate');
if isempty(pool)
    pool = parpool('cluster R2023a');
end
files = dependencies.fileDependencyAnalysis(mdlName);
files{end+1} = 'C:\GECO\GIT\Material Parameters\Subsystems\+SimscapeCustomBlocks\+SMA\SmoothHardeningSMA_RPhase_01_NoMass.ssc';
files{end+1} ='C:\GECO\GIT\Material Parameters\Subsystems\+SimscapeCustomBlocks\+Thermal\variable_conduction.ssc';
pool.addAttachedFiles(files);

Opt.M = 1;
Opt.R = 0;

CrashRestart = 0;
CrashFile = '';
P.DSC = 0;
%% Load data
if  P.DSC == 0

    D.Temps =       [0  10  20  30  40  50  60];
    D.TempEnable =  [0  1   1   1   1   1   1] ;
    D.Heating =     [13.1 15.2 16.5 17 17.6 17.2 17.1];    % kJ/kg
    D.StrainOffset = ones(size(D.Temps))*-0.25;
    P.nTemps = length(D.Temps);

    for i = 1:P.nTemps
        D.FileName{i} = [num2str(D.Temps(i)) 'deg'];
        D.Name{i} = [num2str(D.Temps(i)) '°C'];
        load([D.FileName{i} '.mat'])
        D.T{i} = TDs;
    end
else
    load('MaterialOptimiserData_NiTiCr_DSC.mat')
end
% load('MaterialOptimiserData_SMA08.mat')
h = plotInputData(TDs);

name = 'sb_Core_P';
tempStruct = Simulink.Bus.createObject(P);
eval([name ' = evalin(''base'',tempStruct.busName);']);

name = 'sb_Core_O';
tempStruct = Simulink.Bus.createObject(O);
eval([name ' = evalin(''base'',tempStruct.busName);']);

%% Configure Parameters to Optimise
Opt.Name = 'DA_Niti_01A';

Opt.TestNum = '00';

if Opt.M
    Opt.DimNames{1} = 'P.NiTi.M.Hcur';
    Opt.DimNames{end+1} = 'P.NiTi.M.MS';
    Opt.DimNames{end+1} = 'P.NiTi.M.MF';
    Opt.DimNames{end+1} = 'P.NiTi.M.A1S';
    Opt.DimNames{end+1} = 'P.NiTi.M.A1F';
    Opt.DimNames{end+1} = 'P.NiTi.M.N1';
    Opt.DimNames{end+1} = 'P.NiTi.M.N2';
    Opt.DimNames{end+1} = 'P.NiTi.M.N3';
    Opt.DimNames{end+1} = 'P.NiTi.M.N4';
    Opt.DimNames{end+1} = 'P.NiTi.M.CA1';
    Opt.DimNames{end+1} = 'P.NiTi.M.EA1';
    Opt.DimNames{end+1} = 'P.NiTi.M.EM';
    % Opt.DimNames{end+1} = 'P.NiTi.M.y';
    % Opt.DimNames{end+1} = 'P.NiTi.M.C_';
    % Opt.DimNames{end+1} = 'P.NiTi.M.CalS';

    Opt.Units{1} = 'm/m';
    Opt.Units{end+1} = 'K';
    Opt.Units{end+1} = 'K';
    Opt.Units{end+1} = 'K';
    Opt.Units{end+1} = 'K';
    Opt.Units{end+1} = '';
    Opt.Units{end+1} = '';
    Opt.Units{end+1} = '';
    Opt.Units{end+1} = '';
    Opt.Units{end+1} = 'Pa/K';
    Opt.Units{end+1} = 'Pa';
    Opt.Units{end+1} = 'Pa';
    % Opt.Units{end+1} = '';
    % Opt.Units{end+1} = '';
    % Opt.Units{end+1} = 'Pa';

    % Opt.InitArray(1)     =	0.03696; % [m/m]
    % Opt.InitArray(end+1) =	289.3535; % [K]
    % Opt.InitArray(end+1) =	228.6008; % [K]
    % Opt.InitArray(end+1) =	246.0136; % [K]
    % Opt.InitArray(end+1) =	293.6631; % [K]
    % Opt.InitArray(end+1) =	0.27408; % []
    % Opt.InitArray(end+1) =	0.27476; % []
    % Opt.InitArray(end+1) =	0.12985; % []
    % Opt.InitArray(end+1) =	0.25595; % []
    % Opt.InitArray(end+1) =	8971928.8251; % [Pa/K]
    % Opt.InitArray(end+1) =	50069703783.6368; % [Pa]
    % Opt.InitArray(end+1) =	69836591498.1017; % [Pa]
    % Opt.InitArray(end+1) =	2.2487; % []
    % Opt.InitArray(end+1) =	0.54648; % []
    % % Opt.InitArray(end+1) =	1; % [Pa]

    Opt.InitArray(1) =	0.041587; % [m/m]
    Opt.InitArray(end+1) =	291.7302; % [K]
    Opt.InitArray(end+1) =	204.7261; % [K]
    Opt.InitArray(end+1) =	245.5436; % [K]
    Opt.InitArray(end+1) =	292.3067; % [K]
    Opt.InitArray(end+1) =	0.3848; % []
    Opt.InitArray(end+1) =	0.18231; % []
    Opt.InitArray(end+1) =	0.28992; % []
    Opt.InitArray(end+1) =	0.28722; % []
    Opt.InitArray(end+1) =	8316685.4859; % [Pa/K]
    Opt.InitArray(end+1) =	76318359375; % [Pa]
    Opt.InitArray(end+1) =	81441895182.0883; % [Pa]
    % Opt.InitArray(end+1) =	5.4099; % []
    % Opt.InitArray(end+1) =	0.46764; % []
    % Opt.InitArray(end+1) =	1; % [Pa]

    Opt.lb =    [4.0    28.5    19   24   28.5    0.1   0.1     0.05    0.1     7.5     5       5];       % Lower limits
    Opt.ub =    [4.4    29.5    22   25   30.0    0.4   0.4     0.4     0.4     8.5     9       9];        % Upper Limits
    Opt.Scale = [1e-2   10      10   10     10    1     1       1       1       1e6     1e10    1e10];         % Define any scaling to apply to the channels

end

if Opt.R    
    if ~ Opt.M
        idx1 = 1;
    else
        idx1 = length(Opt.InitArray)+1;
    end
    Opt.DimNames{idx1} = 'P.NiTi.R.Hcur';
    Opt.DimNames{end+1} = 'P.NiTi.R.RS';
    Opt.DimNames{end+1} = 'P.NiTi.R.RF';
    Opt.DimNames{end+1} = 'P.NiTi.R.A2S';
    Opt.DimNames{end+1} = 'P.NiTi.R.A2F';
    Opt.DimNames{end+1} = 'P.NiTi.R.N1';
    Opt.DimNames{end+1} = 'P.NiTi.R.N2';
    Opt.DimNames{end+1} = 'P.NiTi.R.N3';
    Opt.DimNames{end+1} = 'P.NiTi.R.N4';
    Opt.DimNames{end+1} = 'P.NiTi.R.CA2';
    Opt.DimNames{end+1} = 'P.NiTi.R.EA2';
    Opt.DimNames{end+1} = 'P.NiTi.R.ER';
    Opt.DimNames{end+1} = 'P.NiTi.R.y';
    Opt.DimNames{end+1} = 'P.NiTi.R.C_';
    Opt.DimNames{end+1} = 'P.NiTi.R.CalS';

    Opt.Units{idx1} = 'm/m';
    Opt.Units{end+1} = 'K';
    Opt.Units{end+1} = 'K';
    Opt.Units{end+1} = 'K';
    Opt.Units{end+1} = 'K';
    Opt.Units{end+1} = '';
    Opt.Units{end+1} = '';
    Opt.Units{end+1} = '';
    Opt.Units{end+1} = '';
    Opt.Units{end+1} = 'Pa/K';
    Opt.Units{end+1} = 'Pa';
    Opt.Units{end+1} = 'Pa';
    Opt.Units{end+1} = '';
    Opt.Units{end+1} = '';
    Opt.Units{end+1} = 'Pa';

    Opt.InitArray(1) =	0.02; % [m/m]
    Opt.InitArray(end+1) =	270.0196; % [K]
    Opt.InitArray(end+1) =	186.6071; % [K]
    Opt.InitArray(end+1) =	220; % [K]
    Opt.InitArray(end+1) =	370; % [K]
    Opt.InitArray(end+1) =	0.24736; % []
    Opt.InitArray(end+1) =	0.13345; % []
    Opt.InitArray(end+1) =	0.10001; % []
    Opt.InitArray(end+1) =	0.3; % []
    Opt.InitArray(end+1) =	12000000; % [Pa/K]
    Opt.InitArray(end+1) =	30007851044.276; % [Pa]
    Opt.InitArray(end+1) =	27321920278.4494; % [Pa]
    Opt.InitArray(end+1) =	1.4343; % []
    Opt.InitArray(end+1) =	0.99999; % []
    Opt.InitArray(end+1) =	1.7174; % [Pa]

    Rlb =    [0.1    25 16   12  27  0.1 0.1 0.1 0.1 8   5   5   1   0.1     0.5];  % Lower limits
    Rub =    [4      31 23   25  38  0.4 0.4 0.4 0.4 16  90  90  6   1.0     2.0];  % Upper Limits
    RScale = [1e-2   10 10   10  10  1   1   1   1   1e6 1e9 1e9 1   1       1];    % Scaler for upper and lower bounds

    if ~ Opt.M
        Opt.lb      = Rlb;              % Lower limits
        Opt.ub      = Rub;              % Upper Limits
        Opt.Scale   = RScale;           % Scaler for upper and lower bounds
    else
        Opt.lb =    [Opt.lb Rlb];       % Lower limits
        Opt.ub =    [Opt.ub Rub];       % Upper Limits
        Opt.Scale = [Opt.Scale RScale]; % Scaler for upper and lower bounds
    end
end


%% Optimisation Parameters
Opt.intcon = [];                     % Define which channels are integers
Opt.nD =    length(Opt.DimNames);
Opt.MaxEvals = 110000;
Opt.MinSampleDistance = 0.0000001;
Opt.Folder = [Proj.RootFolder{1} '\SMA_RPhase_Model_01\Optimisation_Data\'];
Opt.dt = mean(diff(TDs.Temp.Time));
Opt.minRandom = 1000;
Opt.ObjTarget = 1e-5;

for i = 1:Opt.nD
    Opt.InitArray(i) = Opt.InitArray(i)/Opt.Scale(i);
end

Opt.Aeq = [];
Opt.beq = [];

Opt.nCons = 3;
Opt.A = zeros(Opt.nCons, Opt.nD);
Opt.b = zeros(Opt.nCons, 1);
% Opt.A(1, 2) = 1;    Opt.A(1, 5) = -1;   % 1: MS <= A1F
% Opt.A(2, 3) = 1;    Opt.A(2, 4) = -1;   % 2: MF <= A1S
% Opt.A(3, 3) = 1;    Opt.A(3, 5) = -1;   % 3: MF <= A1F

Opt.nCat = 3;

%% Prepare temp Data store
filename = ['Opt_Data_' startTime '.mat'];

for i = 1:Opt.nD
    fieldName = strrep(Opt.DimNames{i},'.','_');
    stats.(fieldName) = Opt.InitArray(i);
end
save([Opt.Folder filename],'stats','Opt','D','-v7.3')

%% Cost function Handle
costFcnHandle = @(x) CostFuncRPhaseMaterial(P,filename, mdlName, Opt, D, x);

%% Run Optimisation
try
    %     opts = optimoptions( 'surrogateopt', 'Display', 'iter', 'MinSurrogatePoints',Opt.minRandom,'MaxFunctionEvaluations', Opt.MaxEvals, 'PlotFcn', 'surrogateoptplot','UseParallel', true, 'MinSampleDistance', Opt.MinSampleDistance, 'InitialPoints', Opt.InitArray);
    if CrashRestart
        opts = optimoptions( 'surrogateopt', 'Display', 'iter', 'MinSurrogatePoints',Opt.minRandom,'MaxFunctionEvaluations', Opt.MaxEvals, 'PlotFcn', 'surrogateoptplot','UseParallel', true, 'MinSampleDistance', Opt.MinSampleDistance,'ObjectiveLimit',Opt.ObjTarget,'CheckpointFile', CrashFile);
        [paramsOpt,fval,exitflag,output,trials] = surrogateopt(CrashFile,opts);
    else
        opts = optimoptions( 'surrogateopt', 'Display', 'iter', 'MinSurrogatePoints',Opt.minRandom,'MaxFunctionEvaluations', Opt.MaxEvals, 'PlotFcn', 'surrogateoptplot','UseParallel', true, 'MinSampleDistance', Opt.MinSampleDistance,'ObjectiveLimit',Opt.ObjTarget, 'InitialPoints', Opt.InitArray );
        %
        [paramsOpt,fval,exitflag,output,trials] = surrogateopt( costFcnHandle, Opt.lb, Opt.ub, Opt.intcon, Opt.A, Opt.b, Opt.Aeq, Opt.beq, opts);
        set_param( mdlName, 'FastRestart', 'on' );
    end
    %     ,'CheckpointFile', ['OptTemp_' startTime '.mat']
catch MExc
    MExc.throwAsCaller();
    set_param( mdlName, 'FastRestart', 'off' );
end

%% Save Results
save([Opt.Folder 'Optimisation_' startTime],'Opt','paramsOpt','fval','exitflag','output','trials')

%% Plot Results
Plot_Optimisation_dscatter

% figure
% hold on
% plot((out.NiTi.Strain.Data - (out.NiTi.Strain.Data(1))) .*100,out.Stress.Data ./1e6,'DisplayName','Sim')
% plot(TDs.Strain.Data,TDs.Stress.Data ./1e6, 'DisplayName', 'Test')
% legend
% title('NiTiCuV Strain vs Stress')
% xlabel('Strain [%]')
% ylabel('Stress [MPa]')

%% Functions
function h = plotInputData(TDs)
a = gobjects(0);



h = figure('name','Input Summary','NumberTitle','off','units','normalized','outerposition',[0 0.02 1 0.98]);
t = tiledlayout(2,2);
a = nexttile;
plot(TDs.Force)

%     a(2) = subplot(m,n,2);
%     plot(TDs.Stress)
%
%     a(3) = subplot(m,n,3);
%     plot(TDs.Displacement)
%
%     a(4) = subplot(m,n,4);
%     plot(TDs.Strain)

a(end+1) = nexttile;
plot(TDs.Temp)

a(end+1) = nexttile;
plot(TDs.TransDir)

%     subplot(m,n,7)
%     scatter(squeeze(TDs.Stress.Data)*1e-6, squeeze(TDs.Strain.Data))
%     xlabel('Stress [MPa]')
%     ylabel('Strain [%]')
%     title('Stress - Strain')

linkaxes(a,'x')
end