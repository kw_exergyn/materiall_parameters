clear all
close all

% Get Them parameters
[O,P] =  getDefaultParamsRPhase(0);


startTime = datestr(now,'YYYY-mm-DD_HH-MM');
Proj = matlab.project.rootProject;

%% Get Model, save it and set fast restart on.
% mdlName = 'MaterialOptimisation';
mdlName = 'MaterialOptimisationRphase_04';
load_system(mdlName);

if string( get_param( mdlName, 'Dirty') ) == "on"
    save_system( mdlName );
end
 set_param( mdlName, 'FastRestart', 'off' );

c = parpool('cluster R2023a');
files = dependencies.fileDependencyAnalysis(mdlName);
files{end+1} = 'C:\Users\keith\Documents\Simulation\Material_Parameters\Subsystems\+SimscapeCustomBlocks\+SMA\SmoothHardeningSMA_RPhase_01_NoMass.ssc';
files{end+1} ='C:\Users\keith\Documents\Simulation\Material_Parameters\Subsystems\+SimscapeCustomBlocks\+Thermal\variable_conduction.ssc';
c.addAttachedFiles(files);

CrashRestart = 0;
CrashFile = '';
P.DSC =0;
%% Load data
if  P.DSC == 0

    load('10deg.mat')
    D.T1 = TDs;
    load('30deg.mat')
    D.T2 = TDs;
    load('60deg.mat')
    D.T3 = TDs;
    % load('40deg.mat')
    % D.T4 = TDs;
    % load('50deg.mat')
    % D.T5 = TDs;
    % load('60deg.mat')
    % D.T6 = TDs;


%     load("30deg.mat")
%     D.T4 = TDs;
%      load("20deg.mat")
%      D.T2 = TDs;
    


else
    load('MaterialOptimiserData_NiTiCr_DSC.mat')
end
% load('MaterialOptimiserData_SMA08.mat')
h = plotInputData(TDs);

name = 'sb_Core_P';
tempStruct = Simulink.Bus.createObject(P);
eval([name ' = evalin(''base'',tempStruct.busName);']);

name = 'sb_Core_O';
tempStruct = Simulink.Bus.createObject(O);
eval([name ' = evalin(''base'',tempStruct.busName);']);

%% Configure Parameters to Optimise
Opt.Name = 'DA_Niti_01A';

Opt.TestNum = '00';

Opt.DimNames{1} = 'P.NiTi.M.Hcur';
Opt.DimNames{end+1} = 'P.NiTi.M.MS';
Opt.DimNames{end+1} = 'P.NiTi.M.MF';
Opt.DimNames{end+1} = 'P.NiTi.M.A1S';
Opt.DimNames{end+1} = 'P.NiTi.M.A1F';
Opt.DimNames{end+1} = 'P.NiTi.M.N1';
Opt.DimNames{end+1} = 'P.NiTi.M.N2';
Opt.DimNames{end+1} = 'P.NiTi.M.N3';
Opt.DimNames{end+1} = 'P.NiTi.M.N4';
Opt.DimNames{end+1} = 'P.NiTi.M.CA1';
Opt.DimNames{end+1} = 'P.NiTi.M.EA1';
Opt.DimNames{end+1} = 'P.NiTi.M.EM';
Opt.DimNames{end+1} = 'P.NiTi.M.y';
Opt.DimNames{end+1} = 'P.NiTi.M.C_';
% Opt.DimNames{end+1} = 'P.NiTi.M.CalS';

Opt.DimNames{end+1} = 'P.NiTi.R.Hcur';
Opt.DimNames{end+1} = 'P.NiTi.R.RS';
Opt.DimNames{end+1} = 'P.NiTi.R.RF';
Opt.DimNames{end+1} = 'P.NiTi.R.A2S';
Opt.DimNames{end+1} = 'P.NiTi.R.A2F';
Opt.DimNames{end+1} = 'P.NiTi.R.N1';
Opt.DimNames{end+1} = 'P.NiTi.R.N2';
Opt.DimNames{end+1} = 'P.NiTi.R.N3';
Opt.DimNames{end+1} = 'P.NiTi.R.N4';
Opt.DimNames{end+1} = 'P.NiTi.R.CA2';
Opt.DimNames{end+1} = 'P.NiTi.R.EA2';
Opt.DimNames{end+1} = 'P.NiTi.R.ER';
% Opt.DimNames{end+1} = 'P.NiTi.R.y';
% Opt.DimNames{end+1} = 'P.NiTi.R.C_';
% Opt.DimNames{end+1} = 'P.NiTi.R.C_';
% Opt.DimNames{end+1} = 'P.NiTi.R.CalS';


Opt.Units{1} = 'm/m';
Opt.Units{end+1} = 'K';
Opt.Units{end+1} = 'K';
Opt.Units{end+1} = 'K';
Opt.Units{end+1} = 'K';
Opt.Units{end+1} = '';
Opt.Units{end+1} = '';
Opt.Units{end+1} = '';
Opt.Units{end+1} = '';
Opt.Units{end+1} = 'Pa/K';
Opt.Units{end+1} = 'Pa';
Opt.Units{end+1} = 'Pa';
Opt.Units{end+1} = '';
Opt.Units{end+1} = '';
% Opt.Units{end+1} = 'Pa';

Opt.Units{end+1} = 'm/m';
Opt.Units{end+1} = 'K';
Opt.Units{end+1} = 'K';
Opt.Units{end+1} = 'K';
Opt.Units{end+1} = 'K';
Opt.Units{end+1} = '';
Opt.Units{end+1} = '';
Opt.Units{end+1} = '';
Opt.Units{end+1} = '';
Opt.Units{end+1} = 'Pa/K';
Opt.Units{end+1} = 'Pa';
Opt.Units{end+1} = 'Pa';
% Opt.Units{end+1} = '';
% Opt.Units{end+1} = '';
% Opt.Units{end+1} = 'Pa';


% % SMA
% %Martensite
% Opt.InitArray(1)     =	460; % [J/kg.K]
% Opt.InitArray(end+1) =	460; % [J/kg.K]
% Opt.InitArray(end+1) =	0.034999; % [m/m]
% Opt.InitArray(end+1) =	316.0593; % [K]
% Opt.InitArray(end+1) =	176.8131; % [K]
% Opt.InitArray(end+1) =	222.67; % [K]
% Opt.InitArray(end+1) =	306.543; % [K]
% Opt.InitArray(end+1) =	0.7801; % []
% Opt.InitArray(end+1) =	0.30701; % []
% Opt.InitArray(end+1) =	0.59577; % []
% Opt.InitArray(end+1) =	0.35138; % []
% Opt.InitArray(end+1) =	10499874.91; % [Pa/K]
% Opt.InitArray(end+1) =	46121675277.2479; % [Pa]
% Opt.InitArray(end+1) =	69486505155.0111; % [Pa]
% % Opt.InitArray(end+1) =	4.1157; % []
% % Opt.InitArray(end+1) =	0.37318; % []
% % Opt.InitArray(end+1) =	145724785.9907; % [Pa]
%
% %Austenite
% Opt.InitArray(end+1) =	0.034999; % [m/m]
% Opt.InitArray(end+1) =	316.0593; % [K]
% Opt.InitArray(end+1) =	176.8131; % [K]
% Opt.InitArray(end+1) =	222.67; % [K]
% Opt.InitArray(end+1) =	306.543; % [K]
% Opt.InitArray(end+1) =	0.7801; % []
% Opt.InitArray(end+1) =	0.30701; % []
% Opt.InitArray(end+1) =	0.59577; % []
% Opt.InitArray(end+1) =	0.35138; % []
% Opt.InitArray(end+1) =	10499874.91; % [Pa/K]
% Opt.InitArray(end+1) =	46121675277.2479; % [Pa]
% % Opt.InitArray(end+1) =	69486505155.0111; % [Pa]
% % Opt.InitArray(end+1) =	4.1157; % []
% % Opt.InitArray(end+1) =	0.37318; % []
% % Opt.InitArray(end+1) =	145724785.9907; % [Pa]

% Opt.InitArray(1)     =	469.6268; % [J/kg.K]
% Opt.InitArray(end+1) =	559.5179; % [J/kg.K]

Opt.InitArray(1)     =	0.03696; % [m/m]
Opt.InitArray(end+1) =	289.3535; % [K]
Opt.InitArray(end+1) =	228.6008; % [K]
Opt.InitArray(end+1) =	246.0136; % [K]
Opt.InitArray(end+1) =	293.6631; % [K]
Opt.InitArray(end+1) =	0.27408; % []
Opt.InitArray(end+1) =	0.27476; % []
Opt.InitArray(end+1) =	0.12985; % []
Opt.InitArray(end+1) =	0.25595; % []
Opt.InitArray(end+1) =	8971928.8251; % [Pa/K]
Opt.InitArray(end+1) =	50069703783.6368; % [Pa]
Opt.InitArray(end+1) =	69836591498.1017; % [Pa]
Opt.InitArray(end+1) =	2.2487; % []
Opt.InitArray(end+1) =	0.54648; % []
Opt.InitArray(end+1) =	0.010652; % [m/m]
Opt.InitArray(end+1) =	311.2613; % [K]
Opt.InitArray(end+1) =	199.4583; % [K]
Opt.InitArray(end+1) =	250.5619; % [K]
Opt.InitArray(end+1) =	400.8606; % [K]
Opt.InitArray(end+1) =	0.18841; % []
Opt.InitArray(end+1) =	0.1604; % []
Opt.InitArray(end+1) =	0.14284; % []
Opt.InitArray(end+1) =	0.21632; % []
Opt.InitArray(end+1) =	10e6; % [Pa/K]
Opt.InitArray(end+1) =	41329083139.3629; % [Pa]
Opt.InitArray(end+1) =	32651692783.6518; % [Pa]

Opt.lb =    [ 2    27 18  22  27  0.1    0.1    0.1    0.1    6    30 30 2   0.1  0.1   27 16  22  37   0.1  0.1  0.1   0.1  4  30 30  ];       % Lower limits
Opt.ub =    [ 5    30 25  26  30  0.3    0.3    0.35   0.3    10   90 90 6    1     2   35 23  32  40   0.3  0.3  0.3   0.3  12 90 90 ];        % Upper Limits

%% Optimisation Parameters
Opt.Scale = [1e-2 10 10 10 10 1 1 1 1 1e6 1e9 1e9 1 1  1e-2 10 10 10 10 1 1 1 1 1e6 1e9 1e9 ];         % Define any scaling to apply to the channels
Opt.intcon = [];                     % Define which channels are integers
Opt.nD =    length(Opt.DimNames);
Opt.MaxEvals = 110000;
Opt.MinSampleDistance = 0.00001;
Opt.Folder = [Proj.RootFolder{1} '\SMA_RPhase_Model_01\Optimisation_Data\'];
Opt.dt = mean(diff(TDs.Temp.Time));
Opt.minRandom = 500;
Opt.ObjTarget = 1e-5;
% Opt.EnergyDensityTargetHeat1 = 15.2;    % kJ/kg
% Opt.EnergyDensityTargetHeat2 = 16.5;    % kJ/kg
% Opt.EnergyDensityTargetHeat3 = 17;    % kJ/kg
% Opt.EnergyDensityTargetHeat4 = 17.6;    % kJ/kg
% Opt.EnergyDensityTargetHeat5 = 17.2;    % kJ/kg
% Opt.EnergyDensityTargetHeat6 = 17.1;    % kJ/kg

Opt.EnergyDensityTargetHeat1 = 15.2;    % kJ/kg
Opt.StrainOffset1 = 0.25;
Opt.EnergyDensityTargetHeat2 = 17;    % kJ/kg
Opt.StrainOffset2 = 0.25;
Opt.EnergyDensityTargetHeat3 = 17.1;    % kJ/kg
Opt.StrainOffset3 = 0.25;

% Opt.EnergyDensityTargetCool1 = 14.3;    % kJ/kg
% Opt.EnergyDensityTargetCool2 = 15.6;    % kJ/kg
% Opt.EnergyDensityTargetCool3 = 16.1;    % kJ/kg
% Opt.EnergyDensityTargetCool4 = 16.8;    % kJ/kg
% Opt.EnergyDensityTargetCool5 = 16.4;    % kJ/kg
% Opt.EnergyDensityTargetCool6 = 16.3;    % kJ/kg

Opt.EnergyDensityTargetCool1 = 14.3;    % kJ/kg
Opt.EnergyDensityTargetCool2 = 16.1;    % kJ/kg
Opt.EnergyDensityTargetCool3 = 16.3;    % kJ/kg

% Opt.EnergyDensityTargetCool = 0;    % kJ/kg


for i = 1:Opt.nD
    Opt.InitArray(i) = Opt.InitArray(i)/Opt.Scale(i);
end

Opt.Aeq = [];
Opt.beq = [];

Opt.nCons = 6;
Opt.A = zeros(Opt.nCons, Opt.nD);
Opt.b = zeros(Opt.nCons, 1);
Opt.A(1, 2) = 1;    Opt.A(1, 5) = -1;   % 1: MS <= A1F
Opt.A(2, 3) = 1;    Opt.A(2, 4) = -1;   % 2: MF <= A1S
Opt.A(3, 3) = 1;    Opt.A(3, 5) = -1;   % 3: MF <= A1F
Opt.A(4, 17) = 1;   Opt.A(4, 16) = -1;  % 4: RF <= RS
Opt.A(5, 18) = 1;   Opt.A(5, 19) = -1;  % 4: A2S <= A2F
% Opt.A(4, 16) = 1;   Opt.A(4, 19) = -1;  % 4: RS <= A2F
% Opt.A(5, 17) = 1;   Opt.A(5, 18) = -1;  % 5: RF <= A2S
% Opt.A(6, 17) = 1;   Opt.A(6, 19) = -1;  % 6: RF <= A2F

% c = 0.1;
% Opt.A(7, 11) = 1;    Opt.A(7, 25) = -(1+c);  % 7: EA1 <= (1+c)EA2
% Opt.A(8, 25) = 1-c; Opt.A(8, 11) = -1;       % 8: (1-c)EA2 <= EA1
% Opt.A(9, 11) = 1;    Opt.A(9, 12) = -(1+c);   % 9: EA1 <= (1+c)EM
% Opt.A(10, 12) = 1-c; Opt.A(10, 11) = -1;      % 10: (1-c)EM <= EA1

Opt.A(6, 1) = -1;   Opt.A(6, 15) = 1;        % 9: R.Hcur <= M.Hcur

% Opt.A = [];
% Opt.b = [];

%% Prepare temp Data store
filename = ['Opt_Data_' startTime '.mat'];
% stats.cost = 0;
% % stats.StrainError = 0;
% stats.Yo = 0;
% % stats.HeatingError = 0;
% % stats.CoolingError = 0;
% % stats.Heating = 0;
% % stats.Cooling = 0;

for i = 1:Opt.nD
    fieldName = strrep(Opt.DimNames{i},'.','_');
    stats.(fieldName) = Opt.InitArray(i);
end
save([Opt.Folder filename],'stats','Opt','D','-v7.3')

%% Cost function Handle
costFcnHandle = @(x) CostFuncRPhaseMaterial(P,filename, mdlName, Opt, D, x);

%% Run Optimisation
try
    %     opts = optimoptions( 'surrogateopt', 'Display', 'iter', 'MinSurrogatePoints',Opt.minRandom,'MaxFunctionEvaluations', Opt.MaxEvals, 'PlotFcn', 'surrogateoptplot','UseParallel', true, 'MinSampleDistance', Opt.MinSampleDistance, 'InitialPoints', Opt.InitArray);
    if CrashRestart
        opts = optimoptions( 'surrogateopt', 'Display', 'iter', 'MinSurrogatePoints',Opt.minRandom,'MaxFunctionEvaluations', Opt.MaxEvals, 'PlotFcn', 'surrogateoptplot','UseParallel', true, 'MinSampleDistance', Opt.MinSampleDistance,'ObjectiveLimit',Opt.ObjTarget,'CheckpointFile', CrashFile);
        [paramsOpt,fval,exitflag,output,trials] = surrogateopt(CrashFile,opts);
    else
        opts = optimoptions( 'surrogateopt', 'Display', 'iter', 'MinSurrogatePoints',Opt.minRandom,'MaxFunctionEvaluations', Opt.MaxEvals, 'PlotFcn', 'surrogateoptplot','UseParallel', true, 'MinSampleDistance', Opt.MinSampleDistance,'ObjectiveLimit',Opt.ObjTarget, 'InitialPoints', Opt.InitArray );
        %
        [paramsOpt,fval,exitflag,output,trials] = surrogateopt( costFcnHandle, Opt.lb, Opt.ub, Opt.intcon, Opt.A, Opt.b, Opt.Aeq, Opt.beq, opts);
        set_param( mdlName, 'FastRestart', 'on' );
    end
    %     ,'CheckpointFile', ['OptTemp_' startTime '.mat']
catch MExc
    MExc.throwAsCaller();
    set_param( mdlName, 'FastRestart', 'off' );
end

%% Save Results
save([Opt.Folder 'Optimisation_' startTime],'Opt','paramsOpt','fval','exitflag','output','trials')

%% Plot Results
Plot_Optimisation_Basic

% figure
% hold on
% plot((out.NiTi.Strain.Data - (out.NiTi.Strain.Data(1))) .*100,out.Stress.Data ./1e6,'DisplayName','Sim')
% plot(TDs.Strain.Data,TDs.Stress.Data ./1e6, 'DisplayName', 'Test')
% legend
% title('NiTiCuV Strain vs Stress')
% xlabel('Strain [%]')
% ylabel('Stress [MPa]')

%% Functions
function h = plotInputData(TDs)
a = gobjects(0);



h = figure('name','Input Summary','NumberTitle','off','units','normalized','outerposition',[0 0.02 1 0.98]);
t = tiledlayout(2,2);
a = nexttile;
plot(TDs.Force)

%     a(2) = subplot(m,n,2);
%     plot(TDs.Stress)
%
%     a(3) = subplot(m,n,3);
%     plot(TDs.Displacement)
%
%     a(4) = subplot(m,n,4);
%     plot(TDs.Strain)

a(end+1) = nexttile;
plot(TDs.Temp)

a(end+1) = nexttile;
plot(TDs.TransDir)

%     subplot(m,n,7)
%     scatter(squeeze(TDs.Stress.Data)*1e-6, squeeze(TDs.Strain.Data))
%     xlabel('Stress [MPa]')
%     ylabel('Strain [%]')
%     title('Stress - Strain')

linkaxes(a,'x')
end