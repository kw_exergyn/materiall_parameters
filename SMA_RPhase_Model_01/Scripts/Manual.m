% close all
clear
% Get Them parameters
[O,P] =  getDefaultParamsRPhase(0);


startTime = datestr(now,'YYYY-mm-DD_HH-MM');
Proj = matlab.project.rootProject;

%% Get Model, save it and set fast restart on.
% mdlName = 'MaterialOptimisation';
mdlName = 'MaterialOptimisationRphase_04';
load_system(mdlName);

if string( get_param( mdlName, 'Dirty') ) == "on"
    save_system( mdlName );
end

set_param( mdlName, 'FastRestart', 'off' );
P.NiTi.Offset =  0;

% P.NiTi.M.Hcur	    =	0.03; % [m/m]
% P.NiTi.M.MS	    =	268.15+P.NiTi.Offset; % [K]
% P.NiTi.M.MF	    =	188.15+P.NiTi.Offset; % [K]
% P.NiTi.M.A1S	    =	203.15+P.NiTi.Offset; % [K]
% P.NiTi.M.A1F	    =	283.15+P.NiTi.Offset; % [K]
% P.NiTi.M.N1	    =	0.2; % []
% P.NiTi.M.N2	    =	0.2; % []
% P.NiTi.M.N3	    =	0.2; % []
% P.NiTi.M.N4	    =	0.2; % []
% P.NiTi.M.CA1	    =	10.6e6; % [Pa/K]
% P.NiTi.M.CM  	    =	10.6e6; % [Pa/K]
% P.NiTi.M.EA1 	    =	65e9; % [Pa]
% P.NiTi.M.EM	    =	55e9; % [Pa]
% P.NiTi.M.y  	    =	20; % []
% P.NiTi.M.C_ 	    =	0.3; % []
% P.NiTi.M.CalS	    =	1; % [Pa]
% P.NiTi.M.CPA1     = 460; %[J/kg.K]
% P.NiTi.M.CPM      = 460; %[J/kg.K]
% 
% P.NiTi.M.Hcur	=	0.043652; % [m/m]
% P.NiTi.M.MS	=	286.2499; % [K]
% P.NiTi.M.MF	=	195.8146; % [K]
% P.NiTi.M.A1S	=	233.6609; % [K]
% P.NiTi.M.A1F	=	286.2681; % [K]
% P.NiTi.M.N1	=	0.3; % []
% P.NiTi.M.N2	=	0.14532; % []
% P.NiTi.M.N3	=	0.2935; % []
% P.NiTi.M.N4	=	0.21321; % []
% P.NiTi.M.CA1	=	7502536.5478; % [Pa/K]
% P.NiTi.M.EA1	=	34002396041.5241; % [Pa]
% P.NiTi.M.EM	=	63564717945.7404; % [Pa]
% P.NiTi.M.y	=	5.8917; % []
% P.NiTi.M.C_	=	0.73059; % []
% P.NiTi.R.Hcur	=	0.0099918; % [m/m]
% P.NiTi.R.RS	=	293.6558; % [K]
% P.NiTi.R.RF	=	185.1627; % [K]
% P.NiTi.R.A2S	=	236.4648; % [K]
% P.NiTi.R.A2F	=	376.7904; % [K]
% P.NiTi.R.N1	=	0.10902; % []
% P.NiTi.R.N2	=	0.12204; % []
% P.NiTi.R.N3	=	0.18498; % []
% P.NiTi.R.N4	=	0.11121; % []
% P.NiTi.R.CA2	=	8434306.2094; % [Pa/K]
% P.NiTi.R.EA2	=	37688955537.869; % [Pa]
% P.NiTi.R.ER	=	66338808234.6298; % [Pa]


P.NiTi.M.Hcur	=	0.0397; % [m/m]
P.NiTi.M.MS	=	288.7501; % [K]
P.NiTi.M.MF	=	211.83; % [K]
P.NiTi.M.A1S	=	239.289; % [K]
P.NiTi.M.A1F	=	295.2221; % [K]
P.NiTi.M.N1	=	0.28538; % []
P.NiTi.M.N2	=	0.20438; % []
P.NiTi.M.N3	=	0.13846; % []
P.NiTi.M.N4	=	0.26217; % []
P.NiTi.M.CA1	=	8312570.7589; % [Pa/K]
P.NiTi.M.EA1	=	35339022934.3185; % [Pa]
P.NiTi.M.EM	=	56164209514.7298; % [Pa]
P.NiTi.M.y	=	4.0696; % []
P.NiTi.M.C_	=	0.69225; % []
P.NiTi.R.Hcur	=	0.01602; % [m/m]
P.NiTi.R.RS	=	279.7565; % [K]
P.NiTi.R.RF	=	166.4423; % [K]
P.NiTi.R.A2S	=	224.5687; % [K]
P.NiTi.R.A2F	=	372.0992; % [K]
P.NiTi.R.N1	=	0.29296; % []
P.NiTi.R.N2	=	0.20715; % []
P.NiTi.R.N3	=	0.18774; % []
P.NiTi.R.N4	=	0.15198; % []
P.NiTi.R.CA2	=	6859823.8482; % [Pa/K]
P.NiTi.R.EA2	=	35266469134.7681; % [Pa]
P.NiTi.R.ER	=	64965503305.1278; % [Pa]


% P.NiTi.R.ER = P.NiTi.M.EA1;
P.NiTi.M.CM = P.NiTi.M.CA1;
P.NiTi.R.CR = P.NiTi.R.CA2;

load('50deg.mat')
    D.T2 = TDs;
Opt.StrainOffset = 0.25;
% load("35deg.mat")
% D.T7 = TDs;

Opt.dt = mean(diff(TDs.Temp.Time));
Opt.EnergyDensityTargetHeat1 = 15.2;    % kJ/kg
Opt.EnergyDensityTargetHeat2 = 17;    % kJ/kg
Opt.EnergyDensityTargetHeat3 = 17.1;    % kJ/kg

Opt.EnergyDensityTargetCool1 = 14.3;    % kJ/kg
Opt.EnergyDensityTargetCool2 = 16.1;    % kJ/kg
Opt.EnergyDensityTargetCool3 = 16.3;    % kJ/kg

a =2;

eval(['TDs = D.T', num2str(a)])
eval(['Opt.EnergyDensityTargetHeat = Opt.EnergyDensityTargetHeat',num2str(a) ]);    % kJ/kg;    % kJ/kg
eval(['Opt.EnergyDensityTargetCool = Opt.EnergyDensityTargetCool',num2str(a) ]);    % kJ/kg;    % kJ/kg
B = TDs.Temp.Data;
B =cat(1,B,TDs.Temp.Data);
C = (0:mean(diff(TDs.Temp.Time)):((TDs.Temp.Time(end)*2)))';

Inputs.Temp = timeseries((B(1:end-1)),C);
Inputs.Temp.Data(1:end) = Inputs.Temp.Data(1);
% Inputs.Temp.Data(1:end) = Inputs.Temp.Data(1);

B = TDs.Force.Data;
B =cat(1,B,TDs.Force.Data);
Inputs.Force = timeseries(B(1:end-1),C);
% Inputs.Force = timeseries(TDs.Force.Data,TDs.Temp.Time);
Inputs.Force.Data(1:20) = -100;

B = TDs.TransDir.Data;
B =cat(1,B,TDs.TransDir.Data);
Inputs.TransDir = timeseries(B(1:end-1),C);
% Inputs.TransDir = timeseries(TDs.TransDir.Data,TDs.Temp.Time);

%% Get Parameters

P.Sim.logStep = Opt.dt;
P.Sim.tStep = Opt.dt;                   % Time Step [s]
% P.Sim.tStep = 0.01;
P.Sim.maxTime = TDs.Temp.Time(end)*2;
P.showPlots = 0;

P.FDS.InitTemp = mean(TDs.Temp.Data(1:10))-273.15;
P.FDS.Tmid = P.FDS.InitTemp;
P.FDS.AmbientTemp = 16;
P.FDS.FluidPressure = 4;
P.NiTi.ForceInt = 0;

%     P.InitTemp = P.FDS.InitTemp;


% P.NiTi.R.ER = P.NiTi.M.EA1;
% P.NiTi.M.CM = P.NiTi.M.CA1;
% P.NiTi.R.CR = P.NiTi.R.CA2;

% P.NiTi.R.CPR = P.NiTi.M.CPA1;
% P.NiTi.R.CPA2 = P.NiTi.M.CPA1;

% P.DSC = 1;
RPhaseMat
%     Params_Core_New_DSC

name = 'sb_Core_P';
tempStruct = Simulink.Bus.createObject(P);
eval([name ' = evalin(''base'',tempStruct.busName);']);

name = 'sb_Core_O';
tempStruct = Simulink.Bus.createObject(O);
eval([name ' = evalin(''base'',tempStruct.busName);']);

name = 'sb_Core_Inputs';
tempStruct = Simulink.Bus.createObject(Inputs);
eval([name ' = evalin(''base'',tempStruct.busName);']);


%% Create Simulink Workspace Files
simIn = Simulink.SimulationInput(mdlName);
simIn = simIn.setVariable('P',P);
simIn = simIn.setVariable('O',O);
simIn = simIn.setVariable('Inputs',Inputs);
simIn = simIn.setVariable('sb_Core_P',sb_Core_P);
simIn = simIn.setVariable('sb_Core_O',sb_Core_O);

out = sim(simIn);

%%
P.DSC= 0;
   [cost,StrainError,HeatError,WorkError]  = costCalcMaterialRPhase(out, TDs, Opt,P);
ida = length(TDs.Temp.Data);
disp(['Strain Error = ', num2str(StrainError)])
disp(['Heat Error = ', num2str(HeatError)])
disp(['WorkError = ', num2str(WorkError)])
figure
hold on
plot(((out.NiTi.Strain.Data(ida:end) - (out.NiTi.Strain.Data(ida))) .*100)-0.25,out.Stress.Data(ida:end) ./1e6,'DisplayName','Sim')
plot(TDs.Strain.Data,TDs.Stress.Data ./1e6, 'DisplayName', 'Test')
legend
title('Ni51p6Ti Strain vs Stress')
xlabel('Strain [%]')
ylabel('Stress [MPa]')

