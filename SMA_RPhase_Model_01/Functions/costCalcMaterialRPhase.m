function [cost,StrainError,HeatError,WorkError,Eout, CoolOut, WorkIn, WorkOut, WorkSim] = costCalcMaterialRPhase(out, TDs, Opt,P)
% Calculate the cost of a given run
% Select the last run
ida = length(TDs.Temp.Data);

if isfield(P.Stack, 'nSeg')
    if P.Stack.nSeg > 1
        % Combined the three NiTi pieces into one strucutre (hopefully)
        P.Stack.Mass = P.Stack.Mass*P.Stack.nSeg;
        P.Stack.Height = P.Stack.Height*P.Stack.nSeg;
        
        vars = fields(out.NiTi1);
        nv = length(vars);
        out.NiTi = out.NiTi1;
        for j = 1:nv
            var = vars{j};    
            for i = 2:P.Stack.nSeg
                niti = ['NiTi' num2str(i)];
                out.NiTi.(var).Data = out.NiTi.(var).Data + out.(niti).(var).Data;
            end
            out.NiTi.(var).Data = out.NiTi.(var).Data/P.Stack.nSeg;
        end
        out.NiTi.Qout.Data = out.NiTi.Qout.Data*P.Stack.nSeg;
    end
end

if P.DSC
    
    
%     tstep = mean(diff((out.NiTi.Qout.Time)));
    
    Q = squeeze((out.Q.Data(ida:end))/(P.Stack.Mass))/1000;
    power = squeeze((out.Q.Data(ida:end))/(P.Stack.Mass))/1000;
        
    power(power == 0) =nan;
    power(power > 0) = 1;
    Q = Q(1:end) .* power;
   
    DSCPower = abs(TDs.Power.Data);
    DSCPower(DSCPower == 0) =nan;
    DSCPower(DSCPower > 0) = 1;
    
    DSCQ =   TDs.Power.Data .*DSCPower;
    HeatError = abs(DSCQ-Q);
%     idx1 = round(length(Eout)/2);
%     Eout = max(Eout(1:idx1))-min(Eout(1:idx1));
%     HeatError = abs(TDs.Energy.Data-Eout);
    
    cost = double((rms(HeatError,'omitnan')));
    
    if P.NiTi.M.Y0 <=0 || P.NiTi.R.Y0 <=0 || P.NiTi.M.A1F < P.NiTi.M.MS || P.NiTi.R.A2F < P.NiTi.R.RS
        cost = 20;
    end
%     if cost >20
%         cost =20;
%     end
     
else
    
    try
        simStrain = out.Strain.Data(ida:end)*100;
    catch
        try
            simStrain = squeeze(out.NiTi.NiTi.Strain.Data(ida:end))*100;
        catch
            simStrain = out.NiTi.Strain.Data(ida:end)*100;
        end
    end
    simTime = out.NiTi.Strain.Time;
    simStrain = (simStrain-simStrain(1))+Opt.StrainOffset;
    SimVel = [0; abs(diff(simStrain))];
    SimVel(SimVel < 0.5e-3) =nan;
    SimVel(SimVel >= 0.5e-3) = 1;
    SimVel(simStrain>-0.3) = nan;

    simStrain = simStrain .* SimVel;
    
    Stress = squeeze(TDs.Stress.Data);
    
    MeasStrain = squeeze(TDs.Strain.Data);
    MeasVel = [0; abs(diff(MeasStrain))];
    MeasVel(MeasVel < 0.5e-3) =nan;
    MeasVel(MeasVel >= 0.5e-3) = 1;
    MeasVel(MeasStrain>-0.3) = nan;

    MeasStrain = MeasStrain .* MeasVel;
    
    A = (MeasStrain-simStrain);

    StrainError = double((rms(A,'omitnan')))/mean(abs(MeasStrain), 'omitnan');
    
    StrainErrorCheck =isnan(StrainError);
    if StrainErrorCheck
        StrainError = 5;
    end
    
    tstep = mean(diff(out.NiTi.Qout.Time));
    
    % heatTarget = 15;        % Target Qout [kJ]
    Eout = cumsum((out.Q.Data(ida:end)/(P.Stack.Mass))*tstep)/1000;
    idx1 = round(length(Eout)/2);
    CoolOut = max(Eout(idx1:end))-min(Eout(idx1:end));
    Eout = max(Eout(1:idx1))-min(Eout(1:idx1));
    HeatError = abs(Opt.EnergyDensityTargetHeat-Eout)/Opt.EnergyDensityTargetHeat;

    % WorkMeas = Opt.EnergyDensityTargetHeat - Opt.EnergyDensityTargetCool;
    force = out.NiTi.Stress.Data(ida:end)*P.Stack.Area*10^-6;
    displacement = out.NiTi.Strain.Data(ida:end)*P.Stack.Height*1e-3;
    vel = [0; diff(displacement)]/Opt.dt;
    pwr = vel.*force;
    energy = cumsum(pwr)*Opt.dt;
    specEnergy = energy/P.Stack.Mass*1e-3;
    WorkIn = max(specEnergy);
    WorkSim = specEnergy(end);
    WorkOut = WorkIn-WorkSim;
    % WorkSim = (polyarea(out.NiTi.Strain.Data(ida:end),out.NiTi.Stress.Data(ida:end))/P.NiTi.Dens)/1000;
    WorkError = 0; % Needs calculating if used for anything
    if Opt.EnergyDensityTargetHeat >0
        
        cost = 50*StrainError + 50*HeatError ;
        
         % cost = StrainError;
    else
        cost = StrainError;
    end
    
    if P.NiTi.M.Y0 <=0 || P.NiTi.R.Y0 <=0 
%         || P.NiTi.M.A1F < P.NiTi.M.MS || P.NiTi.R.A2F < P.NiTi.R.RS
        cost = cost + 2;
    end
end

% figure
% hold on
% plot((out.NiTi.Strain.Data(ida:end) - (out.NiTi.Strain.Data(ida))) .*100,out.Stress.Data(ida:end) ./1e6,'DisplayName','Sim')
% plot(TDs.Strain.Data,TDs.Stress.Data ./1e6, 'DisplayName', 'Test')
% legend
% title('NiTiCuV Strain vs Stress')
% xlabel('Strain [%]')
% ylabel('Stress [MPa]')


%% Write stats to file
% stats.cost = cost;
% stats.StrainError = StrainError;
% stats.Yo = P.NiTi.M.Y0;
% stats.HeatError = HeatError;
