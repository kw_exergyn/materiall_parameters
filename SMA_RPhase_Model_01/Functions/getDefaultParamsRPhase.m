function [O,P] = getDefaultParamsRPhase(enableOutputs)

P.CycleTime = 14;               % Length of the cylce [s]
P.nCycles = 5;                  % Number of cycles to run

P.showPlots = 0;                % Show plots or not
P.Sim.logStep = 0.2;          % Timestep of the logger [s]
P.Sim.tStep = 0.001;            % Timestep of the simulation [s]
P.Sim.maxTime = P.CycleTime*P.nCycles; % Total Simulation Time [s]

% FDS Parameters

P.FDS.InitTemp = 18;            % Initial temperature of components [°C]
P.FDS.AmbientTemp = 18;         % Ambient temperature of the air around the cores [°C]

P.InitTemp = P.FDS.InitTemp;

% P.NiTi.M.Hcur	 =	0.024483; % [m/m]
% P.NiTi.M.MS	     =	223; % [K]
% P.NiTi.M.MF	     =	123; % [K]
% P.NiTi.M.A1S	 =	170; % [K]
% P.NiTi.M.A1F	 =	293; % [K]
% P.NiTi.M.N1	     =	0.4; % []
% P.NiTi.M.N2	     =	0.18993; % []
% P.NiTi.M.N3	     =	0.63408; % []
% P.NiTi.M.N4	     =	0.23816; % []
% P.NiTi.M.CA1	 =	10e6; % [Pa/K]
% P.NiTi.M.CM  	 =	10e6; % [Pa/K]
% P.NiTi.M.EA1 	 =	55e9; % [Pa]
% P.NiTi.M.EM	     =	45e9; % [Pa]
% P.NiTi.M.y  	 =	2.5699; % []
% P.NiTi.M.C_ 	 =	0.44116; % []
% P.NiTi.M.CalS	 =	1; % [Pa]

% 
% P.NiTi.R.Hcur	 =	0.001; % [m/m]
% P.NiTi.R.RS	     =	300; % [K]
% P.NiTi.R.RF	     =	250; % [K]
% P.NiTi.R.A2S	 =	273; % [K]
% P.NiTi.R.A2F	 =	308; % [K]
% P.NiTi.R.N1	     =	0.4; % []
% P.NiTi.R.N2	     =	0.18993; % []
% P.NiTi.R.N3	     =	0.63408; % []
% P.NiTi.R.N4	     =	0.23816; % []
% P.NiTi.R.CA2	 =	20e6; % [Pa/K]
% P.NiTi.R.CR  	 =	20e6; % [Pa/K]
% P.NiTi.R.EA2 	 =	60e9; % [Pa]

% P.NiTi.M.Hcur	=	0.029745; % [m/m]
% P.NiTi.M.MS	=	316.4164; % [K]
% P.NiTi.M.MF	=	70.3078; % [K]
% P.NiTi.M.A1S	=	195.077; % [K]
% P.NiTi.M.A1F	=	345.3313; % [K]
% P.NiTi.M.N1	=	0.55577; % []
% P.NiTi.M.N2	=	0.27896; % []
% P.NiTi.M.N3	=	0.1; % []
% P.NiTi.M.N4	=	0.3652; % []
% P.NiTi.M.CA1	=	7161373.8802; % [Pa/K]
% P.NiTi.M.EA1	=	43475361493.2335; % [Pa]
% P.NiTi.M.EM	=	55977792337.5055; % [Pa]
% P.NiTi.R.Hcur	=	0.0019018; % [m/m]
% P.NiTi.R.RS	=	282.3271; % [K]
% P.NiTi.R.RF	=	206.86; % [K]
% P.NiTi.R.A2S	=	292.1883; % [K]
% P.NiTi.R.A2F	=	339.9597; % [K]
% P.NiTi.R.N1	=	0.50386; % []
% P.NiTi.R.N2	=	0.38448; % []
% P.NiTi.R.N3	=	0.36342; % []
% P.NiTi.R.N4	=	0.50432; % []
% P.NiTi.R.CA2	=	17996450.8901; % [Pa/K]
% P.NiTi.R.EA2	=	72734870544.582; % [Pa]

P.NiTi.M.Hcur	=	0.01681; % [m/m]
P.NiTi.M.MS	=	256.5903; % [K]
P.NiTi.M.MF	=	230.182; % [K]
P.NiTi.M.A1S	=	202.4912; % [K]
P.NiTi.M.A1F	=	340.4892; % [K]
P.NiTi.M.N1	=	0.2969; % []
P.NiTi.M.N2	=	0.19681; % []
P.NiTi.M.N3	=	0.13902; % []
P.NiTi.M.N4	=	0.26119; % []
P.NiTi.M.CA1	=	10647224.3378; % [Pa/K]
P.NiTi.M.EA1	=	59317450479.6175; % [Pa]
P.NiTi.M.EM	=	61687837027.512; % [Pa]
P.NiTi.M.y	=	2.4506; % []
P.NiTi.M.C_	=	0.96083; % []
P.NiTi.R.Hcur	=	0.010472; % [m/m]
P.NiTi.R.RS	=	306.3386; % [K]
P.NiTi.R.RF	=	223.7814; % [K]
P.NiTi.R.A2S	=	258.1531; % [K]
P.NiTi.R.A2F	=	284.7846; % [K]
P.NiTi.R.N1	=	0.41458; % []
P.NiTi.R.N2	=	0.4917; % []
P.NiTi.R.N3	=	0.58279; % []
P.NiTi.R.N4	=	0.34951; % []
P.NiTi.R.CA2	=	17914514.285; % [Pa/K]
P.NiTi.R.EA2	=	60850462910.6844; % [Pa]
P.NiTi.R.y	=	1.9404; % []
P.NiTi.R.C_	=	0.57803; % []


P.NiTi.R.ER	     =	P.NiTi.M.EA1; % [Pa]
% P.NiTi.R.y  	 =	2.5699; % []
% P.NiTi.R.C_ 	 =	0.44116; % []
P.NiTi.R.CalS	 =	1; % [Pa]

P.NiTi.M.CPA1     = 460; %[J/kg.K]
P.NiTi.M.CPM      = 460; %[J/kg.K]
P.NiTi.R.CPR      = P.NiTi.M.CPA1;%[J/kg.K]
P.NiTi.R.CPA2     = P.NiTi.M.CPA1;%[J/kg.K]

% Load Params
RPhaseMat

if enableOutputs
    Test_Outputs
end


