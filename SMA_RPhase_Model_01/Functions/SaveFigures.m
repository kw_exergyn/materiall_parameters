function SaveFigures(Enable,h,width, height)

if Enable>0
    proj = matlab.project.rootProject;
    TopDir = [proj.RootFolder{1} '\'];
    if ~exist([TopDir  'Plots'],'dir')
        mkdir([TopDir  'Plots']);
    end
    for i = 1:numel(h)
        figure(h(i))
        if nargin > 3
            set(gcf,'Units','pixels','Position',[9 39 width height])
        end
        filename = [TopDir  'Plots\' get(h(i),'Name') '.emf'];
        print('-dmeta','-painters', filename);

        set(gcf,'PaperPositionMode','auto')
        filename = [TopDir  'Plots\' get(h(i),'Name') '.png'];
        print('-dpng','-painters', filename);
        if Enable == 2
            filename = [TopDir  'Plots\' get(h(i),'Name') '.fig'];
            savefig(h(i),filename,'compact')
        end
    end
end