function cost = CostFuncRPhaseMaterial(P,filename, mdlName, Opt, D, Array)

nD = length(Opt.DimNames);
for i = 1:nD
    eval([Opt.DimNames{i} ' = Array(i) * Opt.Scale(i);']);
end
P.DSC=0;
%% Get the Input Data
CalcCost = zeros(P.nTemps,1);
for i = 1:P.nTemps
    if D.TempEnable(i) == 1
        % Inputs.Temp = timeseries(movmean(TDs.Temp.Data,10000),TDs.Temp.Time);
        TDs = D.T{i};
        Opt.StrainOffset = D.StrainOffset(i);
        Opt.EnergyDensityTargetHeat = D.Heating(i);
    
        time = linspace(0, TDs.Temp.Time(end)*Opt.nCat, length(TDs.Temp.Time)*Opt.nCat)';
    
        Inputs.Temp = timeseries(repmat(TDs.Temp.Data,Opt.nCat,1),time);
        Inputs.Temp.Data(1:end) = mean(Inputs.Temp.Data);
    
        Inputs.Force = timeseries(repmat(TDs.Force.Data,Opt.nCat,1),time);
        Inputs.Force.Data(1:20) = -100;
    
        Inputs.TransDir = timeseries(repmat(TDs.TransDir.Data,Opt.nCat,1),time);
    
        %% Get Parameters
        P.Sim.logStep = Opt.dt;
        P.Sim.tStep = Opt.dt;                   % Time Step [s]
    
        P.Sim.maxTime = TDs.Temp.Time(end)*2;
        P.showPlots = 0;
    
        P.FDS.InitTemp = mean(TDs.Temp.Data(1:10))-273.15;
        P.FDS.Tmid = P.FDS.InitTemp;
        P.FDS.AmbientTemp = 16;
        P.FDS.FluidPressure = 4;
        P.NiTi.ForceInt = 0;
    
        %     P.InitTemp = P.FDS.InitTemp;
    
        % P.NiTi.R.ER = P.NiTi.M.EA1;
        P.NiTi.M.CM = P.NiTi.M.CA1;
        P.NiTi.R.CR = P.NiTi.R.CA2;
    
        % P.NiTi.M.MS     = P.NiTi.M.A1F;
        % P.NiTi.M.N3	    =	0;
    
        % P.NiTi.R.CPR = P.NiTi.M.CPA1;
        % P.NiTi.R.CPA2 = P.NiTi.M.CPA1;
    
        % P.DSC = 1;
        RPhaseMat
    
        tempStruct = Simulink.Bus.createObject(P);
        sb_Core_P = evalin('base',tempStruct.busName);
    
        tempStruct = Simulink.Bus.createObject(O);
        sb_Core_O = evalin('base',tempStruct.busName);
    
        % tempStruct = Simulink.Bus.createObject(Inputs);
        % sb_Core_Inputs = evalin('base',tempStruct.busName);
    
    
        %% Create Simulink Workspace Files
        simIn = Simulink.SimulationInput(mdlName);
        simIn = simIn.setVariable('P',P);
        simIn = simIn.setVariable('O',O);
        simIn = simIn.setVariable('Inputs',Inputs);
        simIn = simIn.setVariable('sb_Core_P',sb_Core_P);
        simIn = simIn.setVariable('sb_Core_O',sb_Core_O);
        % simIn = simIn.set_param(mdlName,'AccelVerboseBuild','on');
        %% Run the simulation and process the output
    
    %     if P.NiTi.M.Y0 <=0 || P.NiTi.R.Y0 <=0 || P.NiTi.R.RS < P.NiTi.R.RF || P.NiTi.R.A2F < P.NiTi.R.A2S ...
    %             ||P.NiTi.M.A1F < P.NiTi.M.MS || P.NiTi.M.A1F < P.NiTi.M.A1S || P.NiTi.M.MS < P.NiTi.M.MF || P.NiTi.R.A2F < P.NiTi.R.RS
    %         %            P.NiTi.R.A2F < P.NiTi.R.RS ...
    %                         ...
    % %                      || P.NiTi.R.EA2 < P.NiTi.M.EA1 || P.NiTi.R.A2F < P.NiTi.M.A1F
    %         %
    %         success = 0;
    %         %         pause(10)
    %     else
             try
                out = sim(simIn);
                success = 1;
             catch
                success = 0;
             end
    %     end
    
        %% Run processing
        if success
            % try
            CalcCost(i) = costCalcMaterialRPhase(out,TDs,Opt,P);
           
            % catch
            %    [CalcCost(a)] = nan;
            % end
        else
            CalcCost(i) = 100;
        end
    end
end
cost = sum(CalcCost) / sum(D.TempEnable);
if cost > 1000
    cost = 1000;
end
if success
    for i = 1:Opt.nD
        fieldName = strrep(Opt.DimNames{i},'.','_');
        stats.(fieldName) = Array(i);
    end

    try
        m = matfile([Opt.Folder filename],'Writable',true);
        m.stats(1,end+1) = stats;
    catch
        pause(rand*10)
        try
            m = matfile([Opt.Folder filename],'Writable',true);
            m.stats(1,end+1) = stats;
        catch
            disp('Failed to write to log');
            disp(stats);
        end
    end
else
    disp('Simulation Failed')

    cost = 50;
 
end