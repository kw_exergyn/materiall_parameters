function h = subtightplotGP(m,n,p,yy,varargin)
%function h=subtightplotGP(m,n,p,yy,varargin)
% Function to replace subplot with tighter margins. Attempt has been made
% to make the margins universily a good (small) size.
% 
% Input:    m:  Number of Rows
%           n:  Number of Columns
%           p:  Current subplot number
%           yy: set to 1 if a second Y axis is used, leave empty or 0 if using single Y axis.
%
% Output arguments: same as subplot- none, or axes handle according to function call.
%
% Issues & Comments: Note that if additional elements are used in order to be passed to subplot, gap parameter must
%       be defined. For default gap value use empty element- [].      
%
% Usage example: h=subtightplot((2,3,1)

if (nargin<4), yy = 0; end

outPos = get(gcf,'outerposition');
windowWidth = outPos(3);
windowHeight = outPos(4);
% gap_horz = (0.025 + 0.025*yy*0.8 + 0.005)/windowWidth;
% marg_left = 0.025/windowWidth;
% marg_right = (0.0125 + 0.0125*yy)/windowWidth;

gap_horz = (0.021 + 0.025*yy*0.75 + 0.005)/windowWidth;
marg_left = 0.023/windowWidth;
marg_right = (0.01 + 0.011*yy)/windowWidth;

gap_vert = 0.054/windowHeight;
% gap_vert = 0.05/windowHeight;
marg_lower = 0.04/windowHeight;
marg_upper = 0.02/windowHeight;

%note n and m are switched as Matlab indexing is column-wise, while subplot indexing is row-wise :(
[subplot_col,subplot_row]=ind2sub([n,m],p);  

% note subplot suppors vector p inputs- so a merged subplot of higher dimentions will be created
subplot_cols=1+max(subplot_col)-min(subplot_col); % number of column elements in merged subplot 
subplot_rows=1+max(subplot_row)-min(subplot_row); % number of row elements in merged subplot   

% single subplot dimensions:
height=(1-(marg_lower+marg_upper)-(m-1)*gap_vert)/m;
width =(1-(marg_left+marg_right)-(n-1)*gap_horz)/n;

% merged subplot dimensions:
merged_height=subplot_rows*( height+gap_vert )- gap_vert;
merged_width= subplot_cols*( width +gap_horz )- gap_horz;

% merged subplot position:
merged_bottom=(m-max(subplot_row))*(height+gap_vert) +marg_lower;
merged_left=(min(subplot_col)-1)*(width+gap_horz) +marg_left;
pos_vec=[merged_left merged_bottom merged_width merged_height];

h = subplot('Position',pos_vec,varargin{:});

if (nargout < 1),  clear h;  end

end
