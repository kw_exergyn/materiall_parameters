function [O]= MatInitial(P)

Temperature =  P.NiTi.T0;
Stress = 0;

O.Trans = 1;
O.DTrDt = 0;
O.Stress = Stress;
O.Temperature = Temperature;

O.M.DFDE_ForOne  = P.NiTi.M.ForOne;
O.M.DFDE_ForZero = P.NiTi.M.ForZero;
O.M.DFDE_RevOne  = P.NiTi.M.RevOne;
O.M.DFDE_RevZero = P.NiTi.M.RevZero;

O.M.K = 0;
O.M.MarVol = 0;
O.M.CN = min([P.NiTi.M.C_, O.M.MarVol, (1 - O.M.MarVol),0.01]); % Tuning Variable
O.M.MarvolR = O.M.MarVol;                                       % Reversal Martensitic Volume
O.M.MarR = (O.M.MarVol - O.M.MarvolR);                          % Martensitic Volume difference from Reversal Point
O.M.Nth = min(-1e-6,-(P.NiTi.M.y/O.M.CN)*abs(O.M.MarR));        % Minor Loop Slope

O.M.YS = max(0,(P.NiTi.M.Y0 + P.NiTi.M.D * P.NiTi.M.CalS * P.NiTi.M.Hcur));    % Thermodynamic Force [J/m^3]
O.M.S = P.NiTi.M.SA1 + O.M.MarVol *( P.NiTi.M.SM - P.NiTi.M.SA1);              % Stress Compliance [1/GPa]
O.M.A = P.NiTi.M.ExpanA1 + O.M.MarVol * ( P.NiTi.M.ExpanM - P.NiTi.M.ExpanA1); % Thermal Expansion [1/K]


O.M.DFDE = ( abs(Stress)* P.NiTi.M.Hcur *(1-P.NiTi.M.D)) + (0.5 * P.NiTi.M.Delta_S * Stress^2) +  ...
    ( Stress* P.NiTi.M.DA * ( Temperature - P.NiTi.T0)) + ( P.NiTi.Dens * P.NiTi.M.Delta_entr * Temperature) - ...
    P.NiTi.M.InEn - O.M.YS; % Current Hardening Function Value
try
    O.M.DFDE_For  = max(min(interp1(P.NiTi.M.Marvol_For,P.NiTi.M.DFDE_For,O.M.MarVol,'nearest','extrap'),P.NiTi.M.ForOne),P.NiTi.M.ForZero); % Hardening Function Boundary

    O.M.MarVol = max(min(interp1(P.NiTi.M.DFDE_For,P.NiTi.M.Marvol_For,O.M.DFDE,'nearest','extrap'),1),0); % Update current Martensitic Volume
    O.M.DFDE_Rev  = max(min(interp1(P.NiTi.M.Marvol_Rev,P.NiTi.M.DFDE_Rev,O.M.MarVol,'nearest','extrap'),P.NiTi.M.RevOne),P.NiTi.M.RevZero); % Hardening Function Boundary
catch
    O.M.DFDE_For  = 0; % Hardening Function Boundary
    O.M.MarVol = 0;    % Update current Martensitic Volume
    O.M.DFDE_Rev  =0;
end
O.M.TBound = - O.M.YS;


%% Martensite  Output

O.TempOld   = Temperature; % Old Temperautre
O.StressOld = Stress;      % Old Stress
O.MarOld    = O.M.MarVol;  % Old Martensitic Volume


O.DTemperature = Temperature - O.TempOld; %  Temperautre Difference
O.DStress      = Stress - O.StressOld ;   % Stress Difference
O.DMar         = O.M.MarVol - O.MarOld;   %  Martensitic Volume Difference

O.A = P.NiTi.M.ExpanA1 + O.M.MarVol * ( P.NiTi.M.ExpanM - P.NiTi.M.ExpanA1) ; % Thermal Expansion [1/K]
O.S = P.NiTi.M.SA1 + O.M.MarVol *( P.NiTi.M.SM - P.NiTi.M.SA1);               % Stress Compliance [1/GPa]
O.k = P.NiTi.M.kA1 + O.M.MarVol *( P.NiTi.M.kM - P.NiTi.M.kA1);               % Stress Compliance [1/GPa]
O.C = P.NiTi.M.CPA1 + O.M.MarVol *( P.NiTi.M.CPM - P.NiTi.M.CPA1);            % Specific Heat Capacity [J/kg.K]
%Strain

O.Strain = (Stress * O.S) + O.A*(Temperature - P.NiTi.T0) + ( sign(Stress) * ( O.M.MarVol * P.NiTi.M.Hcur));

%Energy

O.Q =-( (Temperature* O.A* O.DStress) + (O.M.TBound + (Temperature * P.NiTi.M.DA * Stress) ...
    -(P.NiTi.Dens* P.NiTi.M.DC*log(Temperature/P.NiTi.T0)) ...
    + (P.NiTi.Dens *P.NiTi.M.Delta_entr * Temperature)) * O.DMar)/P.NiTi.Dens; % Energy Change for Current Time Step [J/m^3]

O.Entropy  = ((1/P.NiTi.Dens) * O.A * O.DStress) + ((O.C/Temperature)* O.DTemperature) + ...
    (((1/P.NiTi.Dens)*P.NiTi.M.DA*Stress) - P.NiTi.M.DC*log(Temperature/P.NiTi.T0) ...
    + P.NiTi.M.Delta_entr)* O.DMar ;% Entropy Change for Current Time Step




