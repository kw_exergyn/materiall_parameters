function [O,P] = getDefaultParams03(enableOutputs)

P.CycleTime = 14;               % Length of the cylce [s]
P.nCycles = 5;                  % Number of cycles to run

P.showPlots = 0;                % Show plots or not
P.Sim.logStep = 0.1;          % Timestep of the logger [s]
P.Sim.tStep = 0.1;            % Timestep of the simulation [s]
P.Sim.maxTime = P.CycleTime*P.nCycles; % Total Simulation Time [s]

% FDS Parameters

P.FDS.InitTemp = 18;            % Initial temperature of components [°C]
P.FDS.AmbientTemp = 18;         % Ambient temperature of the air around the cores [°C]

P.InitTemp = P.FDS.InitTemp;

P.NiTi.M.Hcur	 =	0.024483; % [m/m]
P.NiTi.M.MS	     =	223; % [K]
P.NiTi.M.MF	     =	123; % [K]
P.NiTi.M.A1S	 =	170; % [K]
P.NiTi.M.A1F	 =	293; % [K]
P.NiTi.M.N1	     =	0.4; % []
P.NiTi.M.N2	     =	0.18993; % []
P.NiTi.M.N3	     =	0.63408; % []
P.NiTi.M.N4	     =	0.23816; % []
P.NiTi.M.CA1	 =	10e6; % [Pa/K]
P.NiTi.M.CM  	 =	10e6; % [Pa/K]
P.NiTi.M.EA1 	 =	55e9; % [Pa]
P.NiTi.M.EM	     =	45e9; % [Pa]
P.NiTi.M.y  	 =	2.5699; % []
P.NiTi.M.C_ 	 =	0.44116; % []
P.NiTi.M.CalS	 =	1e6; % [Pa]
P.NiTi.M.CPA1     = 460; %[J/kg.K]
P.NiTi.M.CPM      = 460; %[J/kg.K]

P.NiTi.M.Hcur	=	0.035127; % [m/m]
P.NiTi.M.MS	    =	300; % [K]
P.NiTi.M.MF	    =	130; % [K]
P.NiTi.M.A1S	=	180; % [K]
P.NiTi.M.A1F	=	310; % [K]
P.NiTi.M.N1	    =	0.25; % []
P.NiTi.M.N2	    =	0.15; % []
P.NiTi.M.N3	    =	0.2; % []
P.NiTi.M.N4	    =	0.2; % []
P.NiTi.M.CA1	=	8604646.3512; % [Pa/K]
P.NiTi.M.EA1	=	33434511616.6939; % [Pa]
P.NiTi.M.EM	    =	53835303217.1635; % [Pa]
P.NiTi.M.y	    =	1.2268; % []
P.NiTi.M.C_	    =	0.37091; % []







% Load Params
MPhaseMat03

if enableOutputs
    Test_Outputs
end


