function TD = ReadJanLog(path, filename, Calib)

    files = dir([path '*' filename '*']);
    nf = length(files);
    
    for j = 1:nf
        A = importdata([files(j).folder '\' files(j).name]);
        nVars = length(A.textdata);
        for i = 1:nVars
            varName = regexprep(A.textdata{i}, '[%()!"°/µ_.-]', '');
            varName = strrep(varName,'  ',' ');
            varName = strrep(varName,' ','_');
            if strcmp(varName(1),'_')
                varName = varName(2:end);
            end
            if strcmp(varName(end),'_')
                varName = varName(1:end-1);
            end
            
            A.data(:,1) = A.data(:,1)-A.data(1,1); % Zero Time
            
            if j == 1
%                 if strcmp(varName,'Extensometer')
%                     TD.(varName) = A.data(:,i)-mean(A.data(1:100,i));
%                 else
                    TD.(varName) = A.data(:,i);
%                 end
            elseif strcmp(varName,'Time_s')
                time = A.data(:,i)+TD.Time_s(end)+0.02;
                TD.(varName) = [TD.(varName); time];
%             elseif strcmp(varName,'Extensometer')
%                 TD.(varName) = [TD.(varName); A.data(:,i)-mean(A.data(1:100,i))];
            else
                TD.(varName) = [TD.(varName); A.data(:,i)];
            end
        end
    end
    
    % %% Calibration
    % TD.Extensometer = TD.Extensometer - mean(TD.Extensometer(1:100));

    %% Apply Filtering
    vars = fields(TD);
    nvars = length(vars);
    for i = 2:nvars
        TD.([vars{i} '_smooth']) = nanfastsmooth(TD.(vars{i}),19,1,1);
    end

end
