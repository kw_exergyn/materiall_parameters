function cost = CostFuncMaterial03(filename, mdlName, Opt, D, Array)

nD = length(Opt.DimNames);
for i = 1:nD
    eval([Opt.DimNames{i} ' = Array(i) * Opt.Scale(i);']);
end

%%  Get the Input Data
if P.DSC
    RunNo = 1;
else
    RunNo = 3;
end

for a = 1:1:3
    % Inputs.Temp = timeseries(movmean(TDs.Temp.Data,10000),TDs.Temp.Time);
    eval(['TDs = D.T', num2str(a)])
    eval(['Opt.EnergyDensityTargetHeat = Opt.EnergyDensityTargetHeat',num2str(a) ]);    % kJ/kg;    % kJ/kg
    B = TDs.Temp.Data;
    B =cat(1,B,TDs.Temp.Data);
    C = (0:mean(diff(TDs.Temp.Time)):((TDs.Temp.Time(end)*2)))';

    Inputs.Temp = timeseries((B(1:end-1)),C);
    % Inputs.Temp.Data(1:end) = Inputs.Temp.Data(1);

    B = TDs.Force.Data;
    B =cat(1,B,TDs.Force.Data);
    Inputs.Force = timeseries(B(1:end-1),C);
    % Inputs.Force = timeseries(TDs.Force.Data,TDs.Temp.Time);
    Inputs.Force.Data(1:20) = 0;

    B = TDs.TransDir.Data;
    B =cat(1,B,TDs.TransDir.Data);
    Inputs.TransDir = timeseries(B(1:end-1),C);
    % Inputs.TransDir = timeseries(TDs.TransDir.Data,TDs.Temp.Time);

    %% Get Parameters

    P.Sim.logStep = Opt.dt;
    P.Sim.tStep = Opt.dt;                   % Time Step [s]

    P.Sim.maxTime = TDs.Temp.Time(end)*2;
    P.showPlots = 0;

    P.FDS.InitTemp = mean(TDs.Temp.Data(1:10))-273.15;
    P.FDS.Tmid = P.FDS.InitTemp;
    P.FDS.AmbientTemp = 16;
    P.FDS.FluidPressure = 4;
    P.NiTi.ForceInt = 0;

    P.InitTemp = P.FDS.InitTemp;
    P.NiTi.M.CM = P.NiTi.M.CA1;


    % P.NiTi.R.CPR = P.NiTi.M.CPA1;
    % P.NiTi.R.CPA2 = P.NiTi.M.CPA1;

   
    MPhaseMat03


    name = 'sb_Core_P';
    tempStruct = Simulink.Bus.createObject(P);
    eval([name ' = evalin(''base'',tempStruct.busName);']);

    name = 'sb_Core_O';
    tempStruct = Simulink.Bus.createObject(O);
    eval([name ' = evalin(''base'',tempStruct.busName);']);


    % Create Simulink Workspace Files
    simIn = Simulink.SimulationInput(mdlName);
    simIn = simIn.setVariable('P',P);
    simIn = simIn.setVariable('O',O);
    simIn = simIn.setVariable('Inputs',Inputs);
    simIn = simIn.setVariable('sb_Core_P',sb_Core_P);
    simIn = simIn.setVariable('sb_Core_O',sb_Core_O);
    % simIn = simIn.set_param(mdlName,'AccelVerboseBuild','on');
    % Run the simulation and process the output

    if P.NiTi.M.Y0 <=0   || P.NiTi.M.A1F < P.NiTi.M.MS ||  ...
            P.NiTi.M.A1F < P.NiTi.M.A1S || P.NiTi.M.MS < P.NiTi.M.MF

        success = 0;
        pause(1)
    else
        try
            out = sim(simIn);
            success = 1;
        catch
            success = 0;
        end
    end
    if success
        [CalcCost(a)] = costCalcMaterial03(out,TDs,Opt,P);
    else
        CalcCost(a) = 30;
    end
end

cost = sum(CalcCost);

%% Run processing
if success
%     [cost] = costCalcMaterial03(out,TDs,Opt,P);
    for i = 1:Opt.nD
        fieldName = strrep(Opt.DimNames{i},'.','_');
        stats.(fieldName) = Array(i);
    end

    try
        m = matfile([Opt.Folder filename],'Writable',true);
        m.stats(1,end+1) = stats;
    catch
        pause(rand*10)
        try
            m = matfile([Opt.Folder filename],'Writable',true);
            m.stats(1,end+1) = stats;
        catch
            disp('Failed to write to log');
            disp(stats);
        end
    end
else
    disp('Simulation Failed')

    %     [cost, stats] = costCalcMaterial(out,TDs,Opt);
    cost = 30;
    %     stats.cost = cost;
    %     stats.StrainError = 5;
    for i = 1:Opt.nD
        fieldName = strrep(Opt.DimNames{i},'.','_');
        stats.(fieldName) = Array(i);
    end
    try
        m = matfile([Opt.Folder filename],'Writable',true);
        m.stats(1,end+1) = stats;
    catch
        pause(rand*10)
        try
            m = matfile([Opt.Folder filename],'Writable',true);
            m.stats(1,end+1) = stats;
        catch
            disp('Failed to write to log');
            disp(stats);
        end
    end
end