function [O]= R_PhaseInitial(P)
% Temperature = P.NiTi.T0+50;
Temperature =  P.NiTi.T0;
if isfield(P,'TNK')
    Stress = -(P.Core.Hyd.CSA*1e-6)*(P.Hyd.PresMin*1e5);
else
    Stress = 0;
end

O.Trans = 1;
O.DTrDt = 0;
O.Stress = Stress;
O.Temperature = Temperature;
O.R.DFDE_ForOne  = P.NiTi.R.ForOne;
O.R.DFDE_ForZero = P.NiTi.R.ForZero;
O.R.DFDE_RevOne  = P.NiTi.R.RevOne;
O.R.DFDE_RevZero = P.NiTi.R.RevZero;

O.M.DFDE_ForOne  = P.NiTi.M.ForOne;
O.M.DFDE_ForZero = P.NiTi.M.ForZero;
O.M.DFDE_RevOne  = P.NiTi.M.RevOne;
O.M.DFDE_RevZero = P.NiTi.M.RevZero;

O.M.K = 0;
O.M.MarVol = 0;
O.M.CN = min([P.NiTi.M.C_, O.M.MarVol, (1 - O.M.MarVol),0.01]); % Tuning Variable
O.M.MarvolR = O.M.MarVol; % Reversal Martensitic Volume
O.M.MarR = (O.M.MarVol - O.M.MarvolR); % Martensitic Volume difference from Reversal Point
O.M.Nth = min(-1e-6,-(P.NiTi.M.y/O.M.CN)*abs(O.M.MarR)); % Minor Loop Slope

O.M.YS = max(0,(P.NiTi.M.Y0 + P.NiTi.M.D * P.NiTi.M.CalS * P.NiTi.M.Hcur));% Thermodynamic Force [J/m^3]
O.M.S = P.NiTi.M.SA1 + O.M.MarVol *( P.NiTi.M.SM - P.NiTi.M.SA1); % Stress Compliance [1/GPa]
O.M.A = P.NiTi.M.ExpanA1 + O.M.MarVol * ( P.NiTi.M.ExpanM - P.NiTi.M.ExpanA1); % Thermal Expansion [1/K]


O.M.DFDE = ( abs(Stress)* P.NiTi.M.Hcur *(1-P.NiTi.M.D)) + (0.5 * P.NiTi.M.Delta_S * Stress^2) +  ...
    ( Stress* P.NiTi.M.DA * ( Temperature - P.NiTi.T0)) + ( P.NiTi.Dens * P.NiTi.M.Delta_entr * Temperature) - ...
    P.NiTi.M.InEn - O.M.YS; % Current Hardening Function Value
try
    O.M.DFDE_For  = max(min(interp1(P.NiTi.M.Marvol_For,P.NiTi.M.DFDE_For,O.M.MarVol,'nearest','extrap'),P.NiTi.M.ForOne),P.NiTi.M.ForZero); % Hardening Function Boundary
    
    O.M.MarVol = max(min(interp1(P.NiTi.M.DFDE_For,P.NiTi.M.Marvol_For,O.M.DFDE,'nearest','extrap'),1),0); % Update current Martensitic Volume
    O.M.DFDE_Rev  = max(min(interp1(P.NiTi.M.Marvol_Rev,P.NiTi.M.DFDE_Rev,O.M.MarVol,'nearest','extrap'),P.NiTi.M.RevOne),P.NiTi.M.RevZero); % Hardening Function Boundary
catch
    O.M.DFDE_For  = 0; % Hardening Function Boundary
    O.M.MarVol = 0; % Update current Martensitic Volume
    O.M.DFDE_Rev  =0;
end
O.M.TBound = - O.M.YS;






%%  [R-Phase]
if P.NiTi.RPhase
    O.R.RVol = 0;
    O.R.S = P.NiTi.R.SA2 + O.R.RVol *( P.NiTi.R.SR - P.NiTi.R.SA2); % Stress Compliance [1/GPa]
    O.R.A = P.NiTi.R.ExpanA2 + O.R.RVol * ( P.NiTi.R.ExpanR - P.NiTi.R.ExpanA2); % Thermal Expansion [1/K]
    O.R.K = 0; % Activate the Minor Loop
    O.R.CN = min([P.NiTi.R.C_, O.R.RVol, (1 - O.R.RVol),0.01]); % Tuning Variable
    O.R.RvolR = O.R.RVol; % Reversal Martensitic Volume
    O.R.RR = (O.R.RVol - O.R.RvolR); % Martensitic Volume difference from Reversal Point
    O.R.Nth = min(-1e-6,-(P.NiTi.R.y/O.R.CN)*abs(O.R.RR)); % Minor Loop Slope
    O.R.YS = max(0,(P.NiTi.R.Y0 + P.NiTi.R.D * P.NiTi.R.CalS * P.NiTi.R.Hcur));% Thermodynamic Force [J/m^3]
    
    O.R.DFDE = ( abs(Stress)* P.NiTi.R.Hcur *(1-P.NiTi.R.D)) + (0.5 * P.NiTi.R.Delta_S * Stress^2) +  ...
        ( Stress* P.NiTi.R.DA * ( Temperature - P.NiTi.T0)) + ( P.NiTi.Dens * P.NiTi.R.Delta_entr * Temperature) - ...
        P.NiTi.R.InEn - O.R.YS; % Current Hardening Function Value
    try
        O.R.DFDE_For  = max(min(interp1(P.NiTi.R.Rvol_For,P.NiTi.R.DFDE_For,O.R.RVol,'nearest','extrap'),P.NiTi.R.ForOne),P.NiTi.R.ForZero); % Hardening Function Boundary
        O.R.RVol = max(min(interp1(P.NiTi.R.DFDE_For,P.NiTi.R.Rvol_For,O.R.DFDE,'nearest','extrap'),1),0); % Update current Martensitic Volume
        O.R.DFDE_Rev  = max(min(interp1(P.NiTi.R.Rvol_Rev,P.NiTi.R.DFDE_Rev,O.R.RVol,'nearest','extrap'),P.NiTi.R.RevOne),P.NiTi.R.RevZero); % Hardening Function Boundary
    catch
        O.R.DFDE_For  = 0; % Hardening Function Boundary
        O.R.RVol = 0; % Update current Martensitic Volume
        O.R.DFDE_Rev  = 0; % Hardening Function Boundary
    end
    O.R.TBound = - O.R.YS;
    
    
else
    O.R.RVol  = 0;
    O.R.S     = 0;
    O.R.A     = 0;
    O.R.K     = 0; % Activate the Minor Loop
    O.R.CN    = 0;
    O.R.RvolR = 0; % Reversal Martensitic Volume
    O.R.RR    = 0; % Martensitic Volume difference from Reversal Point
    O.R.Nth   = 0; % Minor Loop Slope
    O.R.YS    = 0;% Thermodynamic Force [J/m^3]
    
    O.R.DFDE      = 0; % Current Hardening Function Value
    
    O.R.DFDE_For  = 0; % Hardening Function Boundary
    O.R.MarVol    = 0; % Update current Martensitic Volume
    O.R.TBound    = 0;
    
    O.R.DFDE_Rev  = 0; % Hardening Function Boundary
    
end



%% Combined Martensite & R-phase Output

O.TempOld   = Temperature; % Old Temperautre
O.StressOld = Stress; % Old Stress
O.MarOld    = O.M.MarVol; % Old Martensitic Volume
O.RVolOld   = O.R.RVol; % Old R-Phase Volume

O.DTemperature = Temperature - O.TempOld; %  Temperautre Difference
O.DStress      = Stress - O.StressOld ;   % Stress Difference
O.DMar         = O.M.MarVol - O.MarOld;   %  Martensitic Volume Difference
O.DRVol        = O.R.RVol - O.RVolOld;     % Old R-Phase Volume

if P.NiTi.RPhase
    
    O.A = P.NiTi.R.ExpanA2 + O.M.MarVol * ( P.NiTi.M.ExpanM - P.NiTi.M.ExpanA1) + O.R.RVol*( P.NiTi.R.ExpanR - P.NiTi.R.ExpanA2); % Thermal Expansion [1/K]
    O.S = P.NiTi.R.SA2 + O.M.MarVol *( P.NiTi.M.SM - P.NiTi.M.SA1) + O.R.RVol*( P.NiTi.R.SR - P.NiTi.R.SA2); % Stress Compliance [1/GPa]
    O.k = P.NiTi.R.kA2 + O.M.MarVol *( P.NiTi.M.kM - P.NiTi.M.kA1) + O.R.RVol*( P.NiTi.R.kR - P.NiTi.R.kA2); % Thermal Conductivity [W/m.K]
    O.C = P.NiTi.R.CPA2 + O.M.MarVol *( P.NiTi.M.CPM - P.NiTi.M.CPA1) + O.R.RVol*( P.NiTi.R.CPR - P.NiTi.R.CPA2); % Specific Heat Capacity [J/kg.K]
    %Strain
    
    O.Strain = (Stress * O.S) + O.A*(Temperature - P.NiTi.T0) + ( sign(Stress) * ( O.M.MarVol * P.NiTi.M.Hcur)) ...
        + ( sign(Stress) * ( O.R.RVol * P.NiTi.R.Hcur));
    
    %Energy
    
     O.Q = -((Temperature* O.A* O.DStress) + (O.M.TBound + (Temperature * P.NiTi.M.DA * Stress) ...
        -(P.NiTi.Dens* P.NiTi.M.DC*log(Temperature/P.NiTi.T0))+ (P.NiTi.Dens *P.NiTi.M.Delta_entr * Temperature)) * O.DMar ...
        + (O.R.TBound + (Temperature * P.NiTi.R.DA * Stress)-(P.NiTi.Dens* P.NiTi.R.DC*log(Temperature/P.NiTi.T0))...
        + (P.NiTi.Dens *P.NiTi.R.Delta_entr * Temperature))* O.DRVol)/P.NiTi.Dens; % Energy Change for Current Time Step [J/m^3]
    
    O.Entropy  = ((1/P.NiTi.Dens) * O.A * O.DStress) + ((O.C/Temperature)* O.DTemperature) + ...
        (((1/P.NiTi.Dens)*P.NiTi.M.DA*Stress) - P.NiTi.M.DC*log(Temperature/P.NiTi.T0) ...
        + P.NiTi.M.Delta_entr)* O.DMar + (((1/P.NiTi.Dens)*P.NiTi.R.DA*Stress) ... 
        - P.NiTi.R.DC*log(Temperature/P.NiTi.T0))* O.DRVol; %Entropy Change for Current Time Step
    
    O.TempOld   = Temperature; % Old Temperautre
    O.StressOld = Stress; % Old Stress
    O.MarOld    = O.M.MarVol; % Old Martensitic Volume
    O.RVolOld    = O.R.RVol; % Old R-Phase Volume
else
    
    O.A = P.NiTi.M.ExpanA1 + O.M.MarVol * ( P.NiTi.M.ExpanM - P.NiTi.M.ExpanA1) ; % Thermal Expansion [1/K]
    O.S = P.NiTi.M.SA1 + O.M.MarVol *( P.NiTi.M.SM - P.NiTi.M.SA1); % Stress Compliance [1/GPa]
    O.k = P.NiTi.M.kA1 + O.M.MarVol *( P.NiTi.M.kM - P.NiTi.M.kA1); % Stress Compliance [1/GPa]
    O.C = P.NiTi.R.CPA2 + O.M.MarVol *( P.NiTi.M.CPM - P.NiTi.M.CPA1); % Specific Heat Capacity [J/kg.K]
    %Strain
    
    O.Strain = (Stress * O.S) + O.A*(Temperature - P.NiTi.T0) + ( sign(Stress) * ( O.M.MarVol * P.NiTi.M.Hcur));
    
    %Energy
    
    O.Q =-( (Temperature* O.A* O.DStress) + (O.M.TBound + (Temperature * P.NiTi.M.DA * Stress) ...
        -(P.NiTi.Dens* P.NiTi.M.DC*log(Temperature/P.NiTi.T0)) ...
        + (P.NiTi.Dens *P.NiTi.M.Delta_entr * Temperature)) * O.DMar)/P.NiTi.Dens; % Energy Change for Current Time Step [J/m^3]
    
    O.Entropy  = ((1/P.NiTi.Dens) * O.A * O.DStress) + ((O.C/Temperature)* O.DTemperature) + ...
        (((1/P.NiTi.Dens)*P.NiTi.M.DA*Stress) - P.NiTi.M.DC*log(Temperature/P.NiTi.T0) ...
        + P.NiTi.M.Delta_entr)* O.DMar ;% Entropy Change for Current Time Step
    
    O.TempOld   = Temperature; % Old Temperautre
    O.StressOld = Stress; % Old Stress
    O.MarOld    = O.M.Marvol; % Old Martensitic Volume
end

