function SetGPStyle(enable)

darkColor = [102 102 102]/255;

if enable
    % Axes
    set(groot,'DefaultAxesXColor',darkColor);
    set(groot,'DefaultAxesYColor',darkColor);
    set(groot,'DefaultAxesZColor',darkColor);
    set(groot,'DefaultAxesGridColor',darkColor);
    set(groot,'DefaultAxesFontName','Avenir Next LT Pro');
    set(groot,'DefaultAxesBox','on');
    set(groot,'DefaultAxesXGrid','on');
    set(groot,'DefaultAxesYGrid','on');
    
    % Polar
    set(groot,'DefaultPolaraxesThetaColor',darkColor);
    set(groot,'DefaultPolaraxesRColor',darkColor);
    set(groot,'DefaultPolaraxesGridColor',darkColor);
    set(groot,'DefaultPolaraxesFontName','Avenir Next LT Pro');
    set(groot,'DefaultPolaraxesThetaDir','clockwise');
    set(groot,'DefaultPolaraxesThetaZeroLocation','top');
            
    % Legend
    set(groot,'DefaultLegendFontName','Avenir Next LT Pro');
    set(groot,'DefaultLegendEdgeColor',darkColor);
    set(groot,'DefaultLegendInterpreter','none');
    set(groot,'DefaultLegendTextColor',darkColor);
    set(groot,'DefaultLegendLocation','best');
    
    % Colorbar
    set(groot,'DefaultColorbarFontName','Avenir Next LT Pro');
else
    % Axes
    set(groot,'DefaultAxesColorOrder',get(groot,'FactoryAxesColorOrder'));
    set(groot,'DefaultAxesXColor',get(groot,'FactoryAxesXColor'));
    set(groot,'DefaultAxesYColor',get(groot,'FactoryAxesYColor'));
    set(groot,'DefaultAxesZColor',get(groot,'FactoryAxesXColor'));
    set(groot,'DefaultAxesGridColor',get(groot,'FactoryAxesGridColor'));
    set(groot,'DefaultAxesFontName',get(groot,'FactoryAxesFontName'));
    set(groot,'DefaultAxesBox',get(groot,'factoryAxesBox'));
    set(groot,'DefaultAxesXGrid',get(groot,'factoryAxesXGrid'));
    set(groot,'DefaultAxesYGrid',get(groot,'factoryAxesYGrid'));
    
    % Polar
    set(groot,'DefaultPolaraxesColorOrder',get(groot,'FactoryPolaraxesColorOrder'));
    set(groot,'DefaultPolaraxesThetaColor',get(groot,'factoryPolaraxesThetaColor'));
    set(groot,'DefaultPolaraxesRColor',get(groot,'factoryPolaraxesRColor'));
    set(groot,'DefaultPolaraxesGridColor',get(groot,'FactoryPolaraxesGridColor'));
    set(groot,'DefaultPolaraxesFontName',get(groot,'FactoryPolaraxesFontName'));
    set(groot,'DefaultPolaraxesThetaDir',get(groot,'FactoryPolaraxesThetaDir'));
    set(groot,'DefaultPolaraxesThetaZeroLocation',get(groot,'FactoryPolaraxesThetaZeroLocation'));
    
    % Legend
    set(groot,'DefaultLegendFontName',get(groot,'factoryLegendFontName'));
    set(groot,'DefaultLegendEdgeColor',get(groot,'FactoryLegendEdgeColor'));
    set(groot,'DefaultLegendInterpreter',get(groot,'factoryLegendInterpreter'));
    set(groot,'DefaultLegendTextColor',get(groot,'factoryLegendTextColor'));
    set(groot,'DefaultLegendLocation',get(groot,'factoryLegendLocation'));
    
    % Colorbar
    set(groot,'DefaultColorbarFontName',get(groot,'factoryColorbarFontName'));
end