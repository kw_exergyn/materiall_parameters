function [cost] = costCalcMaterial03(out, TDs, Opt,P)
% Calculate the cost of a given run
% Select the last run
ida = length(TDs.Temp.Data);


if P.DSC
    
    
%     tstep = mean(diff((out.NiTi.Qout.Time)));
    CpOffset = ((P.NiTi.M.CPM/1000)/P.Sim.logStep)*diff(out.Temp.Data(ida:end));
    CpOffset(end+1) = CpOffset(end);
    Q = (((squeeze(out.NiTi.Qout.Data(ida:end))/(P.Stack.Mass)))/1000)+CpOffset;
%     dQ = ((squeeze(out.NiTi.Q.Data(ida:end))/(P.Stack.Mass))*tstep)/1000;
    power = abs(((squeeze(out.NiTi.Qout.Data(ida:end))/(P.Stack.Mass)))/1000);
        
    power(power == 0) =nan;
    power(power > 0) = 1;
    Q = abs(Q(1:end) .* power);
   
    DSCPower = abs(TDs.Power.Data);
    DSCPower(DSCPower == 0) =nan;
    DSCPower(DSCPower > 0) = 1;
    
    DSCQ =   TDs.Power.Data .*DSCPower;
    HeatError = abs(DSCQ-Q);
%     idx1 = round(length(Eout)/2);
%     Eout = max(Eout(1:idx1))-min(Eout(1:idx1));
%     HeatError = abs(TDs.Energy.Data-Eout);
    
    cost = double((rms(HeatError,'omitnan')));
    
    if P.NiTi.M.Y0 <=0 ||  P.NiTi.M.A1F < P.NiTi.M.MS 
        cost = 20;
    end
%     if cost >20
%         cost =20;
%     end
     
else
    
    try
        simStrain = out.Strain.Data(ida:end)*100;
    catch
        try
            simStrain = squeeze(out.NiTi.NiTi.Strain.Data(ida:end))*100;
        catch
            simStrain = out.NiTi.Strain.Data(ida:end)*100;
        end
    end
    simTime = out.NiTi.Strain.Time;
    simStrain = (simStrain-simStrain(1));
    SimVel = abs(diff(simStrain));
    SimVel(SimVel < 0.5e-3) =nan;
    SimVel(SimVel > 0.5e-3) = 1;
    
    simStrain = simStrain(2:end) .* SimVel;
    
    Stress = squeeze(TDs.Stress.Data);
    
    MeasStrain = squeeze(TDs.Strain.Data);
    MeasVel = abs(diff(MeasStrain));
    MeasVel(MeasVel < 0.5e-3) =nan;
    MeasVel(MeasVel > 0.5e-3) = 1;
    
    MeasStrain = MeasStrain(2:end) .* MeasVel;
    
    A = (MeasStrain-simStrain);
    StrainError = double((rms(A,'omitnan')));
    
    StrainErrorCheck =isnan(StrainError);
    if StrainErrorCheck
        StrainError = 5;
    end
    
    tstep = mean(diff(out.NiTi.Qout.Time));
    
    % heatTarget = 15;        % Target Qout [kJ]
    Eout = cumsum((out.NiTi.Qout.Data(ida:end)/(P.Stack.Mass))*tstep)/1000;
    idx1 = round(length(Eout)/2);
    Eout = max(Eout(1:idx1))-min(Eout(1:idx1));
    HeatError = abs(Opt.EnergyDensityTargetHeat-Eout);
    
    if Opt.EnergyDensityTargetHeat >0
        
        cost = StrainError + (HeatError)/10;
        
    else
        cost = StrainError;
    end
    
    if P.NiTi.M.Y0 <=0  || P.NiTi.M.A1F < P.NiTi.M.MS
        cost = cost + 2;
    end
end

% figure
% hold on
% plot((out.NiTi.Strain.Data(ida:end) - (out.NiTi.Strain.Data(ida))) .*100,out.Stress.Data(ida:end) ./1e6,'DisplayName','Sim')
% plot(TDs.Strain.Data,TDs.Stress.Data ./1e6, 'DisplayName', 'Test')
% legend
% title('NiTiCuV Strain vs Stress')
% xlabel('Strain [%]')
% ylabel('Stress [MPa]')


%% Write stats to file
% stats.cost = cost;
% stats.StrainError = StrainError;
% stats.Yo = P.NiTi.M.Y0;
% stats.HeatError = HeatError;
