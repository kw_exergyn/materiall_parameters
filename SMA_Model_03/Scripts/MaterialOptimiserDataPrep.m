clear
close all
h = gobjects(0);
SetGPStyle(1);
subplot = @(m,n,p) subtightplotGP (m, n, p);

% Get The default Parameters
[Inputs, P] = getDefaultParams03(0);

model = 'MaterialOptimisation.slx';

%% Load the Test Data
path = 'C:\Users\kevin\Desktop\Simulation\Material_Parameters\SMA_Model_03\TestData\Material\';
core = 1;
corestr = num2str(core);

filename{1} = '21-04-14_15-12-05_C1Iso40CRun2.mat';
t{1}.Start = 4;
t{1}.End = 200;

% filename{end+1} = '21-04-14_16-14-06_C1Iso40CRun3.mat';
% t{end+1}.Start = 0;
% t{end}.End = 200;

filename{end+1} = '21-04-14_16-27-22_C1Iso50CRun1.mat';
t{end+1}.Start = 2;
t{end}.End = 190;

% filename{end+1} = '21-04-14_17-11-47_C1Iso50CRun2.mat';
% t{end+1}.Start = 0;
% t{end}.End = 220;

tags{1} = ['D_Cr' corestr 'Load'];
tags{2} = ['CI_HydCr' corestr 'D'];
tags{3} = ['CI_FdsCr' corestr 'InTT_Filt'];
tags{4} = ['CI_FdsCr' corestr 'OutTT_Filt'];

Scales(1) = 1e3;
Scales(2) = 1;
Scales(3) = 1;
Scales(4) = 1;

Units{1} = 'N';
Units{2} = 'mm';
Units{3} = '°C';
Units{4} = '°C';

ntags = length(tags);

nDatasets = length(t);

for i = 1:nDatasets
    td = load([path filename{i}]);
    td = processSignals(td);
    for iT = 1:ntags
        if i == 1
            td.(tags{iT}).TimeInfo.StartDate = '';
            TD.(tags{iT}) = cropTS(td.(tags{iT})*Scales(iT),t{i});
        else
            td.(tags{iT}).TimeInfo.StartDate = '';
            td.(tags{iT}) = cropTS(td.(tags{iT})*Scales(iT),t{i});
            td.(tags{iT}).Time = td.(tags{iT}).Time+TD.(tags{iT}).Time(end)+0.01;
            TD.(tags{iT}) = append(TD.(tags{iT}), td.(tags{iT}));
            if i == nDatasets
                TD.(tags{iT}).Name = tags{iT};
                TD.(tags{iT}).DataInfo.units = Units{iT};
            end
        end
    end
end
TestName = 'Isothermal Charaterisation';


%% Prepare Data
% Plot Summary of run
h = plotSummary(TD,h,TestName,tags);

%%
% Configure Inputs
Force = TD.(tags{1});
tmax = Force.Time(end);
ts = 0.1;
Time = 0:ts:tmax;
Force = interp1(Force.Time, squeeze(Force.Data), Time);
TDs.Force = timeseries(Force,Time);
TDs.Force.DataInfo.Units = 'N';
TDs.Force.Name = 'Force';

TDs.Stress = TDs.Force./P.Stack.AreaM2;
TDs.Stress.DataInfo.Units = 'Pa';
TDs.Stress.Name = 'Stress';

Displacement = TD.(tags{2});
Displacement = interp1(Displacement.Time, squeeze(Displacement.Data-Displacement.Data(1)), Time);
TDs.Displacement = timeseries(Displacement,Time);
TDs.Displacement.Name = 'Displacement';

TDs.Strain = (TDs.Displacement/(P.Stack.Height*P.nStacks))*100;
TDs.Strain.DataInfo.Units = '%';
TDs.Strain.Name = 'Strain';

Temp = (TD.(tags{3}) + TD.(tags{4}))/2+273.15;
Temp = interp1(Temp.Time, squeeze(Temp.Data), Time);
TDs.Temp = timeseries(Temp,Time);
TDs.Temp.DataInfo.Units = 'K';
TDs.Temp.Name = 'Temperature';

TransDirTime = [0 60 60+ts 196 258 tmax];
TransDirData = [1 1 -1 1 -1 -1];
TransDir = interp1(TransDirTime, TransDirData ,Time,'previous');
TDs.TransDir = timeseries(TransDir,Time);
TDs.TransDir.Name = 'Transformation Direction';

save('MaterialOptimiserInputData.mat','TDs');


%% Functions
function h = plotSummary(TD,h,TestName,tags)
    subplot = @(m,n,p) subtightplotGP (m, n, p);
    a = gobjects(0);
    
    h(end+1) = figure('name',[TestName ' - Summary'],'NumberTitle','off','units','normalized','outerposition',[0 0.02 1 0.98]);
    m = 2;
    n = 2;
    nt = length(tags);
    for i = 1:nt
        a(i) = subplot(m,n,i);
        plot(TD.(tags{i}))
    end

    linkaxes(a,'x')
end

function TS = cropTS(TS,t)
    name = TS.Name;
    idx1 = find(TS.Time >= t.Start,1,'first');
    idx2 = find(TS.Time <= t.End,1,'last');
    TS = getsamples(TS,idx1:idx2);
    TS.Name = name;
    TS.Time = TS.Time-TS.Time(1);
end