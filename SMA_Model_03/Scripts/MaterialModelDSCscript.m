% clear
% close all

%% Zero Stress DSC Model
% This model performs a dsc cycle with the material model is order to 
% Debug the material parameters used and to check with alignment to the 
% DSC test results

% Full equations and derivations can be found in chapter 3 of "shape memory
% alloys modeling and engineering applications" Dimitris C. Lagoudas. 1D
% reduction has been used throughout.

%% Material Parameters
%     MaterialDBLX3
P.CYC.PhaseShift = 0;%K
% NiTiCuV % Material Parameters Script
%  L12SMA005
% X4_NiTi_Param_00
% NiTiCr
P.NiTi.YCal = 1;
P.CYC.Thigh = 200; %°C
P.CYC.Tlow = -150; %°C
P.NiTi.ExpanM = P.NiTi.ExpanM;
P.NiTi.ExpanA = P.NiTi.ExpanA;
P.NiTi.T0= 0;
P.NiTi.n=0.0001; 
P.CYC.dec = 3; % Number Of Decimal Places  Martensitic Volume Calculated to

%% Unit Conversion
P.CYC.Thigh = P.CYC.Thigh + 273.15;%K
P.CYC.Tlow = P.CYC.Tlow + 273.15;%K

%% Setup Calcs

%Limits of the Thermodynamic Boundary

%Forward Transformation
% Martensite Volume =1

P.NiTi.Marone = round(P.NiTi.Marone, P.CYC.dec);

% Martensite Volume =0

P.NiTi.Marzero = round(P.NiTi.Marzero, P.CYC.dec);

%Reverse Transformation
% Martensite Volume =1

P.NiTi.MaroneR = round(P.NiTi.MaroneR, P.CYC.dec);

% Martensite Volume =0

P.NiTi.MarzeroR = round(P.NiTi.MarzeroR, P.CYC.dec);


%Array Setup
P.RES.Strain = [];
P.RES.StrainRB = [];

P.RES.Temperature = [];
P.RES.TemperatureB = [];
P.CYC.TimeRan  = [];
P.RES.TemperatureF = [];
P.RES.TemperatureFi = [];

P.RES.STRESS = [];
P.RES.STRESSRB = [];
P.RES.STRESSI = [];

P.RES.EntropyR = [];
P.RES.EntropyRB = [];

P.RES.QCycle = [];
P.RES.QForward = [];
P.RES.Qreverse = [];

%% Cycle Sequence (Hybrid Cycle)
%initial State
%The material temperature is above Af,unloaded and not deformed.
P.CYC.T=P.CYC.Thigh;
P.CYC.T0=P.CYC.T;%K

P.CYC.Entr=-P.NiTi.Delta_entr-10;%J/K
P.CYC.Stress=0; % Intial Material Stress
P.CYC.Prev_Stress=0; %Used in finding if load is increasing/decreasing
P.CYC.Prev_Stress_Dirc=0;


P.CYC.En_row=1;
P.CYC.H=0; %Strain is initally 0
P.CYC.Stress_Dirc=0;

P.CYC.Mar_vol = 0; % Initial Martensite Volume Fraction
P.CYC.Mar_vol_old = 0;
P.CYC.Mar_volR = 1; % Initial Martensite Minor Loop Volume Fraction
P.NiTi.S = P.NiTi.Sa;% Initial Stress Compliance
P.NiTi.Alpha = P.NiTi.ExpanA; %Inital Thermal Expansion Factor
P.CYC.Etr = P.NiTi.Hcur;% Inital Transformation Strain at reversal
P.CYC.H=0; %Strain is initally 0
P.CYC.H_old =P.CYC.H;
P.CYC.Prev_Dirc=0; %inital Previous Transformtion Direction
P.CYC.Ms_stressn=0; %Initial Previous Ms_stress
P.CYC.Dirc=0;%inital Transformtion Direction
P.CYC.k=0;% Minor Loop in not initally activated
P.CYC.iold=0;
P.CYC.EntrS=0;
P.CYC.Q=0;
P.CYC.i=1000e6;
P.CYC.Trans=1;
P.CYC.Qf=0;
P.CYC.Time=0;
P.CYC.Qpower=0;
P.CYC.Dirc =1;

%Minor Loop Setup

P.CYC.Cn=min(P.NiTi.C_,P.CYC.Mar_volR); % Material Constant
P.CYC.ntho= -(P.NiTi.y/P.CYC.Cn)*1.5;
P.CYC.An=P.NiTi.Y0/(1-exp(P.CYC.ntho));
%% CYCLE
% for i = 0:20e6:1500e6
for T =[P.CYC.Thigh:-0.5:P.CYC.Tlow,P.CYC.Tlow: 0.5:P.CYC.Thigh]
    P.CYC.T=T;
    
    P.CYC.Trans=P.CYC.Dirc;
        
        [P]=TranDirc(P);
        [P]=InterVar(P);
        
        if P.CYC.Trans==1
            
            [P]=ForTrans(P);
            
        elseif P.CYC.Trans == -1
            
            [P]=RevsTrans(P);
            
        else
            
            P.CYC.DQ=0;
            P.CYC.DS=0;
            P.CYC.Mar_vol=P.CYC.Mar_vol;
            P.CYC.Mar_vol_old=P.CYC.Mar_vol;
            P.CYC.H=P.CYC.H;
        end
    P.RES.Mar_vol(P.CYC.En_row,1) = P.CYC.Mar_vol;
    
    P.RES.Trans(P.CYC.En_row,1) = P.CYC.Trans;
    
    P.RES.Strain(P.CYC.En_row,1)=P.CYC.H;
    
    P.RES.Temperature(P.CYC.En_row,1)=P.CYC.T;
    
    P.RES.STRESS(P.CYC.En_row,1)=P.CYC.i;
    
    P.RES.QMaterial(P.CYC.En_row,1)=P.CYC.DQ;
    
    P.RES.Qlatent(P.CYC.En_row,1)=P.NiTi.Y;
    
    P.CYC.En_row = P.CYC.En_row + 1;
end

figure(2)
tstep=0.5*(60/4);
hold on
% plot(P.RES.Temperature, movmean(-P.RES.QMaterial,0))
plot(P.RES.Temperature-273.15, ((-P.RES.QMaterial/(tstep))))
grid on
xlabel('Temperature [°C]')
title('DSC Model Analysis')
ylabel('W/kg')

for i = 2: length(P.RES.Temperature)

EntChg(i,1) = P.NiTi.cp * log(P.RES.Temperature(i)/P.RES.Temperature(i-1));
EngTChg(i,1) = P.RES.QMaterial(i) /P.RES.Temperature(i);
end

figure(3)
hold on
plot(cumsum(EntChg),'DisplayName', 'Log');
plot(cumsum(EngTChg),'DisplayName', 'Iso');
legend
title('Entropy')

figure(4)
Energy = cumsum(P.RES.QMaterial);
plot(P.RES.Temperature-273.15,Energy/1000)

figure(5)
hold on
plot(P.RES.Strain,P.RES.Temperature)
%% Thermomechanical Function
function [P]=TranDirc(P)

P.CYC.Di=P.CYC.i-P.CYC.iold;
P.CYC.iold=P.CYC.i;
%% Transformation Direction

% Ms_stress=(i/10^6)+((2*T)); % This just determines if the Stress/Temperature is increasing/decreasing
P.CYC.Ms_stress = (((0.5*P.NiTi.Delta_S*P.CYC.i^2)+(P.CYC.i*P.NiTi.Hcur)+(P.NiTi.Dens*P.NiTi.Delta_entr* P.CYC.T))*(-1/(P.NiTi.Dens*P.NiTi.Delta_entr)));
P.CYC.ActDiff = P.CYC.Ms_stress - P.CYC.Ms_stressn; % Has MS_stress increased or decreased since the last iteration
P.CYC.Ms_stressn = P.CYC.Ms_stress;% Store Ms_stress value for next iteration


%Forward Transformation
if P.CYC.ActDiff > 0
    % Initiate Minor Loop
    if (P.CYC.Prev_Dirc == 0) && P.CYC.Mar_vol < 1 && P.CYC.Mar_vol > 0
        
        P.CYC.r=1; %Minor loop is only initialising and parameter must be recalculated
        P.CYC.k=1; %Minor is active
        
    else
        P.CYC.r=0; % Minor is active but beyond the intial state
        P.CYC.k=P.CYC.k;
    end
    
    P.CYC.Dirc=1; % Forward Tansformation
end

%Reversal Point
if P.CYC.ActDiff==0
    Dirc=0; % Transformation is neither increasing/decreasing
    P.CYC.r=0;
    P.CYC.k=P.CYC.k;
end

%Reverse Transformaton
if P.CYC.ActDiff < 0
    % Initiate Minor Loop
    if P.CYC.Prev_Dirc == 0 && P.CYC.Mar_vol < 1 && P.CYC.Mar_vol > 0
        P.CYC.r=1;%Minor loop is only initialising and parameter must be recalculated
        P.CYC.k=1;%Minor is active
        
    else
        P.CYC.r=0;% Minor is active but beyond the intial stat
        P.CYC.k=P.CYC.k;
    end
    
    P.CYC.Dirc=-1;
end
P.CYC.Prev_Dirc = P.CYC.Dirc;  % Store Transformation Direction for next iteration

end
%% Internal Variable Recalculation
function [P]=InterVar(P)
P.NiTi.S = P.NiTi.Sa + ( P.CYC.Mar_vol*P.NiTi.Delta_S); %Stress Compliance
P.NiTi.Alpha = P.NiTi.ExpanA + ( P.CYC.Mar_vol*(P.NiTi.ExpanM-P.NiTi.ExpanA)); %Thermal Expansion Coefficient
P.CYC.K = P.NiTi.kA + ( P.CYC.Mar_vol*(P.NiTi.kM-P.NiTi.kA));

if P.CYC.i >= 0 % used to indicate the direction of loading(compression/Tension)
    P.CYC.sgn = 1; %Tension
else
    P.CYC.sgn = -1;% Compression
end
%Full Loop
P.NiTi.A1 = P.NiTi.Dens * P.NiTi.Delta_entr * ( P.NiTi.Mf - P.NiTi.Ms );%J/m3.K Forward Transformation
P.NiTi.A2 = P.NiTi.Dens * P.NiTi.Delta_entr * ( P.NiTi.As - P.NiTi.Af );%J/m3.K Reverse Transformation

%Smooth Hardening Behaviour
P.NiTi.A3a = ( ( 0.25 * P.NiTi.A1 ) * ( 1 + ( 1 / ( P.NiTi.N1 + 1 ) ) - ( 1 / ( P.NiTi.N2 + 1 ) ) ) );
P.NiTi.A3b = ( ( 0.25 * P.NiTi.A2 ) * ( 1 + ( 1 / ( P.NiTi.N3 + 1 ) ) - ( 1 / ( P.NiTi.N4 + 1 ) ) ) );
P.NiTi.A3 = -P.NiTi.A3a + P.NiTi.A3b;

if P.CYC.k == 0
    P.NiTi.Y =( 0.5 * P.NiTi.Dens * P.NiTi.Delta_entr * ( (P.NiTi.Ms - P.NiTi.Af) ) ) - P.NiTi.A3; %J/m3.K Hystersis Energy Disappation [Full loop]
end

P.NiTi.Delta_InEn = 0.5 * P.NiTi.Dens * P.NiTi.Delta_entr *( P.NiTi.Ms + P.NiTi.Af);%J/m3.K Reference Internal Energy

% Minor Loop
if P.CYC.Mar_vol > 0 && P.CYC.Mar_vol < 1 && P.CYC.k == 1
    
    if P.CYC.r == 1 % Reversal point Paramters
        
        P.CYC.Ex = 1 / P.NiTi.S; %Update the Young's Modulus
        
        P.CYC.Etr = ( P.CYC.i / P.CYC.Ex ) - P.CYC.H + ( Alpha *  (P.CYC.T - P.NiTi.T0 ) ); %Recalcualte the Transformation Strain
        P.CYC.Etr = abs( P.CYC.Etr );
        
        P.CYC.Mar_volR = P.CYC.Mar_vol;%Set the Reversal Martensitic Volume to the current volum
        P.NiTi.Y0 = P.NiTi.Y; % Hystersis dispatiion at Reversal
        P.CYC.Cn = min( P.NiTi.C_,P.CYC.Mar_volR ); % Material Constant
        
        P.CYC.ntho= -( P.NiTi.y / P.CYC.Cn ) * 1.5;
        P.CYC.An = P.NiTi.Y0 / ( 1 - exp( P.CYC.ntho ) );
        
        P.CYC.r = 0;
        
    end
    
    P.CYC.nth= -( P.NiTi.y / P.NiTi.Cn ) * abs( P.CYC.Mar_vol - P.CYC.Mar_volR );
    if P.CYC.nth == 0
        P.CYC.nth = 1e-6;
    end
    
    P.NiTi.Y = ( ( (P.CYC.An ) * ( 1 - exp( P.CYC.nth ) ) ) );
    
end
end
%% Forward Transformation
function [P]=ForTrans(P)


P.CYC.Mar_vol_G = ( ( ( ( 1 - P.NiTi.D ) * P.NiTi.Hcur * P.CYC.i ) + (0.5 * P.NiTi.Delta_S *( P.CYC.i ^ 2 ) ) + ( P.NiTi.Dens * P.NiTi.Delta_entr *( P.CYC.T ) ) - P.NiTi.Delta_InEn - P.NiTi.Y - P.NiTi.A3 ) / ( 0.5 * P.NiTi.A1));
P.CYC.Mar_vol_G = round(P.CYC.Mar_vol_G, P.CYC.dec);
m=P.CYC.Mar_vol;

if  P.CYC.Mar_vol_G <  P.NiTi.Marone && P.CYC.Mar_vol_G > P.NiTi.Marzero
    %Find what value of Martensite equal the current Hardening Value
    while m < 1
        
        Mar= ((1+(m^(P.NiTi.N1))-((1-m)^(P.NiTi.N2))));
        Mar = round(Mar, P.CYC.dec);
        
        if P.CYC.Mar_vol_G == Mar
            P.CYC.Mar_vol = m;
            
            m = 1;
        else
            m = m + P.NiTi.n;
        end
        
    end
    
end

if P.CYC.Mar_vol >= 0.998
    P.CYC.Mar_vol = 1;
    P.CYC.k = 0 ;
    
elseif P.CYC.Mar_vol <= 0
    P.CYC.Mar_vol = 0;
    P.CYC.k = 0;
end

P.CYC.H = ( P.NiTi.S * P.CYC.i ) +( P.NiTi.Alpha *( P.CYC.T -P.NiTi.T0 ) ) + ( P.NiTi.Hcur * P.CYC.Mar_vol );% Update Material Strain

P.CYC.Delta_H = P.CYC.H - P.CYC.H_old;

P.CYC.H_old =P.CYC.H;

P.CYC.Delta_Mar = P.CYC.Mar_vol - P.CYC.Mar_vol_old; % Find Change in Martensitic Volume

% Adiabatic Temperature Change


P.CYC.C = ( ( -P.NiTi.Y *P.NiTi.YCal ) + ( P.NiTi.Dens * P.NiTi.Delta_entr*P.CYC.T));

    P.CYC.DQ = ( ( -P.CYC.T * P.NiTi.Alpha * P.CYC.Di ) + ( P.CYC.C * P.CYC.Delta_Mar ) ) / P.NiTi.Dens;
% P.CYC.DQ = (   ( P.CYC.C * P.CYC.Delta_Mar ) ) / P.NiTi.Dens;

P.CYC.Mar_vol_old = P.CYC.Mar_vol;
P.CYC.k = P.CYC.k;
end
%% Reverse Transformation
function[P]=RevsTrans(P)
P.CYC.Mar_vol_G = ( ( ( -( 1 + P.NiTi.D ) * P.CYC.i *( P.NiTi.Hcur ) ) -( 0.5 * P.NiTi.Delta_S *(P.CYC.i^2 ) ) - (P.NiTi.Dens * P.NiTi.Delta_entr *( P.CYC.T ) ) +( P.NiTi.Delta_InEn) - P.NiTi.Y- P.NiTi.A3 ) / ( -0.5 * P.NiTi.A2 ) );
P.CYC.Mar_vol_G = round(P.CYC.Mar_vol_G, P.CYC.dec);
m = P.CYC.Mar_vol;

if  P.CYC.Mar_vol_G <  P.NiTi.MaroneR && P.CYC.Mar_vol_G > P.NiTi.MarzeroR
    %Find what value of Martensite equal the current Hardening Value
    while m > 0
        
        P.CYC.Mar= ( 1 + ( m ^( P.NiTi.N3 ) ) - ( ( 1 - m) ^ (P.NiTi.N4 ) ) );
        P.CYC.Mar = round(P.CYC.Mar, P.CYC.dec );
        
        if P.CYC.Mar_vol_G == P.CYC.Mar
            P.CYC.Mar_vol = m;
            
            
            m = 0;
        else
            m = m - P.NiTi.n ;
        end
    end
end

if P.CYC.Mar_vol >= 1
    P.CYC.Mar_vol = 1;
    P.CYC.k = 0;
elseif P.CYC.Mar_vol <= 0.001
    P.CYC.Mar_vol = 0;
    P.CYC.k = 0;
end


P.CYC.H = ( P.NiTi.S * P.CYC.i ) +( P.NiTi.Alpha *( P.CYC.T - P.NiTi.T0 ) ) + ( ( P.CYC.Etr/P.CYC.Mar_volR ) * P.CYC.Mar_vol ); % Change in Material Strain

P.CYC.Delta_H = P.CYC.H - P.CYC.H_old;

P.CYC.H_old =P.CYC.H;

P.CYC.Delta_Mar= (P.CYC.Mar_vol - P.CYC.Mar_vol_old); %Find Change in Martensitic Volume


P.CYC.C = ((P.NiTi.Y*P.NiTi.YCal) + (P.NiTi.Dens*P.NiTi.Delta_entr*P.CYC.T));

    P.CYC.DQ=( (  -P.CYC.T * P.NiTi.Alpha * P.CYC.Di)+( P.CYC.C * P.CYC.Delta_Mar ) ) / P.NiTi.Dens;
% P.CYC.DQ=( ( P.CYC.C * P.CYC.Delta_Mar ) ) / P.NiTi.Dens;


P.CYC.Mar_vol_old = P.CYC.Mar_vol;
P.CYC.k=P.CYC.k;
end