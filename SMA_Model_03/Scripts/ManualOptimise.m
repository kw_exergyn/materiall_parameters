clear all
close all

% Get Them parameters
[O,P] =  getDefaultParams03(0);
startTime = datestr(now,'YYYY-mm-DD_HH-MM');
Proj = matlab.project.rootProject;

mdlName = 'MaterialOptimisation_03';
load_system(mdlName);

if string( get_param( mdlName, 'Dirty') ) == "on"
    save_system( mdlName );
end

%% Load data
             load('MaterialOptimiserData_NiTiCuV_S01_iCW_450C_30min_DSC_01.mat')
                 load('MaterialOptimiserData_NiTiCuV_S01_iCW_450C_30min_Strain_35degC.mat')
%                        load('MaterialOptimiserData_NiTiCuV_S01_iCW_450C_30min_Strain_10degC.mat')
%           load('MaterialOptimiserData_NiTiCuV_S01_iCW_450C_30min_Strain_m10degC.mat')

Opt.dt = mean(diff(TDs.Temp.Time));

%% SetupData

B = TDs.Temp.Data;
B =cat(1,B,TDs.Temp.Data);
C = (0:mean(diff(TDs.Temp.Time)):((TDs.Temp.Time(end)*2)))';

Inputs.Temp = timeseries((B(1:end-1)),C);
% Inputs.Temp.Data(1:end) = Inputs.Temp.Data(1);

B = TDs.Force.Data;
B =cat(1,B,TDs.Force.Data);
Inputs.Force = timeseries(B(1:end-1),C);
% Inputs.Force = timeseries(TDs.Force.Data,TDs.Temp.Time);
Inputs.Force.Data(1:20) = 0;

B = TDs.TransDir.Data;
B =cat(1,B,TDs.TransDir.Data);
Inputs.TransDir = timeseries(B(1:end-1),C);
% Inputs.TransDir = timeseries(TDs.TransDir.Data,TDs.Temp.Time);

%% Get Parameters

P.Sim.logStep = Opt.dt;
P.Sim.tStep = Opt.dt;                   % Time Step [s]

P.Sim.maxTime = TDs.Temp.Time(end)*2;
P.showPlots = 0;

P.FDS.InitTemp = mean(TDs.Temp.Data(1:10))-273.15;
P.FDS.Tmid = P.FDS.InitTemp;
P.FDS.AmbientTemp = 16;
P.FDS.FluidPressure = 4;
P.NiTi.ForceInt = 0;

P.InitTemp = P.FDS.InitTemp;

%% Material Parameters
% P.NiTi.M.Hcur	=	0.028; % [m/m]
% P.NiTi.M.MS	    =	300; % [K]
% P.NiTi.M.MF	    =	165; % [K]
% P.NiTi.M.A1S	=	210; % [K]
% P.NiTi.M.A1F	=	320; % [K]
% P.NiTi.M.N1	    =	0.35; % []
% P.NiTi.M.N2	    =	0.3; % []
% P.NiTi.M.N3	    =	0.2; % []
% P.NiTi.M.N4	    =	0.4; % []
% P.NiTi.M.CA1	=	13.1e6; % [Pa/K]
% P.NiTi.M.EA1	=	60e9; % [Pa]
% P.NiTi.M.EM	    =	55e9; % [Pa]
% P.NiTi.M.y	    =	3; % []
% P.NiTi.M.C_	    =	0.37091; % []
% P.NiTi.M.CalS	 =	1; % [Pa]
% P.NiTi.M.CM = P.NiTi.M.CA1;

P.NiTi.M.Hcur	=	0.041852; % [m/m]
P.NiTi.M.MS	=	302.2918; % [K]
P.NiTi.M.MF	=	89.7174; % [K]
P.NiTi.M.A1S	=	166.3381; % [K]
P.NiTi.M.A1F	=	318.1065; % [K]
P.NiTi.M.N1	=	0.59982; % []
P.NiTi.M.N2	=	0.38476; % []
P.NiTi.M.N3	=	0.24922; % []
P.NiTi.M.N4	=	0.34165; % []
P.NiTi.M.CA1	=	12255857.7178; % [Pa/K]
P.NiTi.M.EA1	=	62117278866.6646; % [Pa]
P.NiTi.M.EM	=	73179642144.0241; % [Pa]
P.NiTi.M.y	=	4.2143; % []
P.NiTi.M.C_	=	0.2817; % []
P.NiTi.M.CM = P.NiTi.M.CA1;
P.DSC = 1;
MPhaseMat03




%% Create Simulink Workspace Files
simIn = Simulink.SimulationInput(mdlName);
simIn = simIn.setVariable('P',P);
simIn = simIn.setVariable('O',O);
simIn = simIn.setVariable('Inputs',Inputs);


out = sim(simIn);

%% Plot data

ida = length(TDs.Temp.Data);

CpOffset = ((P.NiTi.M.CPM/1000)/P.Sim.logStep)*diff(out.Temp.Data(ida:end));
CpOffset(end+1) = CpOffset(end);
Q = (((squeeze(out.Q.Data(ida:end))/(P.Stack.Mass)))/1000)+CpOffset;
%     dQ = ((squeeze(out.NiTi.Q.Data(ida:end))/(P.Stack.Mass))*tstep)/1000;
power = abs(((squeeze(out.Q.Data(ida:end))/(P.Stack.Mass)))/1000);
Qout = Q;
power(power == 0) =nan;
power(power > 0) = 1;
Q = abs(Q(1:end) .* power);
try
DSCPower = abs(TDs.Power.Data);
DSCPower(DSCPower == 0) =nan;
DSCPower(DSCPower > 0) = 1;

DSCQ =   TDs.Power.Data .*DSCPower;
HeatError = abs(DSCQ-Q);
end
%% Figure
figure('Name', 'Thermodynamic Cycles Temperature Entropy','units','normalized','outerposition', [0,0.02,1,0.98],'DefaultAxesFontSize',12)
t = tiledlayout(2,2);
t.TileSpacing = 'compact';
t.Padding = 'compact';

a = nexttile;
hold on
grid on
grid minor
try
plot(TDs.Temp.Data,TDs.Power.Data,'DisplayName','Measured');
end
plot(TDs.Temp.Data, Qout,'DisplayName','Sim')

title('DSC Power ')
xlabel('Temperature [K')
ylabel('Power [mW/mg]')

% gcaExpandable

a(end+1) = nexttile;

hold on
grid on
grid minor
try
plot(TDs.Temp.Data,cumsum(TDs.Power.Data*P.Sim.logStep),'DisplayName','Measured');
end
plot(TDs.Temp.Data, cumsum(Qout*P.Sim.logStep),'DisplayName','Sim')

title('DSC Energy ')
xlabel('Temperature [K]')
ylabel('Power [kJ/kg]')

% gcaExpandable

a(end+1) = nexttile;

hold on
grid on
grid minor
try
plot(TDs.Power.Time,cumsum(TDs.Power.Data*P.Sim.logStep),'DisplayName','Measured');
end
plot(out.NiTi.Strain.Time(1:ida),cumsum(Qout*P.Sim.logStep),'DisplayName','Sim')

% gcaExpandable
a(end+1) = nexttile;

hold on
grid on
grid minor
try
plot(TDs.Strain.Data,TDs.Stress.Data,'DisplayName','Measured');
end
plot((out.NiTi.Strain.Data(ida:end)-out.NiTi.Strain.Data(ida))*100,out.NiTi.Stress.Data(ida:end),'DisplayName','Sim')

% gcaExpandable
