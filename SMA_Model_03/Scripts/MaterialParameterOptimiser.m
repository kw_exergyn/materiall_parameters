clear all
close all

% Get Them parameters
[O,P] =  getDefaultParams03(0);

 P.DSC = 1;
startTime = datestr(now,'YYYY-mm-DD_HH-MM');
Proj = matlab.project.rootProject;

%% Get Model, save it and set fast restart on.
% mdlName = 'MaterialOptimisation';
mdlName = 'MaterialOptimisation_03';
load_system(mdlName);

if string( get_param( mdlName, 'Dirty') ) == "on"
    save_system( mdlName );
end
% set_param( mdlName, 'FastRestart', 'on' );

CrashRestart = 0; 
CrashFile = ''; 

%% Load data

if  P.DSC == 0

load('MaterialOptimiserData_NiTiCuV_S01_iCW_450C_30min.mat')
D.T1 = TDs;
load('MaterialOptimiserData_NiTiCuV_S01_iCW_450C_30min.mat')
D.T2 = TDs;
load('MaterialOptimiserData_NiTiCuV_S01_iCW_450C_30min.mat')
D.T3 = TDs;

else

   load() 'MaterialOptimiserData_NiTiCuV_S01_iCW_450C_30m_10c_DSC_01.mat')

end

% load('MaterialOptimiserData_SMA08.mat')
% h = plotInputData(TDs);

name = 'sb_Core_P';
tempStruct = Simulink.Bus.createObject(P);
eval([name ' = evalin(''base'',tempStruct.busName);']);

name = 'sb_Core_O';
tempStruct = Simulink.Bus.createObject(O);
eval([name ' = evalin(''base'',tempStruct.busName);']);

%% Configure Parameters to Optimise
Opt.Name = 'NiTiCuV';
    
Opt.TestNum = '00';

Opt.DimNames{1}       = 'P.NiTi.M.Hcur';
Opt.DimNames{end+1}   = 'P.NiTi.M.MS';
Opt.DimNames{end+1}   = 'P.NiTi.M.MF';
Opt.DimNames{end+1}   = 'P.NiTi.M.A1S';
Opt.DimNames{end+1}   = 'P.NiTi.M.A1F';
Opt.DimNames{end+1}   = 'P.NiTi.M.N1';
Opt.DimNames{end+1}   = 'P.NiTi.M.N2';
Opt.DimNames{end+1}   = 'P.NiTi.M.N3';
Opt.DimNames{end+1}   = 'P.NiTi.M.N4';
Opt.DimNames{end+1}   = 'P.NiTi.M.CA1';
Opt.DimNames{end+1}   = 'P.NiTi.M.EA1';
Opt.DimNames{end+1}   = 'P.NiTi.M.EM';
Opt.DimNames{end+1}   = 'P.NiTi.M.y';
Opt.DimNames{end+1}   = 'P.NiTi.M.C_';
% Opt.DimNames{end+1}  = 'P.NiTi.M.CPA1';




Opt.Units{1} = 'm/m';   
Opt.Units{end+1} = 'K';   
Opt.Units{end+1} = 'K';   
Opt.Units{end+1} = 'K';   
Opt.Units{end+1} = 'K';   
Opt.Units{end+1} = '';   
Opt.Units{end+1} = '';   
Opt.Units{end+1} = '';   
Opt.Units{end+1} = ''; 
Opt.Units{end+1} = 'Pa/K';   
Opt.Units{end+1} = 'Pa';   
Opt.Units{end+1} = 'Pa'; 
Opt.Units{end+1} = '';   
Opt.Units{end+1} = ''; 
% Opt.Units{end+1} = '';   
% Opt.Units{end+1} = ''; 
% Opt.Units{end+1} = 'J/kg.K';
%  Opt.Units{end+1} = 'Pa'; 




Opt.InitArray(1) =	0.047923; % [m/m]
Opt.InitArray(end+1) =	303.2879; % [K]
Opt.InitArray(end+1) =	151.0158; % [K]
Opt.InitArray(end+1) =	213.1036; % [K]
Opt.InitArray(end+1) =	324.9208; % [K]
Opt.InitArray(end+1) =	0.34827; % []
Opt.InitArray(end+1) =	0.59879; % []
Opt.InitArray(end+1) =	0.36632; % []
Opt.InitArray(end+1) =	0.55752; % []
Opt.InitArray(end+1) =	14969947.4225; % [Pa/K]
Opt.InitArray(end+1) =	50509602432.0704; % [Pa]
Opt.InitArray(end+1) =	59719887763.3494; % [Pa]
Opt.InitArray(end+1) =	1.229; % []
Opt.InitArray(end+1) =	0.34231; % []

Opt.lb =    [ 2 27 08 12 30 0.1 0.1 0.1 0.1 10  40 40  3 0.1 ];       % Lower limits
Opt.ub =    [ 6 31 12 22 33 0.6 0.6 0.6 0.6 15  75 75  5 0.5 ];        % Upper Limits

%% Optimisation Parameters
Opt.Scale = [1e-2 10 10 10 10 1 1 1 1 1e6 1e9 1e9 1 1 ];         % Define any scaling to apply to the channels
Opt.intcon = [];                     % Define which channels are integers
Opt.nD =    length(Opt.DimNames);
Opt.MaxEvals = 30000;
Opt.MinSampleDistance = 0.001;
Opt.Folder = [Proj.RootFolder{1} '\SMA_Model_03\Optimisation_Data\'];
Opt.dt = mean(diff(TDs.Temp.Time));
Opt.minRandom = 200;
Opt.ObjTarget = 0.15;
Opt.EnergyDensityTargetHeat1 = 10.7;    % kJ/kg
Opt.EnergyDensityTargetHeat2 = 11.6;    % kJ/kg
Opt.EnergyDensityTargetHeat3 = 9.6;    % kJ/kg
Opt.EnergyDensityTargetCool = 0;    % kJ/kg

for i = 1:Opt.nD
    Opt.InitArray(i) = Opt.InitArray(i)/Opt.Scale(i);
end

%% Prepare temp Data store 
 filename = ['Opt_Data_' startTime '.mat'];
% stats.cost = 0;
% % stats.StrainError = 0;
% stats.Yo = 0;
% % stats.HeatingError = 0;
% % stats.CoolingError = 0;
% % stats.Heating = 0;
% % stats.Cooling = 0;

for i = 1:Opt.nD
    fieldName = strrep(Opt.DimNames{i},'.','_');
    stats.(fieldName) = Opt.InitArray(i);
end
 save([Opt.Folder filename],'stats','Opt','D','-v7.3')

%% Cost function Handle
costFcnHandle = @(x) CostFuncMaterial03(filename, mdlName, Opt, D, x);

%% Run Optimisation
try
%     opts = optimoptions( 'surrogateopt', 'Display', 'iter', 'MinSurrogatePoints',Opt.minRandom,'MaxFunctionEvaluations', Opt.MaxEvals, 'PlotFcn', 'surrogateoptplot','UseParallel', true, 'MinSampleDistance', Opt.MinSampleDistance, 'InitialPoints', Opt.InitArray);
    if CrashRestart 
        opts = optimoptions( 'surrogateopt', 'Display', 'iter', 'MinSurrogatePoints',Opt.minRandom,'MaxFunctionEvaluations', Opt.MaxEvals, 'PlotFcn', 'surrogateoptplot','UseParallel', true, 'MinSampleDistance', Opt.MinSampleDistance,'ObjectiveLimit',Opt.ObjTarget,'CheckpointFile', CrashFile);
        [paramsOpt,fval,exitflag,output,trials] = surrogateopt(CrashFile,opts);
    else
opts = optimoptions( 'surrogateopt', 'Display', 'iter', 'MinSurrogatePoints',Opt.minRandom,'MaxFunctionEvaluations', Opt.MaxEvals, 'PlotFcn', 'surrogateoptplot','UseParallel', true, 'MinSampleDistance', Opt.MinSampleDistance,'ObjectiveLimit',Opt.ObjTarget );
%    
     [paramsOpt,fval,exitflag,output,trials] = surrogateopt( costFcnHandle, Opt.lb, Opt.ub, Opt.intcon, opts);
    set_param( mdlName, 'FastRestart', 'on' );
    end
%     ,'CheckpointFile', ['OptTemp_' startTime '.mat']
catch MExc
    MExc.throwAsCaller();
    set_param( mdlName, 'FastRestart', 'off' );
end

%% Save Results
save([Opt.Folder 'Optimisation_' startTime],'Opt','paramsOpt','fval','exitflag','output','trials')

%% Plot Results
Plot_Optimisation_Basic

% figure
% hold on
% plot((out.NiTi.Strain.Data - (out.NiTi.Strain.Data(1))) .*100,out.Stress.Data ./1e6,'DisplayName','Sim')
% plot(TDs.Strain.Data,TDs.Stress.Data ./1e6, 'DisplayName', 'Test')
% legend
% title('NiTiCuV Strain vs Stress')
% xlabel('Strain [%]')
% ylabel('Stress [MPa]')

%% Functions
function h = plotInputData(TDs)
    a = gobjects(0);
   
    
    
    h = figure('name','Input Summary','NumberTitle','off','units','normalized','outerposition',[0 0.02 1 0.98]);
   t = tiledlayout(2,2);
   a = nexttile;
    plot(TDs.Force)
    
%     a(2) = subplot(m,n,2);
%     plot(TDs.Stress)
%     
%     a(3) = subplot(m,n,3);
%     plot(TDs.Displacement)
%     
%     a(4) = subplot(m,n,4);
%     plot(TDs.Strain)
    
     a(end+1) = nexttile;
    plot(TDs.Temp)
    
   a(end+1) = nexttile;
    plot(TDs.TransDir)
    
%     subplot(m,n,7)
%     scatter(squeeze(TDs.Stress.Data)*1e-6, squeeze(TDs.Strain.Data))  
%     xlabel('Stress [MPa]')
%     ylabel('Strain [%]')
%     title('Stress - Strain')
    
    linkaxes(a,'x')
end