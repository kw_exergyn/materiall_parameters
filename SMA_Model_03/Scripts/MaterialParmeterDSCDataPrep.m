% This Model converts DSC data from experiment to arrays to be used with
% the Optimiser

clear
close all
SetGPStyle(1);
subplot = @(m,n,p) subtightplotGP(m, n, p);

h = gobjects(0);
a = gobjects(0);
b = gobjects(0);

Proj = matlab.project.rootProject;
Rstart = 18;
Colstart = 2;
TempRate = 5; %K/min

path = [Proj.RootFolder{1} '\SMA_Model_03\TestData\NewMaterial\'];
fileName = 'ExpDat_UTe_051b_021_USF_P_NiTiCuV_S01_iCW_450C_30m_10c.xlsx';

T = readtable([path,fileName]);
%Temperature
TD.TempRev =   table2array(T(Rstart:end,1));
TD.TempFor =   table2array(flipud(T(Rstart:end,1)));
TD.Temp = cat(1,TD.TempRev,TD.TempFor)+273.15;

Time = cat(1,0,(cumsum(abs(diff(TD.Temp))/(TempRate/60))));
%Power mW/mg
TD.PowerFor =  table2array(flipud(T(Rstart:end,Colstart)));
TD.PowerRev =  table2array(T(Rstart:end,Colstart+1));
TD.CPPowerFor =  table2array(flipud(T(Rstart:end,Colstart)));
TD.CPPowerRev =  table2array(T(Rstart:end,Colstart+1));
% TD.PowerFor =  table2array(flipud(T(18:end,2)));
% TD.PowerRev =  table2array(T(18:end,3));
PowerFor = TD.PowerFor;
PowerRev = TD.PowerRev;
TempFor  = TD.TempFor +273.15;
TempRev  = TD.TempRev + 273.15;

% Cprow = find(TD.Temp > -60+273.15,1);
% CPPower = TD.CPPowerRev(Cprow);
% dt= mean(diff(Time));
% Cp = abs(CPPower *dt);
PRev = [-1.443e-09 ,  1.512e-06, -0.0005207, 0.01977];
for i = 1: length(TD.TempRev)
   CPPowerRev(i,1) =  polyval(PRev,TD.TempRev(i)+273.15);
end

PFor = [1.341e-09, -1.492e-06, 0.0005298 , -0.02179];
for i = 1: length(TD.TempFor)
   CPPowerFor(i,1) =  polyval(PFor,TD.TempFor(i)+273.15);
end

% CPPowerRev = 0.000589*(TD.TempRev+273.15).^3 + 0.004572*(TD.TempRev+273.15).^2 +  0.006854*(TD.TempRev+273.15) -0.03714;
% CPPowerFor = 0.001078*(TD.TempFor+273.15).^3 -0.005202*(TD.TempFor+273.15).^2 +  0.005083*(TD.TempFor+273.15) + 0.03814;

TD.PowerRev = TD.PowerRev - CPPowerRev;
TD.PowerFor = TD.PowerFor - CPPowerFor;

TD.PowerRev = movmean(TD.PowerRev,20);
TD.PowerFor = movmean(TD.PowerFor,20);

for i = 1:length(TD.PowerRev)
    if TD.PowerRev(i) > 0
        TD.PowerRev(i) = 0;
    end
    
end
for i = 1:length(TD.PowerFor)
    if TD.PowerFor(i) < 0
        TD.PowerFor(i) = 0;
    end
    
end

TD.Power = cat(1,TD.PowerRev,TD.PowerFor);



TD.Force = zeros(1,length(TD.Power))';%Force
TransDirRev = - ones(1,length(TD.PowerRev))';
TransDirFor =  ones(1,length(TD.PowerFor))';
TD.TransDir = cat(1,TransDirRev,TransDirFor);

%Correction

TD.Energy = cumsum(TD.Power*mean(diff(Time)));





%% Make New Mat file for optimiser
TDs.Force = timeseries(TD.Force,Time);
TDs.Temp = timeseries(TD.Temp,Time);
TDs.TransDir = timeseries(TD.TransDir,Time);
TDs.Power = timeseries(TD.Power,Time);
TDs.Energy = timeseries(TD.Energy,Time);

  save('MaterialOptimiserData_NiTiCuV_S01_iCW_450C_30m_10c_DSC_01' ,'TDs')

%% Plots
m = 2;
n = 3;
h(end+1) = figure('name',[fileName],'NumberTitle','off','units','normalized','outerposition',[0.1 0.1 0.8 0.8]);
t = tiledlayout(2,3);
t.TileSpacing = 'compact';
t.Padding = 'compact';
a = nexttile;
hold on
plot(Time, TD.Force)

title('Force')
xlabel('Time [s]')
ylabel('Force [N]')


a(end+1)  = nexttile;
hold on
plot(Time, TD.Temp)

title('Temperatures')
xlabel('Time [s]')
ylabel('Temperature [K]')

a(end+1)  = nexttile;
hold on

plot(Time, TD.TransDir)
title('Transformation Direction')
xlabel('Time [s]')
ylabel('')


a(end+1)  = nexttile;
hold on
plot(Time, TD.Power)
title('Power')
xlabel('Time [s]')
ylabel('Power [mW/mg]')


a(end+1)  = nexttile;
hold on
plot(Time, TD.Energy)
title('Material Energy')
xlabel('Time [s]')
ylabel('Energy [KJ/kg]')

linkaxes(a,'x')



