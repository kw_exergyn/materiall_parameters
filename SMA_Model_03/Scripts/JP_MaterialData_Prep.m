clear
close all
SetGPStyle(1);
subplot = @(m,n,p) subtightplotGP(m, n, p);

h = gobjects(0);
a = gobjects(0);
b = gobjects(0);

Proj = matlab.project.rootProject;

path = 'C:\Users\keith\Documents\Simulation\Material_Parameters\SMA_RPhase_Model_01\TestData\X4_SMA_04\40deg\';
runDesc = 'NiTi';
runName = '0';
SaveName ='40deg';

TD = ReadJanLog(path, runName);

%% Time
Time = TD.Time_s;

dtMean = mean(diff(Time));
dt = [dtMean; diff(Time)];

if max(dt)>dtMean*1.01 || min(dt)<dtMean*0.99
    figure
    plot(Time,dt)
    ylim([0 0.1])
    xlabel('Time [s]')
    ylabel('Timestep [s]')
end

%% Calculate Transformation Direction
TD.DeltaStress = [0; diff((movmean(TD.Stress_MPa_smooth,80)))];
transDir = nan(size(TD.DeltaStress));
 
StressMax = min(round(TD.Stress_MPa_smooth));
StressMin = max(round(TD.Stress_MPa_smooth));


transDir(TD.DeltaStress<-0.05) = 1;
transDir(TD.DeltaStress>0.05) = -1;
transDir(1) = -1;
transDir(end) =-1;


% transDir(1) = (transDir(find(~isnan(transDir),1,'first')));
% transDir(end) = transDir(find(~isnan(transDir),1,'last'));
TD.TransDir = interp1(Time(~isnan(transDir)),transDir(~isnan(transDir)),Time,'previous');


%% Make New Mat file for optimiser
TDs.Force = timeseries(TD.Force_N_smooth,Time);
TDs.Stress = timeseries(TD.Stress_MPa_smooth*1e6,Time);
TDs.Displacement = timeseries(TD.Extensometer_mm_smooth,Time);
TDs.Strain = timeseries(TD.Extensometer_smooth,Time);
TDs.Temp = timeseries((TD.Temperature1_C_smooth+TD.Temperature2_C_smooth)/2+273.15,Time);
TDs.TransDir = timeseries(TD.TransDir,Time);

save(SaveName ,'TDs')

%% Plots
m = 2;
n = 3;
h(end+1) = figure('name',[runDesc ' - Stress Strain'],'NumberTitle','off','units','normalized','outerposition',[0.1 0.1 0.8 0.8]);
t = tiledlayout(2,3);
t.TileSpacing = 'compact';
t.Padding = 'compact';
a = nexttile;
hold on
plot(Time, TD.Stress_MPa)
plot(Time, TD.Stress_MPa_smooth)
title('Stress')
xlabel('Time [s]')
ylabel('Stress [MPa]')

a(end+1)  = nexttile;
hold on
% plot(Time, TD.Strain)
plot(Time, TD.Extensometer)
plot(Time, TD.Extensometer_smooth)
title('Strain')
xlabel('Time [s]')
ylabel('Strain [%]')

a(end+1)  = nexttile;
hold on
plot(Time, TD.Temperature1_C)
plot(Time, TD.Temperature2_C)
plot(Time, TD.Temperature3_C)
plot(Time, TD.Temperature1_C_smooth)
plot(Time, TD.Temperature2_C_smooth)
plot(Time, TD.Temperature3_C_smooth)
title('Temperatures')
xlabel('Time [s]')
ylabel('Temperature [°C]')

a(end+1)  = nexttile;
hold on
% plot(Time, TD.DeltaStress)
plot(Time, TD.TransDir)
title('Delta Stress')
xlabel('Time [s]')
ylabel('')
linkaxes(a,'x')

nexttile;
hold on
plot(TD.Extensometer, TD.Stress_MPa)
plot(TD.Extensometer_smooth, TD.Stress_MPa_smooth)
ylabel('Stress [MPa]')
xlabel('Strain [%]')
title('Stress-Strain Curve')

