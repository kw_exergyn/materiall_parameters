
Stack(1)= P.NiTi.ForceInt;

%% Core

for i=1:1:length(Stack)
    
P.NiTi.StressInt = Stack(i)/(P.Stack.Area/10^6);
% P.NiTi.StressInt = P.NiTi.ForceInt/(P.Stack.Area/10^6);

P.NiTi.M.DFDE =   ( ( ( 1 - P.NiTi.M.D ) * P.NiTi.M.Hcur * P.NiTi.StressInt ) + (0.5 * P.NiTi.M.Delta_S *( P.NiTi.StressInt ^ 2 ) ) +(P.NiTi.StressInt*P.NiTi.M.DA*((P.FDS.InitTemp+273.15) - P.NiTi.M.A1F)) + ( P.NiTi.Dens * P.NiTi.M.Delta_entr *( (P.FDS.InitTemp+273.15) ) ) - P.NiTi.M.InEn - P.NiTi.M.Y0-P.NiTi.M.A3)/( 0.5 * P.NiTi.M.A1 ) ;
P.NiTi.M.DFDE = round(P.NiTi.M.DFDE, 3);

m=0;
BB = 1e-4;
I = 0.999;
O = 0.001;
IN1 =((I^(1/P.NiTi.M.N1))/((I+BB)^((1/P.NiTi.M.N1)-1)))^P.NiTi.M.N1;
IN2 =(((1-I)^(1/P.NiTi.M.N2))/((1-I+BB)^((1/P.NiTi.M.N2)-1)))^P.NiTi.M.N2;
ON1 =((O^(1/P.NiTi.M.N1))/((O+BB)^((1/P.NiTi.M.N1)-1)))^P.NiTi.M.N1;
ON2 =(((1-O)^(1/P.NiTi.M.N2))/((1-O+BB)^((1/P.NiTi.M.N2)-1)))^P.NiTi.M.N2;

P.NiTi.M.M.Marone =(1+IN1 -IN2);
P.NiTi.M.Marzero =(1+ON1 -ON2);

% P.NiTi.Marone =  ((1+(0.999^(P.NiTi.N1))-((1-0.999)^(P.NiTi.N2))))  ;
% P.NiTi.Marzero = ((1+(1e-3^(P.NiTi.N1))-((1-1e-3)^(P.NiTi.N2))))  ;



if  P.NiTi.M.DFDE >= P.NiTi.M.Marone
    P.NiTi.M.Mar_volInt(i) = I-BB;
elseif P.NiTi.M.DFDE <= P.NiTi.M.Marzero
    P.NiTi.M.Mar_volInt(i) = O+BB;
else
    %Find what value of Martensite equal the current Hardening Value
    while m < 0.999
        
        Mar= ((1+(m^(P.NiTi.M.N1))-((1-m)^(P.NiTi.M.N2))));
        Mar = round(Mar, 3);
        
        if P.NiTi.M.DFDE == Mar
            P.NiTi.M.Mar_volInt(i) = m;
            m = 1;
        else
            m = m + 1e-4;
        end
        
    end
    
end

if ~isfield(P.NiTi.M, 'Mar_volInt')
    P.NiTi.M.Mar_volInt(i) = 0.999;
elseif length(P.NiTi.M.Mar_volInt)<i
    P.NiTi.M.Mar_volInt(i) = 0.999;
end

if P.NiTi.M.Mar_volInt(i) > 0.999
    P.NiTi.M.Mar_volInt(i) = 0.999 -BB;
elseif P.NiTi.M.Mar_volInt(i) <0.001
    P.NiTi.M.Mar_volInt(i) = 0.001+BB;
end

P.NiTi.M.DFDEInt(i) =   ( ( ( 1 - P.NiTi.M.D ) * P.NiTi.M.Hcur * P.NiTi.StressInt ) + (0.5 * P.NiTi.M.Delta_S *( P.NiTi.StressInt ^ 2 ) ) + ( P.NiTi.Dens * P.NiTi.M.Delta_entr *( (P.FDS.InitTemp+273.15) ) ) - P.NiTi.M.InEn - P.NiTi.M.Y0) ;
P.NiTi.M.DispT(i) =  P.NiTi.M.Hcur *P.NiTi.M.Mar_volInt(i);
P.NiTi.M.S = P.NiTi.M.SA1 + ( P.NiTi.M.Mar_volInt(i)*P.NiTi.M.Delta_S); %Material Stiffness
P.NiTi.M.Alpha = P.NiTi.M.ExpanA1 + ( P.NiTi.M.Mar_volInt(i)*(P.NiTi.M.ExpanM-P.NiTi.M.ExpanA1)); %Thermal Expansion Coefficient

P.NiTi.M.Disp(i) = ((P.NiTi.M.S *P.NiTi.StressInt)+ (-P.NiTi.M.Alpha*((P.FDS.InitTemp+273.15) - P.NiTi.M.A1F))+P.NiTi.M.DispT(i))*P.Stack.Height;
P.NiTi.M.StrainInt(i) = P.NiTi.M.Disp(i)/P.Stack.Height;


end

for i = 1:length(Stack)
varName = ['Int' num2str(i) ];
StackName = ['Stack_' num2str(i)];
XPlateName = ['X_Plate_' num2str(i)];

P.NiTi.(varName).ForceInt = Stack(i);

P.NiTi.(varName).DFDEInt = P.NiTi.M.DFDEInt(i);
P.NiTi.(varName).Disp = P.NiTi.M.Disp(i);
P.NiTi.(varName).Mar_volInt = P.NiTi.M.Mar_volInt(i);

% P.NiTi.(varName).ForceInt = 0;
% P.NiTi.(varName).DFDEInt = 0;
% P.NiTi.(varName).Disp = 0;
% P.NiTi.(varName).Mar_volInt = 0;

P.NiTi.(varName).DsigmaInt  =  0;
P.NiTi.(varName).DmarInt  =  0;
P.NiTi.(varName).QInt  =   0;
P.NiTi.(varName).VelInt  =  0;
P.NiTi.(varName).MassVelInt  =  0;
P.NiTi.(varName).MassForceInt  =  0;

P.Stack.(varName).XPlate.Mass.VelInt  =  0;
P.Stack.(varName).XPlate.Mass.ForceInt  =  0;
P.Stack.(varName).XPlate.Friction.VelInt  =  0;
P.Stack.(varName).XPlate.Friction.ForceInt  =  0;


end
P.Core.HydCyl.Int.Mass.VelInt  =  0;
P.Core.HydCyl.Int.Mass.ForceInt  =  0;
P.Core.HydCyl.Int.Friction.VelInt =  0;
P.Core.HydCyl.Int.Friction.ForceInt =  0;

%% Martensitic Table Data
P.NiTi.M.DFDE_For = zeros(1/1e-4,1);
P.NiTi.M.Marvol_For = zeros(1/1e-4,1);
P.NiTi.M.DFDE_Rev = zeros(1/1e-4,1);
P.NiTi.M.Marvol_Rev = zeros(1/1e-4,1);

n = 1;

for i = 0:1e-4:1
    
DFDE = 0.5*P.NiTi.M.A1 *(1+i^P.NiTi.M.N1 -(1-i)^P.NiTi.M.N2) + P.NiTi.M.A3;

P.NiTi.M.DFDE_For(n) = DFDE ;
P.NiTi.M.Marvol_For(n) = i;
n= n+1;

end

n = 1;
for i = 1:-1e-4:0
    
DFDE = 0.5*P.NiTi.M.A2 *(1+i^P.NiTi.M.N3 -(1-i)^P.NiTi.M.N4) - P.NiTi.M.A3;

P.NiTi.M.DFDE_Rev(n) = DFDE ;
P.NiTi.M.Marvol_Rev(n) = i;
n = n+1;

end