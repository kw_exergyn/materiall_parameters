% P.NiTi.M.ExpanA = 11E-6;  % 1/K Thermal Exapansion
% P.NiTi.M.ExpanM = 6.6E-6;  % 1/K Thermal Exapansion
% P.NiTi.M.DA = -P.NiTi.M.ExpanM - (-P.NiTi.M.ExpanA); % Thermal Expansion Difference
% P.NiTi.Dens_gmm3 = P.NiTi.Dens/1e9*1e3;
% % Tuning Parameters
% P.NiTi.T0 = 273.15;
% 
% % Setup Calcs
% P.NiTi.M.SA1 = 1/P.NiTi.M.Ea;  % Stress Compliance coefficient (Austenite)
% P.NiTi.M.SM = 1/P.NiTi.M.Em;  % Stress Compliance coefficient (Martensite)
% P.NiTi.M.Delta_S = P.NiTi.M.SM-P.NiTi.M.SA1;
% P.NiTi.pr = 0.3;
% 
% P.NiTi.M.Delta_entr = -(2*P.NiTi.M.CA1*P.NiTi.M.CM*(P.NiTi.M.Hcur + (P.NiTi.M.CalS*P.NiTi.M.Delta_S)))/(P.NiTi.Dens*(P.NiTi.M.CA1+P.NiTi.M.CM));
% P.NiTi.M.D =((P.NiTi.M.CM - P.NiTi.M.CA1)*(P.NiTi.M.Hcur + (P.NiTi.M.CalS*P.NiTi.M.Delta_S)))/((P.NiTi.M.CM + P.NiTi.M.CA1)*P.NiTi.M.Hcur);
% 
% P.NiTi.Pr = 0.33;   % Poisson Ratio
% P.NiTi.SurfaceRough = 0.1;
% 
% % PreCalculated channels
% %Limits of the Thermodynamic Boundary
% %Forward Transformation
% 
% % Martensite Volume =1
% P.NiTi.M.Marone =(1+(0.998^(P.NiTi.M.N1))-((1-0.998)^(P.NiTi.M.N2)));
% 
% % Martensite Volume =0
% P.NiTi.M.Marzero = (1+(0^(P.NiTi.M.N1))-((1-0)^(P.NiTi.M.N2)));
% 
% %Reverse Transformation
% 
% % Martensite Volume =1
% P.NiTi.M.MaroneR=(1+(0.998^(P.NiTi.M.N3))-((1-0.998)^(P.NiTi.M.N4)));
% 
% % Martensite Volume =0
% P.NiTi.M.MarzeroR=(1+(0^(P.NiTi.M.N3))-((1-0)^(P.NiTi.M.N4)));
% 
% 
% P.NiTi.M.Mdelta = (P.NiTi.M.MF-P.NiTi.M.MS);
% P.NiTi.M.Adelta = (P.NiTi.M.A1S-P.NiTi.M.A1F);
% 
% P.NiTi.M.DeltaEntDensity = P.NiTi.M.Delta_entr*P.NiTi.Dens;
% 
% P.NiTi.M.A1 = P.NiTi.M.DeltaEntDensity*P.NiTi.M.Mdelta;
% P.NiTi.M.A2 = P.NiTi.M.DeltaEntDensity*P.NiTi.M.Adelta;
% 
% P.NiTi.M.A3a=((-0.25*P.NiTi.M.A1)*(1+(1/(P.NiTi.M.N1+1))-(1/(P.NiTi.M.N2+1))));
% P.NiTi.M.A3b=((0.25*P.NiTi.M.A2)*(1+(1/(P.NiTi.M.N3+1))-(1/(P.NiTi.M.N4+1))));
% P.NiTi.M.A3=P.NiTi.M.A3a+P.NiTi.M.A3b;
% 
% P.NiTi.M.InEn = 0.5*P.NiTi.M.DeltaEntDensity*(P.NiTi.M.MS+P.NiTi.M.A1F) ; % Internal Energy
% P.NiTi.M.Y0 = (0.5*P.NiTi.M.DeltaEntDensity*((P.NiTi.M.MS -  P.NiTi.M.A1F))- P.NiTi.M.A3);
% P.NiTi.M.n=0.0001;
% 
P.NiTi.ForceInt = 0;
P.NiTi.Dens_gmm3    = P.NiTi.Dens/1e9*1e3; %  Material Density [g/mm^3]

P.Stack.Area        = 8.444672213352160*1000;
P.Stack.Height      = 100;
P.Stack.Volume      = P.Stack.Area *P.Stack.Height ;
P.Stack.AreaM2      = P.Stack.Area*1e-6;
P.Stack.CSA = P.Stack.AreaM2;
P.Stack.Mass        = P.Stack.Volume*P.NiTi.Dens_gmm3;
P.Stack.AreaRatio   = 1;
% Hydraulic Cylinder
P.Core.Cyl.A.CSA    = P.Stack.Area ;
P.Core.Cyl.Stroke   = 20;      % Piston Stroke [mm]
P.Core.Cyl.RodMass  = P.Stack.Mass; % Mass of the rod [kg]
P.Core.Cyl.InitPres = 1;    % intial Pressure [bar]

P.NiTi.ForceInt     = (P.Core.Cyl.InitPres*1e5 )*(P.Core.Cyl.A.CSA/1e6);

%% Martensite - Austenite
% Setup Calcs
P.NiTi.M.SA1        = 1/P.NiTi.M.EA1;  % Stress Compliance coefficient (Austenite 1)
P.NiTi.M.SM         = 1/P.NiTi.M.EM;  % Stress Compliance coefficient (Martensite)
P.NiTi.M.Delta_S    = P.NiTi.M.SM-P.NiTi.M.SA1;
P.NiTi.M.DA = P.NiTi.M.ExpanM - P.NiTi.M.ExpanA1;
P.NiTi.M.DC = P.NiTi.M.CPM - P.NiTi.M.CPA1;

P.NiTi.M.Delta_entr = -(2*P.NiTi.M.CA1*P.NiTi.M.CM*(P.NiTi.M.Hcur + (P.NiTi.M.CalS*P.NiTi.M.Delta_S)))/(P.NiTi.Dens*(P.NiTi.M.CA1+P.NiTi.M.CM));
P.NiTi.M.D          =((P.NiTi.M.CM - P.NiTi.M.CA1)*(P.NiTi.M.Hcur + (P.NiTi.M.CalS*P.NiTi.M.Delta_S)))/((P.NiTi.M.CM + P.NiTi.M.CA1)*P.NiTi.M.Hcur);


P.NiTi.M.Mdelta = (P.NiTi.M.MF-P.NiTi.M.MS);
P.NiTi.M.Adelta = (P.NiTi.M.A1S-P.NiTi.M.A1F);

P.NiTi.M.DeltaEntDensity = P.NiTi.M.Delta_entr*P.NiTi.Dens;

P.NiTi.M.A1 = P.NiTi.M.DeltaEntDensity*P.NiTi.M.Mdelta;
P.NiTi.M.A2 = P.NiTi.M.DeltaEntDensity*P.NiTi.M.Adelta;

P.NiTi.M.A3a=((-0.25*P.NiTi.M.A1)*(1+(1/(P.NiTi.M.N1+1))-(1/(P.NiTi.M.N2+1))));
P.NiTi.M.A3b=((0.25*P.NiTi.M.A2)*(1+(1/(P.NiTi.M.N3+1))-(1/(P.NiTi.M.N4+1))));
P.NiTi.M.A3=P.NiTi.M.A3a+P.NiTi.M.A3b;

P.NiTi.M.InEn = 0.5*P.NiTi.M.DeltaEntDensity*(P.NiTi.M.MS + P.NiTi.M.A1F) ; % Internal Energy
P.NiTi.M.Y0 = (0.5*P.NiTi.M.DeltaEntDensity*((P.NiTi.M.MS -  P.NiTi.M.A1F))- P.NiTi.M.A3);
P.NiTi.M.n=0.0001;

%Limits of the Thermodynamic Boundary
%Forward Transformation
% Martensite Volume =1
P.NiTi.M.Marone =(1+(0.998^(P.NiTi.M.N1))-((1-0.998)^(P.NiTi.M.N2)));
% Martensite Volume =0
P.NiTi.M.Marzero = (1+(0^(P.NiTi.M.N1))-((1-0)^(P.NiTi.M.N2)));
%Reverse Transformation
% Martensite Volume =1
P.NiTi.M.MaroneR=(1+(0.998^(P.NiTi.M.N3))-((1-0.998)^(P.NiTi.M.N4)));
% Martensite Volume =0
P.NiTi.M.MarzeroR=(1+(0^(P.NiTi.M.N3))-((1-0)^(P.NiTi.M.N4)));

% Martensitic Table Data
P.NiTi.M.DFDE_For   = zeros(1/1e-4,1);
P.NiTi.M.Marvol_For = zeros(1/1e-4,1);
P.NiTi.M.DFDE_Rev   = zeros(1/1e-4,1);
P.NiTi.M.Marvol_Rev = zeros(1/1e-4,1);

n = 1;
%Forward Transformation
for iMatInit = 0:1e-4:1
    
    DFDE = 0.5*P.NiTi.M.A1 *(1+iMatInit^P.NiTi.M.N1 -(1-iMatInit)^P.NiTi.M.N2) + P.NiTi.M.A3;
    
    P.NiTi.M.DFDE_For(n) = DFDE ;
    P.NiTi.M.Marvol_For(n) = iMatInit;
    n= n+1;
    
end

n = 1;
%Reverse Transformation
for iMatInit = 1:-1e-4:0
    
    DFDE = 0.5*P.NiTi.M.A2 *(1+iMatInit^P.NiTi.M.N3 -(1-iMatInit)^P.NiTi.M.N4) - P.NiTi.M.A3;
    
    P.NiTi.M.DFDE_Rev(n) = DFDE ;
    P.NiTi.M.Marvol_Rev(n) = iMatInit;
    n = n+1;
    
end
try
P.NiTi.M.ForOne  = interp1(P.NiTi.M.Marvol_For,P.NiTi.M.DFDE_For,1,'nearest');
P.NiTi.M.ForZero = interp1(P.NiTi.M.Marvol_For,P.NiTi.M.DFDE_For,0,'nearest');
P.NiTi.M.RevOne = interp1(P.NiTi.M.Marvol_Rev,P.NiTi.M.DFDE_Rev,1,'nearest');
P.NiTi.M.RevZero = interp1(P.NiTi.M.Marvol_Rev,P.NiTi.M.DFDE_Rev,0,'nearest');
catch
P.NiTi.M.ForOne  = 0;
P.NiTi.M.ForZero = 0;
P.NiTi.M.RevOne  = 0;
P.NiTi.M.RevZero = 0;
end




