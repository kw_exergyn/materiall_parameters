% This script allows the user build an ideal set of  materials that . The user provides the desired operating stress at
% each stage along with the stage DT. A DSC is run for each material

clear
close all
%% User inputs
RunCascade = 0; % Run the cascade Sweep
plotMat    = 1; % Plot the material parameters

P.Sim.logStep = 0.01;          % Timestep of the logger [s]
P.Sim.tStep = 0.01;            % Timestep of the simulation [s]

DeltaMsMf = 80;            % [K] Ms - Mf temperature Difference
DeltaAFMS = 15;            % [K] Af - Ms temperature Difference
DeltaMFAS = 15;            % [K] Ms - As temperature Diffence
Hcur      = 3;             % [%] Trasnsformation Strain
LHloss    = 0.4 ;           % [] latent heat loss after training
Af_Temps  = [283.15]; % [K] Af activation temperatures
DT        = [40 ];    %
OpStress  = [1000];
FluidOffset = 10;%K
% Common Material Parameters
P.NiTi.M.N1	      =	0.2;     % []
P.NiTi.M.N2	      =	0.2;     % []
P.NiTi.M.N3	      =	0.2;     % []
P.NiTi.M.N4	      =	0.2;     % []
P.NiTi.M.EA1	  =	65e9;    % [Pa]
P.NiTi.M.EM	      =	55e9;    % [Pa]
P.NiTi.M.y	      =	2.5;  % []
P.NiTi.M.C_	      =	0.3; % []
P.NiTi.M.CalS	  =	1;       % [Pa]
P.NiTi.M.ExpanA1  = 11E-6;   % 1/K Thermal Exapansion
P.NiTi.M.ExpanM   = 6.6E-6;  % 1/K Thermal Exapansion
P.NiTi.M.CPM      = 460;     % j/kg.K Specific heat
P.NiTi.M.CPA1     = 460;     % j/kg.K Specific heat
P.NiTi.M.kM       = 8.6;     % Thermal Conductivity in Austenite State [W/(m*K)]
P.NiTi.M.kA1      = 18;      % Thermal conductivity in Martensite State [W/(m*K)]
P.NiTi.Dens       = 6450;    % kg/m3 Material Density


Ms_Temps   = Af_Temps - DeltaAFMS;                % [K] Ms activation temperatures
M_PhaseAvg = (Ms_Temps + (Ms_Temps-DeltaMsMf))/2; % Average Martensitic Phase Temperature
LH_Poly    = [0.1567 , -23.33];                   % Polynominals to calculate latent heat at
% given temp. calculated from frenzel paper graph
LH        = polyval(LH_Poly, Ms_Temps);           % [kJ/kg] Latent at each Ms Temp
LH = LH- (LH * LHloss);

% Calculate the MPa/K for each material
MPa_K     = zeros(1,length(LH));
for i     = 1: length(LH)
    MPa_K (i) = (P.NiTi.Dens* (LH(i)*1e3)) /((Hcur/100) *M_PhaseAvg(i));
end

%% Create the individual material parameter structures

figure('Name', 'Isothermal Cycles','units','normalized','outerposition', [0,0.02,1,0.98],'DefaultAxesFontSize',12)
t = tiledlayout(1,length(LH));
t.TileSpacing = 'compact';
t.Padding = 'compact';

l=1;
for i = 1: length(LH)

    eval(['P.NiTi.M.MS	='	num2str(Ms_Temps(i))]);                       % [K]
    eval(['P.NiTi.M.MF	='	num2str(Ms_Temps(i)-DeltaMsMf)]);             % [K]
    eval(['P.NiTi.M.A1S	='	num2str(Ms_Temps(i)-DeltaMsMf + DeltaAFMS)]); % [K]
    eval(['P.NiTi.M.A1F	='	num2str(Af_Temps(i))]);                       % [K]
    eval(['P.NiTi.M.CA1	='	num2str(MPa_K(i))]);                          % [Pa/K]
    eval(['P.NiTi.M.CM	='	num2str(MPa_K(i))]);                          % [Pa/K]
    eval(['P.NiTi.M.Hcur  ='	num2str(Hcur/100)]);                          % [m/m]



    Material_Params_01

    eval(['P.NiTi', num2str(i) '= P.NiTi']);

    P.NiTi.StressMax = OpStress(i); % MPa

    a = nexttile;
    hold on
    grid on
    grid minor
    title(['Material ', num2str(i)])
    xlabel('Strain [m/m]')
    ylabel('Stress [MPa]')


    for j =[ P.NiTi.M.A1F + DT(i) ,P.NiTi.M.A1F + (DT(i)/2) P.NiTi.M.A1F]
        P.FDS.InitTemp = j -273.15-FluidOffset; % °C
        P.NiTi.T0 =P.FDS.InitTemp+273.15;

        [O]= MatInitial(P);
        out = sim("MaterialModelTest");
        idx = find(out.Strain.Time >10, 1 );
        idy =find(out.Strain.Time == 20, 1, 'last' );
        filter = (idx:idy);
        plot(out.Strain.Data(filter), -out.Force.Data(filter) / P.Stack.Area,'DisplayName',[num2str(P.FDS.InitTemp),'°C'])
    end
    text(min(out.Strain.Data(filter))/2,max(out.Force.Data(filter)/ P.Stack.Area), ['latent Heat = ', num2str(round(LH(l),1)) 'kJ/kg'])
    text(min(out.Strain.Data(filter))/2,max(out.Force.Data(filter)/ P.Stack.Area)-20, ['MPa/K = ', num2str(round(MPa_K(l)*1e-6,1))])
    l=l+1;
    legend('location', 'northeast')
end

%% DSC
figure('Name', 'DSC','units','normalized','outerposition', [0,0.02,1,0.98],'DefaultAxesFontSize',12)
t = tiledlayout(1,length(LH));
t.TileSpacing = 'compact';
t.Padding = 'compact';

l=1;
for i = 1: length(LH)

    eval(['P.NiTi.M.MS	='	num2str(Ms_Temps(i))]);                       % [K]
    eval(['P.NiTi.M.MF	='	num2str(Ms_Temps(i)-DeltaMsMf)]);             % [K]
    eval(['P.NiTi.M.A1S	='	num2str(Ms_Temps(i)-DeltaMsMf + DeltaAFMS)]); % [K]
    eval(['P.NiTi.M.A1F	='	num2str(Af_Temps(i))]);                       % [K]
    eval(['P.NiTi.M.CA1	='	num2str(MPa_K(i))]);                          % [Pa/K]
    eval(['P.NiTi.M.CM	='	num2str(MPa_K(i))]);                          % [Pa/K]
    eval(['P.NiTi.M.Hcur  ='	num2str(Hcur/100)]);                          % [m/m]



    Material_Params_01

    eval(['P.NiTi', num2str(i) '= P.NiTi']);

    P.NiTi.StressMax = 0; % MPa

    a = nexttile;
    hold on
    grid on
    grid minor
    title(['Material ', num2str(i)])
    xlabel('Temperature [°C]')
    ylabel('Thermal Power [kW/kg]')




    TempRate = 4 ;% K/min
    TempHigh = 40 + 273.15; % °C
    TempLow = -80 + 273.15;  % °C

    TempInc = TempLow: TempRate/60:TempHigh;
    TempDec = TempHigh:-TempRate/60:TempLow;
    Temp = [TempInc,TempDec];
    Tlength = length(Temp);
    Temp = repmat(Temp,1,3);
    TempTime = 0:1:length(Temp)-1;
    Input.Temp = timeseries(Temp,TempTime);
P.Sim.MaxTime = TempTime(end);
    P.NiTi.T0 =Input.Temp.Data(1);

    [O]= MatInitial(P);
P.Sim.logStep = 0.1;          % Timestep of the logger [s]
P.Sim.tStep = 0.1;            % Timestep of the simulation [s]
    out = sim("MaterialDSCModelTest");

    idx = find(out.Strain.Time >Tlength*2, 1 );
    idy = length(out.Strain.Time);
    filter = (idx:idy);
    plot(out.NiTi.Temp.Data(filter)-273.15, (out.NiTi.Qout.Data(filter)*1e-3)/(P.Stack.Mass*1e-3) )
ylim([-200,200])
    text(min(out.Strain.Data(filter))/2,max(out.Force.Data(filter)/ P.Stack.Area), ['latent Heat = ', num2str(round(LH(l),1)) 'kJ/kg'])
    text(min(out.Strain.Data(filter))/2,max(out.Force.Data(filter)/ P.Stack.Area)-20, ['MPa/K = ', num2str(round(MPa_K(l)*1e-6,1))])
    l=l+1;
    legend('location', 'northeast')
end