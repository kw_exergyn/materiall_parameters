component SmoothHardeningSMA_01
    % Smooth Hardening SMA Model
    % This Block represents the Superelastic behaviour of a
    % Shape Memory Alloy material using a Smooth Hardened
    % Model as describe in Shape Memory Alloy modelling and
    % Engineering Applications by Dimitris Lagoudas
    
    % Copyright 2016 The MathWorks, Inc
    % 2021 Exergyn ltd [Keith Warren]
    
    inputs
        TransDirc = {1, '1'}; % Trans:bottom
        DiffTime = {0.001,'1'}% Dt:bottom
        DTrDt ={ 0, '1' }; % DTdt:bottom
    end
    outputs
        K ={18, 'W/(m*K)'}% k:bottom
        MarSit ={0,'1'}% MarVol: top
        Strain = {0,'1'}% Strain: top
        DsEntOut = {0, 'W/(kg*K)'} % Entropy: top
        Stress = {0, 'N/m^2'} % Stress: top
        Mat_Temp = {293.15,'K'}% Temperature: top
    end
     
    
    nodes
        
        A = foundation.thermal.thermal; % A:top
        R = foundation.mechanical.translational.translational; % C:bottom
        C = foundation.mechanical.translational.translational; % R:top
    end
    
    parameters
        Ms      = {264,   'K'};       % Martensite Start
        As      = {217,   'K'};       % Austenite Start
        Mf      = {160,   'K'};       % Martensite Finish
        Af      = {290,   'K'};       % Austenite Finish
        T_0     = {290,   'K'};       % Reference Temperature
        ExpanA  = {22E-6,   '1/K'};   % Austenite Thermal Expansion
        ExpanM  = {22E-6,   '1/K'};   % Martensite Thermal Expansion
        cpA     = {400, 'J/(kg*K)'};  % Austenite Specific heat
        cpM     = {400, 'J/(kg*K)'};  % Martensite Specific heat
        EA      = {32.5E9,    'Pa'};  % Austenite Young's Modulus
        EM      = {23E9,    'Pa'};    % Martensite Young's Modulus
        Dens    = {6500,'kg/m^3'};    % Density
        KA      = {18,'W/(m*K)'};     % Austenite Thermal Conductivity
        KM      = {8.6,'W/(m*K)'};    % Martensite Thermal Conductivity
        CA      = {7.4e6, 'Pa/K'};    % Austenite Stress Coefficent
        CM      = {7.4e6, 'Pa/K'};    % Martensite Stress Coefficent
        Hcur    = {0.033,  'm/m'};    % Transformation Strain
        N1      = {0.17,     '1'};    % N1
        N2      = {0.27,     '1'};    % N2
        N3      = {0.25,     '1'};    % N3
        N4      = {0.35,     '1'};    % N4
        Cr      = {0.12,     '1'};    % C
        y       = {3,        '1'};    % y
        CalS    = {0,       'Pa'};    % Calibration Stress
        BB      = {1e-4,     '1'};    % Af/Mf tuning
        CSA     = {890e-6, 'm^2'};    % Plate CSA
        Plength = {183e-3,   'm'};    % Stack Length
        limUp   = {0.999,    '1'};    % Marvol Upper Limit
        limDw   = {0.001,    '1'};    % Marvol Lower Limit
        Martol  = {0.01,    '1'};     % Marvol Tolerance
        MaxTs   = {0.01,    '1'};     % Max Time Step
    end
    
    parameters (Size = variable)
        xf = {[1,2,3,4],'J/m^3'}; % Forward DFDE
        yf = {[1,2,3,4], '1'};    % Forward Martensitic Volume
        
        xr = {[1,2,3,4],'J/m^3'}; % Reverse DFDE
        yr = {[1,2,3,4], '1'};    % Reverse Martensitic Volume
        
    end
    variables (Access=public, Conversion=absolute)
        Temp = {303.15, 'K'}; % Temperature
        
    end
    
    variables
        Q    = {0, 'W'};        % Heat flow rate
        v    = {0, 'm/s'};      % Stack Velocity
        Disp = {0, 'm'  };      % Displacement
        f    = { 0, 'N' };      % Force
        Marvol = { 0, '1' };    % Martensitic Volume
        DFDE = { 0, 'J/m^3' }; % Hardening Function
        Dsigma ={ 0, 'Pa/s' };  % Stress Delta
        DTemp = {0, 'K/s'}; % Temperature difference
        Dmar = { 0, '1/s' };    % Martensitic Volume Delta
        Yhys = { 0, 'J/m^3' };    % Hystersis Energy
        DsEnt = {0, 'W/(kg*K)'} % Entropy: top
    end
    
    variables (Access = private)
        
        %         State = { 0, '1' };
        %         nth = { 1, '1' };
        MarvolR = {0, '1' };    % Martensitic Reversal Volume
        
        Y_state = {1,'1'};
        
    end
    variables (Event = true, Access = private)
        
        k = { 0, '1' };          % Minor loop active
        Yn = { 0, 'J/m^3' };
        Dirc = {1,'1'};          % Transformation Direction
        DFDE_For = { 0, 'J/m^3' };
        DFDE_Rev = { 0, 'J/m^3' };
        Cn = {value ={ 0.12,'1'}};
        ntho = { 1, '1' };
    end
    
    
    branches
        Q : A.Q -> *;
        f : R.f -> C.f;
    end
    
    intermediates
        SA      = 1/EA;                                            % Austenite Stress Coefficent
        SM      = 1/EM;                                            % Martensite Stress Coefficent
        DA      = ExpanM - ExpanA;                                 % Thermal Expansion Change
        DC      = cpM - cpA;                                       % Specific Heat Change
        DS      = SM - SA;                                         % Stress Coefficient Change
        DK      = KM - KA;                                           % Thermal Conductivity Change
        Dent    = -(2*CA*CM*(Hcur+(CalS*DS)))/(Dens * (CA+CM));    % Entropy Change
        D       = ((CM - CA)*(Hcur + (CalS*DS)))/((CM + CA)*Hcur); % Transformation Slope Correction
        S       = SA+(MarSit*DS);                                  % Stress Compliance
        Alpha   = ExpanA + (MarSit*DA);                            % Thermal Expansion Coefficient
        
        
        Smass   = CSA * Plength *Dens;                             % Stack mass
        E       = EA + MarSit*(EM-EA);                             % Material Stiffness
        Sigma   = (f/CSA);                                         % Material Stress
        Cp      = cpA + (MarSit*DC);                               % Specific Heat Capacity
        
        A1      = Dens*Dent*((Mf-Ms));
        A2      = Dens*Dent*((As-Af))
        A3a     = ((0.25 * A1 ) * ( 1 + ( 1 / ( N1 + 1 ) ) - ( 1 / ( N2 + 1 ) ) ) );
        A3b     = ((0.25 * A2 ) * ( 1 + ( 1 / ( N3 + 1 ) ) - ( 1 / ( N4 + 1 ) ) ) );
        A3      = -A3a+A3b
        Yo      = 0.5*Dens*Dent*(Ms - Af)-A3                         % Hystersis Disappation Variable
        Uo      = 0.5*Dens*Dent*(Ms + Af);                           % Internal energy
        
        I =limUp;
        O =limDw;
        IN1 =((I^(1/N1))/((I+BB)^((1/N1)-1)))^N1;
        IN2 =(((1-I)^(1/N2))/((1-I+BB)^((1/N2)-1)))^N2;
        
        ON1 =((O^(1/N1))/((O+BB)^((1/N1)-1)))^N1;
        ON2 =(((1-O)^(1/N2))/((1-O+BB)^((1/N2)-1)))^N2;
        
        IN3 =((I^(1/N3))/((I+BB)^((1/N3)-1)))^N3;
        IN4 =(((1-I)^(1/N4))/((1-I+BB)^((1/N4)-1)))^N4;
        
        ON3 =((O^(1/N3))/((O+BB)^((1/N3)-1)))^N3;
        ON4 =(((1-O)^(1/N4))/((1-O+BB)^((1/N4)-1)))^N4;
        
        %         ForOne  =  0.5*A1 *(1+IN1 -IN2) +A3 ;           % Forward Transformation limit (ξ=1)
        % %         ForOneTol = ForOne *Martol;
        %
        %         ForZero =  0.5*A1 *(1+ON1 -ON2) +A3;            % Forward Transformation limit (ξ=0)
        % %         ForZeroTol = ForZero *Martol;
        %
        %         RevOne  = 0.5*A2*(1+IN3-IN4)-A3 ;               % Reverse Transformation limit (ξ=1)
        % %         RevOneTol = RevOne *Martol;
        %
        %         RevZero = 0.5*A2*(1+ON3-ON4)-A3 ;               % Reverse Transformation limit (ξ=0)
        %         RevZeroTol = RevZero *Martol;
        
        ForOne  =  tablelookup(yf,xf,1, interpolation = linear, extrapolation = nearest) ;           % Forward Transformation limit (ξ=1)
        
        ForZero =  tablelookup(yf,xf,0, interpolation = linear, extrapolation = nearest);            % Forward Transformation limit (ξ=0)
        
        RevOne  = tablelookup(yr,xr,1, interpolation = linear, extrapolation = nearest);            % Reverse Transformation limit (ξ=1)
        
        RevZero = tablelookup(yr,xr,0, interpolation = linear, extrapolation = nearest);            % Reverse Transformation limit (ξ=0)
        
        
        
        DispT = Hcur*MarSit;
        
        % minor Loop
        
        MarR =(MarSit - MarvolR) ;
        nth =  min(-1e-6,-(y/Cn)*abs(MarR));
        
        
        
    end
    
    equations
        assert(Plength > 0 , 'Stack length must be greater than zero');
        assert(Hcur > 0 , 'Hcur must be greater than zero');
        assert(Dens > 0 , 'Density must be greater than zero');
        assert(Ms > 0 , 'Ms must be greater than zero');
        assert(Mf > 0 , 'Mf must be greater than zero');
        assert(As > 0 , 'As must be greater than zero');
        assert(Af > 0 , 'Af must be greater than zero');
        
        K == KA +(MarSit*DK) %Thermal Conductivity Change
        MarSit == Marvol;
        Strain == Disp/Plength;
        DsEntOut == DsEnt;
        Stress == Sigma
        Mat_Temp == Temp;
        
        if k == 1
            
            Yhys == max([0, Yo *(1- exp(nth))])
            
            if abs(DTrDt)> 0
                
                MarvolR == Marvol;
            else
                if time <= {0.1,'s'}
                    MarvolR ==tablelookup(xf,yf,DFDE, interpolation = linear, extrapolation = nearest);;
                else
                    MarvolR == (((delay(MarvolR,{DiffTime,'s'},MaximumDelay = {MaxTs,'s'}))));
                end
            end
            Y_state == 2;
            
        else
            
            Yhys == max([0,0.5*Dens*Dent*(Ms - Af)-A3] );
            MarvolR == Marvol;
            Y_state ==1
            
        end
        if Dirc == 1
            
            Temp == A.T;
            v == R.v - C.v;
            Disp/Plength == Sigma*S + Alpha*(Temp - T_0) + (sign(Sigma)*DispT)
            %             *sign(f)
            v == der(Disp) % Velocity
            
            DFDE == (abs(Sigma)*Hcur*(1-D))+(0.5*DS*(Sigma)^2)+((Sigma)*DA*(Temp - T_0))+ (Dens*Dent*Temp)-Uo-Yhys; % Thermodynamic Force
            
            
            if DFDE > DFDE_For && DFDE > ForZero && DFDE < ForOne && abs(DTrDt) <= 0
                Marvol == tablelookup(xf,yf,DFDE, interpolation = linear, extrapolation = nearest);
                
            else
                if time <= {0.1,'s'}
                    %                     Marvol ==MarSit;
                    Marvol == tablelookup(xf,yf,DFDE, interpolation = linear, extrapolation = nearest);
                else
                    Marvol == delay(MarSit,{DiffTime,'s'},MaximumDelay = {MaxTs,'s'});
                end
            end
            
            
            Dmar == der(MarSit);
            Dsigma == der((Sigma ));
            DTemp == Temp.der;
            
            Q == ( ( ( Temp * Alpha * (Dsigma) ) + (Dens * Cp *DTemp ) + ( -Yhys + ( Temp * DA * (Sigma) )  + ( Dens * Dent * Temp ) )* Dmar ) /(  Dens ) )* Smass;
            
            %             DsEnt == (Yhys*Dmar - (Q/Smass)*Dens)/(Dens*Temp)
            DsEnt == (((1/Dens)*( Alpha * (Dsigma))) + ((Cp/Temp)*DTemp) + (((1/Dens)*DA*(Sigma)) + Dent)*Dmar) ;
        else
            
            Temp == A.T;
            v == R.v - C.v;
            Disp/Plength ==(Sigma)*S + Alpha*(Temp - T_0) + (sign(Sigma)*DispT)
            %             *sign(f)
            v == der(Disp) % Velocity
            -DFDE == -((1+D)*abs(Sigma)*Hcur)-(0.5*DS*(Sigma)^2)-((Sigma)*DA*(Temp - T_0))- (Dens*Dent*Temp)+Uo-Yhys; % Thermodynamic Force
            
            
            if DFDE < DFDE_Rev && DFDE > RevZero && DFDE < RevOne  && abs(DTrDt) <= 0
                
                Marvol == tablelookup(xr,yr,DFDE, interpolation = linear, extrapolation = nearest);
                
            else
                if time <= {0.1,'s'}
                    %                     Marvol ==MarSit;
                    Marvol == tablelookup(xf,yf,DFDE, interpolation = linear, extrapolation = nearest);
                else
                    Marvol == delay(MarSit,{DiffTime,'s'},MaximumDelay = {MaxTs,'s'});
                end
                
            end
            
            Dmar == der(MarSit);
            Dsigma == der((Sigma ));
            DTemp == Temp.der;
            
            Q == ( ( ( Temp * Alpha * (Dsigma) ) + (Dens * Cp *DTemp ) +( Yhys + ( Temp * DA * (Sigma) )  + ( Dens * Dent * Temp ) )* Dmar ) /(  Dens ) )* Smass;
            
            DsEnt == (((1/Dens)*( Alpha * (Dsigma))) + ((Cp/Temp)*DTemp) + (((1/Dens)*DA*(Sigma)) + Dent)*Dmar) ;
        end
        
    end
    events
        when initialevent
            
            k = 0;
            Cn = 0.12;
            ntho = -(y/Cn)*(1.5);
            Yn = max([0,Yhys / (1-exp(ntho))]);
            
        elsewhen edge (abs(DTrDt) > 0 && MarSit > limDw+BB && MarSit < limUp-BB && TransDirc == -1)
            k = 1;
            Cn = min([Cr,abs(Marvol),max({1,'1'}-abs(Marvol),0.01)]);
            ntho = -(y/Cn)*(1.5);
            Yn = max([0,Yhys / (1-exp(ntho))]);
            
            
        elsewhen edge (abs(DTrDt) > 0 && MarSit > limDw+BB && MarSit < limUp-BB && TransDirc == 1)
            k = 1;
            Cn = min([Cr,abs(Marvol),max(({1,'1'}-abs(Marvol)),0.01)]);
            ntho = -(y/Cn)*(1.5);
            Yn = max([0,Yhys / (1-exp(ntho))]);
            
        elsewhen  edge (MarSit < limDw+BB || MarSit > limUp-BB)
            k = 0;
            Cn = min([Cr,abs(Marvol),max({1,'1'}-abs(Marvol),0.01)]);
            ntho = -(y/Cn)*1.5;
            Yn = max([0,Yo / (1-exp(ntho))]);
        end
        
        when initialevent
            
            Dirc = 1;
            
        elsewhen edge(TransDirc == 1 )
            Dirc = 1;
            %              DFDE_For = 0.5*A1 *(1+((min(max(Marvol,0),1))+BB)^N1 -(1-(min(max(Marvol,0),1))+BB)^N2) + A3;
            DFDE_For = tablelookup(yf,xf,Marvol, interpolation = linear, extrapolation = nearest);
            
        elsewhen edge(TransDirc == -1 )
            Dirc =0;
            %              DFDE_Rev = 0.5*A2 *(1+((min(max(Marvol,0),1))-BB)^N3 -(1-(min(max(Marvol,0),1))-BB)^N4) - A3;
            DFDE_Rev = tablelookup(yr,xr,Marvol, interpolation = linear, extrapolation = nearest);
        end
        
    end
    
end