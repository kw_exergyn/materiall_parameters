component variable_pressure_setting_3port
% Variable Pressure Reducing/Relieving 3-Way Valve : 2.5
% The block simulates a pressure-reducing 3-way valve also referred to as 
% a pressure reducing/relieving valve. The valve reduces inlet pressure to 
% a preset value similar to a conventional pressure-reducing valve. 
% In addition, valve provides a relieving function if pressure at the 
% outlet builds up above preset value. At this pressure, the valve diverts 
% flow from the outlet to a tank. No inertia, friction, or hydraulic forces 
% are accounted in the model.
% 
% Connections P, A, and T are conserving hydraulic ports associated with 
% the valve inlet, outlet, and return, respectively. The block positive 
% direction is from port P to port A.
%
% S is a physical signal port through which an instantaneous value of 
% Set Pressure at port A should be provided.

% Copyright 2020 Greg Pittam.

inputs
    p_set = {100e5, 'Pa'}; % S:right
    
end

nodes
    P = foundation.hydraulic.hydraulic; % P:left
    A = foundation.hydraulic.hydraulic; % A:right
    T = foundation.hydraulic.hydraulic; % T:left
end

parameters
    C_d         = {0.7,   '1'  }; % Flow discharge coefficient
    leak_area   = {1e-12, 'm^2'}; % Minimum area
    max_area    = {1e-4,  'm^2'}; % Maximum area
    B_lam       = {0.999, '1'  }; % Laminar flow pressure ratio
    p_reg       = {2,     'bar'}; % Valve Regulation Range
end

variables
    q_pa    =   {1e-3,  'm^3/s' };  % Flow rate A->B
    p_pa    =   {0,     'Pa'    };  % Pressure differential AB
    q_at    =   {1e-3,  'm^3/s' };  % Flow rate B->T
    p_at    =   {0,     'Pa'    };  % Pressure differential BT
end

parameters (Access=private)
    p_atm         = {101325, 'Pa' }; % Atmospheric pressure
    leak_area_used = if leak_area == 0, {1e-15, 'm^2'} else leak_area end; % Minimum area
end

branches
    q_pa : P.q -> A.q;
    q_at : A.q -> T.q;
end

equations
    % Assertions
    assert(C_d > 0)
    assert(leak_area ~= 0, message('physmod:simscape:library:hydraulic:ZeroLeakageArea', 'Minimum Area'), Warn=true)
    assert(leak_area_used > 0)
    assert(B_lam > 0)
    assert(B_lam < 1)
    
    let
        k = (max_area-leak_area)/p_reg;
        
        % Regulation Side
        AR_pa = max_area - k*(A.p-(p_set-p_reg))
        area_pa = if gt(AR_pa, leak_area_used), AR_pa else leak_area_used end;
        p_cr_pa = (P.p + A.p + 2*p_atm)/2 * (1 - B_lam)% Pressure drop at transition from laminar to turbulent regime
        
        % Relief Side
        AR_at = leak_area + k*(A.p-p_set)
        area_at = if gt(AR_at, leak_area_used), AR_at else leak_area_used end;
        p_cr_at = (A.p + T.p + 2*p_atm)/2 * (1 - B_lam) % Pressure drop at transition from laminar to turbulent regime
    in
        p_pa == P.p - A.p;
        p_at == A.p - T.p;
        q_pa == C_d*area_pa*sqrt(2.0/P.density) * p_pa/(p_pa^2 + p_cr_pa^2)^(1/4);
        q_at == C_d*area_at*sqrt(2.0/P.density) * p_at/(p_at^2 + p_cr_at^2)^(1/4);
    end
end

end