component pipe_double_walled
% Double Walled Pipe (TL) : 2.5
% This block models pipe flow dynamics in a thermal liquid network due to
% viscous friction losses and convective heat transfer between the liquid
% and the pipe walls. The effects of dynamic compressibility and fluid
% inertia can be optionally included.
%
% The pipe contains a constant volume of liquid. Temperature evolves based
% on the thermal capacity of this liquid volume. Setting Fluid dynamic
% compressibility to On also causes pressure to evolve based on the dynamic
% compressibility of the liquid volume. Setting Fluid inertia to On causes
% the liquid to resist acceleration.
%
% Ports A and B are the thermal liquid conserving ports associated with the
% pipe inlet and outlet. Ports H1 and H2 are the thermal conserving port 
% associated with the pipe walls.
%
% This block can be used for example to model a pipe with a heating element
% inside. Where H1 may be the external wall of the pipe and H2 the surface 
% of the heating element.

% Copyright 2012-2019   The MathWorks, Inc.
%           2020        GECO Ltd

nodes
    H1 = foundation.thermal.thermal;                % H1:left
    A = foundation.thermal_liquid.thermal_liquid;   % A:left
    H2 = foundation.thermal.thermal;                % H2:right
    B = foundation.thermal_liquid.thermal_liquid;   % B:right
end

parameters
    length       = {5,              'm'}; % Pipe length
    zGain        = {0,              'm'}; % Elevation Gain from Port A to Port B
    area         = {0.01,           'm^2'}; % Cross-sectional area
    Dh           = {0.051169,       'm'}; % Hydraulic diameter
    P1           = {0.4712,         'm'}; % Perimeter of Surface 1
    P2           = {0.3105,         'm'}; % Perimeter of Surface 2
    length_add   = {1,              'm'}; % Aggregate equivalent length of local resistances
    roughness    = {15e-6,          'm'}; % Internal surface absolute roughness
    Re_lam       = 2000;            % Laminar flow upper Reynolds number limit
    Re_tur       = 4000;            % Turbulent flow lower Reynolds number limit
    shape_factor = 64;              % Laminar friction constant for Darcy friction factor
    Nu_lam       = 3.66;            % Nusselt number for laminar flow heat transfer
    dynamic_compressibility = simscape.enum.onoff.on; % Fluid dynamic compressibility
    %                                                   1 - on
    %                                                   0 - off
end
parameters (ExternalAccess = none)
    inertia = simscape.enum.onoff.off; % Fluid inertia
    %                                    1 - on
    %                                    0 - off
    p0 = {0.101325, 'MPa'};            % Initial liquid pressure
end
parameters
    T0 = {293.15, 'K'};                % Initial liquid temperature
end
parameters (ExternalAccess = none)
    mdot0 = {0, 'kg/s'};               % Initial mass flow rate from port A to port B
end

parameters (Access = private)
    surface_area1 = P1*length;    % Pipe surface area 1
    surface_area2 = P2*length;    % Pipe surface area 2
    volume        = area * length;       % Pipe volume
    g             = {9.80665, 'm/s^2'};      % Earths Gravity
end

% Parameter checks and visibility
equations
    assert(length > 0)
    assert(area > 0)
    assert(P1 > 0)
    assert(P2 > 0)
    assert(Dh > 0)
    assert(length_add >= 0)
    assert(roughness > 0)
    assert(Re_lam > 1)
    assert(Re_tur > Re_lam)
    assert(shape_factor > 0)
    assert(Nu_lam > 0)
    assert(T0 >= A.T_min)
    assert(T0 <= A.T_max)
end
if dynamic_compressibility == simscape.enum.onoff.on
    annotations
        [inertia, p0] : ExternalAccess = modify;
    end
    parameters (Access = private)
        p_priority = priority.high;
    end
    equations
        assert(p0 >= A.p_min)
        assert(p0 <= A.p_max)
    end
    if inertia == simscape.enum.onoff.on
        annotations
            mdot0 : ExternalAccess = modify;
        end
        parameters (Access = private)
            mdot_priority = priority.high;
        end
    else % inertia == simscape.enum.onoff.off
        parameters (Access = private)
            mdot_priority = priority.none;
        end
    end
else % dynamic_compressibility == simscape.enum.onoff.off
    parameters (Access = private)
        p_priority    = priority.none;
        mdot_priority = priority.none;
    end
end

variables (Access = protected)
    % Through variables
    mdot_A = {value =  mdot0, priority = mdot_priority}; % Mass flow rate into port A
    mdot_B = {value = -mdot0, priority = mdot_priority}; % Mass flow rate into port B
    Phi_A  = {0, 'kW'}; % Energy flow rate into port A
    Phi_B  = {0, 'kW'}; % Energy flow rate into port B
    Q_H1   = {0, 'kW'}; % Heat flow rate into port H1
    Q_H2   = {0, 'kW'}; % Heat flow rate into port H2

    % Internal node
    p_I = {value = p0, priority = p_priority   }; % Pressure of liquid volume
    T_I = {value = T0, priority = priority.high}; % Temperature of liquid volume
end

branches
    mdot_A : A.mdot -> *;
    mdot_B : B.mdot -> *;
    Phi_A  : A.Phi  -> *;
    Phi_B  : B.Phi  -> *;
    Q_H1   : H1.Q   -> *;
    Q_H2   : H2.Q   -> *;
end

intermediates (Access = private, ExternalAccess = none)
    % Liquid properties table lookup
    cp_I    = tablelookup(A.T_TLU, A.p_TLU, A.cp_TLU,    T_I, p_I, interpolation = linear, extrapolation = linear);
    mu_I    = tablelookup(A.T_TLU, A.p_TLU, A.mu_TLU,    T_I, p_I, interpolation = linear, extrapolation = nearest);
    k_I     = tablelookup(A.T_TLU, A.p_TLU, A.k_TLU,     T_I, p_I, interpolation = linear, extrapolation = nearest);
    Pr_I    = tablelookup(A.T_TLU, A.p_TLU, A.Pr_TLU,    T_I, p_I, interpolation = linear, extrapolation = nearest);
    beta_I  = tablelookup(A.T_TLU, A.p_TLU, A.beta_TLU,  T_I, p_I, interpolation = linear, extrapolation = linear);
    alpha_I = tablelookup(A.T_TLU, A.p_TLU, A.alpha_TLU, T_I, p_I, interpolation = linear, extrapolation = linear);

    % Liquid properties for inflow
    mu_A_in = tablelookup(A.T_TLU, A.p_TLU, A.mu_TLU, A.T, p_I, interpolation = linear, extrapolation = nearest);
    mu_B_in = tablelookup(A.T_TLU, A.p_TLU, A.mu_TLU, B.T, p_I, interpolation = linear, extrapolation = nearest);
    k_A_in  = tablelookup(A.T_TLU, A.p_TLU, A.k_TLU,  A.T, p_I, interpolation = linear, extrapolation = nearest);
    k_B_in  = tablelookup(A.T_TLU, A.p_TLU, A.k_TLU,  B.T, p_I, interpolation = linear, extrapolation = nearest);
    Pr_A_in = tablelookup(A.T_TLU, A.p_TLU, A.Pr_TLU, A.T, p_I, interpolation = linear, extrapolation = nearest);
    Pr_B_in = tablelookup(A.T_TLU, A.p_TLU, A.Pr_TLU, B.T, p_I, interpolation = linear, extrapolation = nearest);

    % Average mass flow rate from port A to port B
    mdot_avg = (mdot_A - mdot_B)/2;
    Re_avg = (mdot_avg * Dh) / (area * mu_I);

    %% Heat Transfer to Part 1
    % Convective heat transfer between pipe wall and moist air
    Q_AB1 = foundation.thermal_liquid.elements.pipe_convection(mdot_avg, A.T, H1.T, ...
        (mu_A_in + mu_I)/2, (k_A_in + k_I)/2, (Pr_A_in + Pr_I)/2, ...
        area, Dh, surface_area1, roughness/Dh, Re_lam, Re_tur, Nu_lam);

    Q_BA1 = foundation.thermal_liquid.elements.pipe_convection(-mdot_avg, B.T, H1.T, ...
        (mu_B_in + mu_I)/2, (k_B_in + k_I)/2, (Pr_B_in + Pr_I)/2, ...
        area, Dh, surface_area1, roughness/Dh, Re_lam, Re_tur, Nu_lam);

    Q_conv1 = simscape.function.blend(Q_BA1, Q_AB1, -Re_lam, Re_lam, Re_avg);

    % Conductive heat transfer between pipe wall and moist air
    Q_cond1 = k_I * surface_area1 / Dh * (H1.T - T_I);
    
    %% Heat Transfer to Part 2
    % Convective heat transfer between pipe wall and moist air
    Q_AB2 = foundation.thermal_liquid.elements.pipe_convection(mdot_avg, A.T, H2.T, ...
        (mu_A_in + mu_I)/2, (k_A_in + k_I)/2, (Pr_A_in + Pr_I)/2, ...
        area, Dh, surface_area2, roughness/Dh, Re_lam, Re_tur, Nu_lam);

    Q_BA2 = foundation.thermal_liquid.elements.pipe_convection(-mdot_avg, B.T, H2.T, ...
        (mu_B_in + mu_I)/2, (k_B_in + k_I)/2, (Pr_B_in + Pr_I)/2, ...
        area, Dh, surface_area2, roughness/Dh, Re_lam, Re_tur, Nu_lam);

    Q_conv2 = simscape.function.blend(Q_BA2, Q_AB2, -Re_lam, Re_lam, Re_avg);

    % Conductive heat transfer between pipe wall and moist air
    Q_cond2 = k_I * surface_area2 / Dh * (H2.T - T_I);
    
end

% For logging
intermediates (Access = private)
    rho_I = tablelookup(A.T_TLU, A.p_TLU, A.rho_TLU, T_I, p_I, interpolation = linear, extrapolation = linear); % Density of liquid volume
    u_I   = tablelookup(A.T_TLU, A.p_TLU, A.u_TLU,   T_I, p_I, interpolation = linear, extrapolation = linear); % Specific internal energy of liquid volume
    h_I   = u_I + p_I/rho_I; % Specific enthalpy of liquid volume

    pressure_loss_AI = foundation.thermal_liquid.elements.pipe_friction(mdot_A, rho_I, mu_I, ...
        area, Dh, (length+length_add)/2, roughness/Dh, Re_lam, Re_tur, shape_factor); % Frictional pressure loss at port A

    pressure_loss_BI = foundation.thermal_liquid.elements.pipe_friction(mdot_B, rho_I, mu_I, ...
        area, Dh, (length+length_add)/2, roughness/Dh, Re_lam, Re_tur, shape_factor); % Frictional pressure loss at port B
end

if dynamic_compressibility == simscape.enum.onoff.off

    intermediates (Access = private, ExternalAccess = none)
        % Specific heat at constant volume
        cv_I = cp_I - beta_I * alpha_I^2 * T_I / rho_I;
    end

    equations
        % Mass balance
        mdot_A + mdot_B == 0;

        % Energy conservation
        der(T_I) * cv_I * rho_I * volume == Phi_A + Phi_B + Q_H1 + Q_H2;

        % Momentum balance
        A.p - B.p == pressure_loss_AI - pressure_loss_BI + rho_I*g*zGain;
        p_I == (A.p + B.p)/2;
    end

else % dynamic_compressibility == simscape.enum.onoff.on

    intermediates (Access = private, ExternalAccess = none)
        % Partial derivatives of internal energy of liquid volume with respect to pressure and temperature
        dUdp = (rho_I * h_I / beta_I - T_I * alpha_I) * volume;
        dUdT = (cp_I - h_I * alpha_I) * rho_I * volume;
    end

    equations
        % Mass conservation
        (der(p_I)/beta_I - der(T_I)*alpha_I) * rho_I * volume == mdot_A + mdot_B;

        % Energy conservation
        der(p_I)*dUdp + der(T_I)*dUdT == Phi_A + Phi_B + Q_H1 + Q_H2;
    end

    if inertia == simscape.enum.onoff.off

        equations
            % Momentum balance
            A.p - p_I == pressure_loss_AI + rho_I*g*zGain/2;
            B.p - p_I == pressure_loss_BI - rho_I*g*zGain/2;
        end

    else % inertia == simscape.enum.onoff.on

        % For logging
        intermediates (Access = private)
            inertia_AI = A.p - p_I - pressure_loss_AI; % Fluid inertia at port A
            inertia_BI = B.p - p_I - pressure_loss_BI; % Fluid inertia at port B
        end

        equations
            % Momentum balance
            A.p - p_I == der(mdot_A)*length/2/area + pressure_loss_AI + rho_I*g*zGain/2;
            B.p - p_I == der(mdot_B)*length/2/area + pressure_loss_BI - rho_I*g*zGain/2;
        end

    end
end

equations
    % Heat transfer
    Q_H1 == Q_conv1 + Q_cond1;
    Q_H2 == Q_conv2 + Q_cond2;

    let
        % Indicator variables for the valid region of the property tables
        [indicator_pT_A, indicator_pT_B, indicator_pT_I] = ...
            if A.pT_region_flag == foundation.enum.pT_region_TL.validity_matrix, ...
                tablelookup(A.T_TLU, A.p_TLU, A.pT_validity_TLU, A.T, A.p, interpolation = linear, extrapolation = linear); ...
                tablelookup(A.T_TLU, A.p_TLU, A.pT_validity_TLU, B.T, B.p, interpolation = linear, extrapolation = linear); ...
                tablelookup(A.T_TLU, A.p_TLU, A.pT_validity_TLU, T_I, p_I, interpolation = linear, extrapolation = linear) ...
            else ...
                1; ...
                1; ...
                1 ...
            end;
    in
        % Pressure and temperature must be within the valid region
        assert(indicator_pT_A > 0, message('physmod:simscape:library:thermal_liquid:PressureTemperaturePortValidRegion', 'A'))
        assert(A.p >= A.p_min, message('physmod:simscape:library:thermal_liquid:PressureMinValid', 'A'))
        assert(A.p <= A.p_max, message('physmod:simscape:library:thermal_liquid:PressureMaxValid', 'A'))
        assert(A.T >= A.T_min, message('physmod:simscape:library:thermal_liquid:TemperatureMinValid', 'A'))
        assert(A.T <= A.T_max, message('physmod:simscape:library:thermal_liquid:TemperatureMaxValid', 'A'))
        assert(indicator_pT_B > 0, message('physmod:simscape:library:thermal_liquid:PressureTemperaturePortValidRegion', 'B'))
        assert(B.p >= A.p_min, message('physmod:simscape:library:thermal_liquid:PressureMinValid', 'B'))
        assert(B.p <= A.p_max, message('physmod:simscape:library:thermal_liquid:PressureMaxValid', 'B'))
        assert(B.T >= A.T_min, message('physmod:simscape:library:thermal_liquid:TemperatureMinValid', 'B'))
        assert(B.T <= A.T_max, message('physmod:simscape:library:thermal_liquid:TemperatureMaxValid', 'B'))
        assert(indicator_pT_I > 0, message('physmod:simscape:library:thermal_liquid:PressureTemperatureVolumeValidRegion'))
        assert(p_I >= A.p_min)
        assert(p_I <= A.p_max)
        assert(T_I >= A.T_min)
        assert(T_I <= A.T_max)
    end
end

% Internal components that calculate energy convection at ports A and B
components (ExternalAccess = none)
    convection_A = foundation.thermal_liquid.port_convection(flow_area = area, length_scale = length/2);
    convection_B = foundation.thermal_liquid.port_convection(flow_area = area, length_scale = length/2);
end
connections
    connect(A, convection_A.port)
    connect(B, convection_B.port)
end

% Equate variables for internal components that calculate energy convection at ports A and B
equations
    convection_A.mdot == mdot_A;
    convection_A.Phi  == Phi_A;
    convection_A.u_I  == u_I;
    convection_B.mdot == mdot_B;
    convection_B.Phi  == Phi_B;
    convection_B.u_I  == u_I;
end

end
