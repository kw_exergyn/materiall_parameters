component stack_channel
% stack channel (TL) : 6.0
% This block models the NiTiNol stacks channel flow dynamics in a thermal liquid network due to
% viscous friction losses and convective heat transfer between the liquid
% and the pipe walls. The effects of dynamic compressibility and fluid
% inertia can be optionally included. The addition of the strain input 
% allows the model to adjust depending on the load.
%
% The pipe contains a variable volume of liquid. Temperature evolves based
% on the thermal capacity of this liquid volume. Setting Fluid dynamic
% compressibility to On also causes pressure to evolve based on the dynamic
% compressibility of the liquid volume. Setting Fluid inertia to On causes
% the liquid to resist acceleration.
%
% Ports A and B are the thermal liquid conserving ports associated with the
% pipe inlet and outlet. Port H is the thermal conserving port associated
% with the pipe wall.
%
% The inputs strain is the strain that the stack is experiencing. The
% strain is used directly to reduce the length of the channel as it is
% compressed. The strain is multiplied by the provided Poisson ratio and 
% then used to increase the CSA, hydraulic diameter and surface area.
%
% Copyright 2012-2019   The MathWorks, Inc.
%           2020        GECO Ltd

nodes
    A = foundation.thermal_liquid.thermal_liquid;   % A:left
    H = foundation.thermal.thermal;                 % H:left
    B = foundation.thermal_liquid.thermal_liquid;   % B:right
end

inputs (ExternalAccess = modify)
    strain      = 0;     % Strain:right
end

parameters
    length       = {1,      'm'};   % Nominal Stack Height
    area         = {0.01,   'm^2'}; % Nominal Cross-sectional area of fluid channel
    Dh           = {2,      'cm'};  % Nominal Hydraulic Diameter
    zGain        = {0,      'm'};   % Elevation Gain from Port A to Port B
    length_add   = {1,      'm'};   % Aggregate equivalent length of local resistances
    roughness    = {15e-6,  'm'};   % Internal surface absolute roughness
    pr           = 0.33;            % Poisson's Ratio of material
    Re_lam       = 2000;            % Laminar flow upper Reynolds number limit
    Re_tur       = 4000;            % Turbulent flow lower Reynolds number limit
    shape_factor = 64;              % Laminar friction constant for Darcy friction factor
    Nu_lam       = 3.66;            % Nusselt number for laminar flow heat transfer
    dynamic_compressibility = simscape.enum.onoff.on; % Fluid dynamic compressibility
    %                                                   1 - on
    %                                                   0 - off
end
parameters (ExternalAccess = none)
    inertia = simscape.enum.onoff.off; % Fluid inertia
    %                                    1 - on
    %                                    0 - off
    p0          = {0.101325,    'MPa'}; % Initial liquid pressure
end
parameters
    T0 = {293.15, 'K'};                % Initial liquid temperature
end
parameters (ExternalAccess = none)
    mdot0 = {0, 'kg/s'};               % Initial mass flow rate from port A to port B
end

parameters (Access = private)
    g             = {9.80665, 'm/s^2'};      % Earths Gravity
end

% Parameter checks and visibility
equations
    assert(pr >= 0)
    assert(pr <= 1)
    assert(length > 0)
    assert(area > 0)
    assert(Dh > 0)
    assert(length_add >= 0)
    assert(roughness > 0)
    assert(Re_lam > 1)
    assert(Re_tur > Re_lam)
    assert(shape_factor > 0)
    assert(Nu_lam > 0)
    assert(T0 >= A.T_min)
    assert(T0 <= A.T_max)
end
if dynamic_compressibility == simscape.enum.onoff.on
    annotations
        [inertia, p0] : ExternalAccess = modify;
    end
    parameters (Access = private)
        p_priority = priority.high;
    end
    equations
        assert(p0 >= A.p_min)
        assert(p0 <= A.p_max)
    end
    if inertia == simscape.enum.onoff.on
        annotations
            mdot0 : ExternalAccess = modify;
        end
        parameters (Access = private)
            mdot_priority = priority.high;
        end
    else % inertia == simscape.enum.onoff.off
        parameters (Access = private)
            mdot_priority = priority.none;
        end
    end
else % dynamic_compressibility == simscape.enum.onoff.off
    parameters (Access = private)
        p_priority    = priority.none;
        mdot_priority = priority.none;
    end
end

variables (Access = protected)
    % Through variables
    mdot_A = {value =  mdot0, priority = mdot_priority}; % Mass flow rate into port A
    mdot_B = {value = -mdot0, priority = mdot_priority}; % Mass flow rate into port B
    Phi_A  = {0, 'kW'}; % Energy flow rate into port A
    Phi_B  = {0, 'kW'}; % Energy flow rate into port B
    Q_H   = {0, 'kW'}; % Heat flow rate into port H

    % Internal node
    p_I = {value = p0, priority = p_priority   }; % Pressure of liquid volume
    T_I = {value = T0, priority = priority.high}; % Temperature of liquid volume
end

branches
    mdot_A : A.mdot -> *;
    mdot_B : B.mdot -> *;
    Phi_A  : A.Phi  -> *;
    Phi_B  : B.Phi  -> *;
    Q_H   : H.Q   -> *;
end

intermediates (Access = private, ExternalAccess = none)
    % Liquid properties table lookup
    cp_I    = tablelookup(A.T_TLU, A.p_TLU, A.cp_TLU,    T_I, p_I, interpolation = linear, extrapolation = linear);
    mu_I    = tablelookup(A.T_TLU, A.p_TLU, A.mu_TLU,    T_I, p_I, interpolation = linear, extrapolation = nearest);
    k_I     = tablelookup(A.T_TLU, A.p_TLU, A.k_TLU,     T_I, p_I, interpolation = linear, extrapolation = nearest);
    Pr_I    = tablelookup(A.T_TLU, A.p_TLU, A.Pr_TLU,    T_I, p_I, interpolation = linear, extrapolation = nearest);
    beta_I  = tablelookup(A.T_TLU, A.p_TLU, A.beta_TLU,  T_I, p_I, interpolation = linear, extrapolation = linear);
    alpha_I = tablelookup(A.T_TLU, A.p_TLU, A.alpha_TLU, T_I, p_I, interpolation = linear, extrapolation = linear);

    % Liquid properties for inflow
    mu_A_in = tablelookup(A.T_TLU, A.p_TLU, A.mu_TLU, A.T, p_I, interpolation = linear, extrapolation = nearest);
    mu_B_in = tablelookup(A.T_TLU, A.p_TLU, A.mu_TLU, B.T, p_I, interpolation = linear, extrapolation = nearest);
    k_A_in  = tablelookup(A.T_TLU, A.p_TLU, A.k_TLU,  A.T, p_I, interpolation = linear, extrapolation = nearest);
    k_B_in  = tablelookup(A.T_TLU, A.p_TLU, A.k_TLU,  B.T, p_I, interpolation = linear, extrapolation = nearest);
    Pr_A_in = tablelookup(A.T_TLU, A.p_TLU, A.Pr_TLU, A.T, p_I, interpolation = linear, extrapolation = nearest);
    Pr_B_in = tablelookup(A.T_TLU, A.p_TLU, A.Pr_TLU, B.T, p_I, interpolation = linear, extrapolation = nearest);

    % Average mass flow rate from port A to port B
    mdot_avg        = (mdot_A - mdot_B)/2;
    length_st       = length*(1-strain);            % Reduction in length due to loading
    strain_pr       = strain*pr;                    % Strain seen transverse to the load being applied due to the Poisson effect
    area_pr         = area*(1+strain_pr)^2;         % Increase in area due to the strain
    Dh_pr           = Dh*(1+strain_pr);             % Change in hydraulic diameter due to the strain
    surface_area = (4*area_pr/Dh_pr)*length_st;     % Pipe surface area adjusted for the loading
    volume        = area_pr * length_st;            % Pipe volume adjusted for the loading
    Re_avg          = (mdot_avg * Dh_pr) / (area_pr * mu_I);

    % Convective heat transfer between pipe wall and liquid
    Q_AB = foundation.thermal_liquid.elements.pipe_convection(mdot_avg, A.T, H.T, ...
        (mu_A_in + mu_I)/2, (k_A_in + k_I)/2, (Pr_A_in + Pr_I)/2, ...
        area_pr, Dh_pr, surface_area, roughness/Dh_pr, Re_lam, Re_tur, Nu_lam);

    Q_BA = foundation.thermal_liquid.elements.pipe_convection(-mdot_avg, B.T, H.T, ...
        (mu_B_in + mu_I)/2, (k_B_in + k_I)/2, (Pr_B_in + Pr_I)/2, ...
        area_pr, Dh_pr, surface_area, roughness/Dh_pr, Re_lam, Re_tur, Nu_lam);

    Q_conv = simscape.function.blend(Q_BA, Q_AB, -Re_lam, Re_lam, Re_avg);

    % Conductive heat transfer between pipe wall and liquid
    Q_cond = k_I * surface_area / Dh_pr * (H.T - T_I);
end

% For logging
intermediates (Access = private)
    rho_I = tablelookup(A.T_TLU, A.p_TLU, A.rho_TLU, T_I, p_I, interpolation = linear, extrapolation = linear); % Density of liquid volume
    u_I   = tablelookup(A.T_TLU, A.p_TLU, A.u_TLU,   T_I, p_I, interpolation = linear, extrapolation = linear); % Specific internal energy of liquid volume
    h_I   = u_I + p_I/rho_I; % Specific enthalpy of liquid volume

    pressure_loss_AI = foundation.thermal_liquid.elements.pipe_friction(mdot_A, rho_I, mu_I, ...
        area_pr, Dh_pr, (length_st+length_add)/2, roughness/Dh_pr, Re_lam, Re_tur, shape_factor); % Frictional pressure loss at port A

    pressure_loss_BI = foundation.thermal_liquid.elements.pipe_friction(mdot_B, rho_I, mu_I, ...
        area_pr, Dh_pr, (length_st+length_add)/2, roughness/Dh_pr, Re_lam, Re_tur, shape_factor); % Frictional pressure loss at port B
end

if dynamic_compressibility == simscape.enum.onoff.off

    intermediates (Access = private, ExternalAccess = none)
        % Specific heat at constant volume
        cv_I = cp_I - beta_I * alpha_I^2 * T_I / rho_I;
    end

    equations
        % Mass balance
        mdot_A + mdot_B == 0;

        % Energy conservation
        der(T_I) * cv_I * rho_I * volume == Phi_A + Phi_B + Q_H;

        % Momentum balance
        A.p - B.p == pressure_loss_AI - pressure_loss_BI + rho_I*g*zGain;
        p_I == (A.p + B.p)/2;
    end

else % dynamic_compressibility == simscape.enum.onoff.on

    intermediates (Access = private, ExternalAccess = none)
        % Partial derivatives of internal energy of liquid volume with respect to pressure and temperature
        dUdp = (rho_I * h_I / beta_I - T_I * alpha_I) * volume;
        dUdT = (cp_I - h_I * alpha_I) * rho_I * volume;
    end

    equations
        % Mass conservation
        (der(p_I)/beta_I - der(T_I)*alpha_I) * rho_I * volume == mdot_A + mdot_B;

        % Energy conservation
        der(p_I)*dUdp + der(T_I)*dUdT == Phi_A + Phi_B + Q_H;
    end

    if inertia == simscape.enum.onoff.off

        equations
            % Momentum balance
            A.p - p_I == pressure_loss_AI + rho_I*g*zGain/2;
            B.p - p_I == pressure_loss_BI - rho_I*g*zGain/2;
        end

    else % inertia == simscape.enum.onoff.on

        % For logging
        intermediates (Access = private)
            inertia_AI = A.p - p_I - pressure_loss_AI; % Fluid inertia at port A
            inertia_BI = B.p - p_I - pressure_loss_BI; % Fluid inertia at port B
        end

        equations
            % Momentum balance
            A.p - p_I == der(mdot_A)*length_st/2/area_pr + pressure_loss_AI + rho_I*g*zGain/2;
            B.p - p_I == der(mdot_B)*length_st/2/area_pr + pressure_loss_BI - rho_I*g*zGain/2;
        end

    end
end

equations
    % Heat transfer
    Q_H == Q_conv + Q_cond;

    let
        % Indicator variables for the valid region of the property tables
        [indicator_pT_A, indicator_pT_B, indicator_pT_I] = ...
            if A.pT_region_flag == foundation.enum.pT_region_TL.validity_matrix, ...
                tablelookup(A.T_TLU, A.p_TLU, A.pT_validity_TLU, A.T, A.p, interpolation = linear, extrapolation = linear); ...
                tablelookup(A.T_TLU, A.p_TLU, A.pT_validity_TLU, B.T, B.p, interpolation = linear, extrapolation = linear); ...
                tablelookup(A.T_TLU, A.p_TLU, A.pT_validity_TLU, T_I, p_I, interpolation = linear, extrapolation = linear) ...
            else ...
                1; ...
                1; ...
                1 ...
            end;
    in
        % Pressure and temperature must be within the valid region
        assert(indicator_pT_A > 0, message('physmod:simscape:library:thermal_liquid:PressureTemperaturePortValidRegion', 'A'))
        assert(A.p >= A.p_min, message('physmod:simscape:library:thermal_liquid:PressureMinValid', 'A'))
        assert(A.p <= A.p_max, message('physmod:simscape:library:thermal_liquid:PressureMaxValid', 'A'))
        assert(A.T >= A.T_min, message('physmod:simscape:library:thermal_liquid:TemperatureMinValid', 'A'))
        assert(A.T <= A.T_max, message('physmod:simscape:library:thermal_liquid:TemperatureMaxValid', 'A'))
        assert(indicator_pT_B > 0, message('physmod:simscape:library:thermal_liquid:PressureTemperaturePortValidRegion', 'B'))
        assert(B.p >= A.p_min, message('physmod:simscape:library:thermal_liquid:PressureMinValid', 'B'))
        assert(B.p <= A.p_max, message('physmod:simscape:library:thermal_liquid:PressureMaxValid', 'B'))
        assert(B.T >= A.T_min, message('physmod:simscape:library:thermal_liquid:TemperatureMinValid', 'B'))
        assert(B.T <= A.T_max, message('physmod:simscape:library:thermal_liquid:TemperatureMaxValid', 'B'))
        assert(indicator_pT_I > 0, message('physmod:simscape:library:thermal_liquid:PressureTemperatureVolumeValidRegion'))
        assert(p_I >= A.p_min)
        assert(p_I <= A.p_max)
        assert(T_I >= A.T_min)
        assert(T_I <= A.T_max)
    end
end

% Internal components that calculate energy convection at ports A and B
components (ExternalAccess = none)
    convection_A = foundation.thermal_liquid.port_convection(flow_area = area, length_scale = length/2);
    convection_B = foundation.thermal_liquid.port_convection(flow_area = area, length_scale = length/2);
end
connections
    connect(A, convection_A.port)
    connect(B, convection_B.port)
end

% Equate variables for internal components that calculate energy convection at ports A and B
equations
    convection_A.mdot == mdot_A;
    convection_A.Phi  == Phi_A;
    convection_A.u_I  == u_I;
    convection_B.mdot == mdot_B;
    convection_B.Phi  == Phi_B;
    convection_B.u_I  == u_I;
end

end
