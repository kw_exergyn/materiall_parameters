component variable_temp_reservoir
% Reservoir (TL)
% This block sets constant boundary conditions in a thermal liquid network.
% The volume of liquid inside the reservoir is assumed infinite. Therefore,
% the flow is assumed quasi-steady. Liquid leaves the reservoir at the
% reservoir pressure and temperature. Liquid enters the reservoir at the
% reservoir pressure, but its temperature is determined by the thermal
% liquid network upstream.

% Copyright 2012-2019 The MathWorks, Inc.

nodes
    A = foundation.thermal_liquid.thermal_liquid; % A:top
end
inputs
    reservoir_temperature = {293.15,   'K'  }; % RT:bottom
end

parameters
    pressure_spec = foundation.enum.pressure_spec.atmospheric; % Reservoir pressure specification
    %                                                            1 - atmospheric
    %                                                            2 - specified
end
parameters (ExternalAccess = none)
    reservoir_pressure    = {0.101325, 'MPa'}; % Reservoir pressure
end
parameters
    area                  = {0.01,     'm^2'}; % Cross-sectional area at port A
end

% Parameter checks and visibility
equations
    assert(reservoir_temperature >= A.T_min)
    assert(reservoir_temperature <= A.T_max)
    assert(area > 0)
end
if pressure_spec == foundation.enum.pressure_spec.atmospheric
    parameters (Access = private)
        p_reservoir = A.p_atm;
    end
else % pressure_spec == foundation.enum.pressure_spec.specified
    annotations
        reservoir_pressure : ExternalAccess = modify;
    end
    parameters (Access = private)
        p_reservoir = reservoir_pressure;
    end
    equations
        assert(reservoir_pressure >= A.p_min)
        assert(reservoir_pressure <= A.p_max)
    end
end

variables (Access = protected)
    % Through variables
    mdot_A = {0, 'kg/s'}; % Mass flow rate into port A
    Phi_A  = {0, 'kW'  }; % Energy flow rate into port A

    % Internal node
    p_I = {0.1, 'MPa'}; % Reservoir pressure
    T_I = {300, 'K'  }; % Reservoir temperature
end

branches
    mdot_A : A.mdot -> *;
    Phi_A  : A.Phi  -> *;
end

% For logging
intermediates (Access = private)
    rho_I = tablelookup(A.T_TLU, A.p_TLU, A.rho_TLU, T_I, p_I, interpolation = linear, extrapolation = linear); % Reservoir density
    u_I   = tablelookup(A.T_TLU, A.p_TLU, A.u_TLU,   T_I, p_I, interpolation = linear, extrapolation = linear); % Reservoir specific internal energy
    h_I   = u_I + p_I/rho_I; % Reservoir specific enthalpy
end

equations
    % Assume no flow resistance
    A.p == p_I;

    % Reservoir pressure and temperature
    p_I == p_reservoir;
    T_I == reservoir_temperature;
end

% Internal component that calculates energy convection at port A
components (ExternalAccess = none)
    convection_A = foundation.thermal_liquid.port_convection(flow_area = area, length_scale = sqrt(4*area/pi));
end
connections
    connect(A, convection_A.port)
end

% Equate variables for internal component that calculates energy convection at port A
equations
    convection_A.mdot == mdot_A;
    convection_A.Phi  == Phi_A;
    convection_A.u_I  == u_I;
end

end