
%% General Parameters

P.NiTi.T0           = 273.15+P.InitTemp;              % Reference Temperautre [K]
P.NiTi.pr           = 0.3;
P.NiTi.Pr           = 0.33;                % Poisson Ratio
P.NiTi.SurfaceRough = 0.1;                 % Surface Roughness
P.NiTi.ForceInt     = 0;                   % Initial Force [N]
P.NiTi.Dens         = 6450;                %  Material Density [kg/m^3]
P.NiTi.Dens_gmm3    = P.NiTi.Dens/1e9*1e3; %  Material Density [g/mm^3]
P.NiTi.LwLim        = 0.002;               % Lower Phase Limit
P.NiTi.UpLim        = 0.998;               % Upper Phase Limit
P.NiTi.RPhase       = 0;                   % Material Has R-Phase

%% NiTi Parameters
P.NiTi.M.Hcur	 =	0.03; % [m/m]
P.NiTi.M.MS	     =	268.15; % [K]
P.NiTi.M.MF	     =	188.15; % [K]
P.NiTi.M.A1S	 =	203.15; % [K]
P.NiTi.M.A1F	 =	283.15; % [K]
P.NiTi.M.N1	     =	0.2; % []
P.NiTi.M.N2	     =	0.2; % []
P.NiTi.M.N3	     =	0.2; % []
P.NiTi.M.N4	     =	0.2; % []
P.NiTi.M.CA1	 =	10.6e6; % [Pa/K]
P.NiTi.M.CM  	 =	10.6e6; % [Pa/K]
P.NiTi.M.EA1 	 =	65e9; % [Pa]
P.NiTi.M.EM	     =	55e9; % [Pa]
P.NiTi.M.y  	 =	2.5; % []
P.NiTi.M.C_ 	 =	0.3; % []
P.NiTi.M.CalS	 =	1; % [Pa]
P.NiTi.M.CPA1     = 460; %[J/kg.K]
P.NiTi.M.CPM      = 460; %[J/kg.K]


P.NiTi.R.CPR = P.NiTi.M.CPA1;
P.NiTi.R.CPA2 = P.NiTi.M.CPA1;
% end

if ~isfield(P.NiTi.M, 'Hcur')
P.NiTi.M.Hcur	 =	0.024483; % [m/m]
end
if ~isfield(P.NiTi.M, 'MS')
P.NiTi.M.MS	     =	223; % [K]
P.NiTi.M.MF	     =	123; % [K]
end
if ~isfield(P.NiTi.M, 'A1S')
P.NiTi.M.A1S	 =	170; % [K]
P.NiTi.M.A1F	 =	293; % [K]
end

if ~isfield(P.NiTi.M, 'N1')
P.NiTi.M.N1	     =	0.4; % []
P.NiTi.M.N2	     =	0.18993; % []
P.NiTi.M.N3	     =	0.63408; % []
P.NiTi.M.N4	     =	0.23816; % []
end

if ~isfield(P.NiTi.M, 'CA1')
P.NiTi.M.CA1	 =	10e6; % [Pa/K]
end

if ~isfield(P.NiTi.M, 'CM')
P.NiTi.M.CM  	 =	10e6; % [Pa/K]
end

if ~isfield(P.NiTi.M, 'EA1')
P.NiTi.M.EA1 	 =	55e9; % [Pa]
end

if ~isfield(P.NiTi.M, 'EM')
P.NiTi.M.EM	     =	45e9; % [Pa]
end

if ~isfield(P.NiTi.M, 'y')
P.NiTi.M.y  	 =	2.5699; % []
end

if ~isfield(P.NiTi.M, 'C_')
P.NiTi.M.C_ 	 =	0.44116; % []
end

if ~isfield(P.NiTi.M, 'CalS')
P.NiTi.M.CalS	 =	1; % [Pa]
end




P.NiTi.M.ExpanA1    = 11E-6;  % 1/K Thermal Exapansion
P.NiTi.M.ExpanM     = 6.6E-6;  % 1/K Thermal Exapansion


P.NiTi.M.kM  = 8.6;          % Thermal Conductivity in Martensite State [W/(m*K)]
P.NiTi.M.kA1 = 18;         % Thermal conductivity in Austentite 1 State [W/(m*K)]


% PreCalculated channels
%% Martensite - Austenite
% Setup Calcs
P.NiTi.M.SA1        = 1/P.NiTi.M.EA1;  % Stress Compliance coefficient (Austenite 1)
P.NiTi.M.SM         = 1/P.NiTi.M.EM;  % Stress Compliance coefficient (Martensite)
P.NiTi.M.Delta_S    = P.NiTi.M.SM-P.NiTi.M.SA1;
P.NiTi.M.DA = P.NiTi.M.ExpanM - P.NiTi.M.ExpanA1;
P.NiTi.M.DC = P.NiTi.M.CPM - P.NiTi.M.CPA1;

P.NiTi.M.Delta_entr = -(2*P.NiTi.M.CA1*P.NiTi.M.CM*(P.NiTi.M.Hcur + (P.NiTi.M.CalS*P.NiTi.M.Delta_S)))/(P.NiTi.Dens*(P.NiTi.M.CA1+P.NiTi.M.CM));
P.NiTi.M.D          =((P.NiTi.M.CM - P.NiTi.M.CA1)*(P.NiTi.M.Hcur + (P.NiTi.M.CalS*P.NiTi.M.Delta_S)))/((P.NiTi.M.CM + P.NiTi.M.CA1)*P.NiTi.M.Hcur);


P.NiTi.M.Mdelta = (P.NiTi.M.MF-P.NiTi.M.MS);
P.NiTi.M.Adelta = (P.NiTi.M.A1S-P.NiTi.M.A1F);

P.NiTi.M.DeltaEntDensity = P.NiTi.M.Delta_entr*P.NiTi.Dens;

P.NiTi.M.A1 = P.NiTi.M.DeltaEntDensity*P.NiTi.M.Mdelta;
P.NiTi.M.A2 = P.NiTi.M.DeltaEntDensity*P.NiTi.M.Adelta;

P.NiTi.M.A3a=((-0.25*P.NiTi.M.A1)*(1+(1/(P.NiTi.M.N1+1))-(1/(P.NiTi.M.N2+1))));
P.NiTi.M.A3b=((0.25*P.NiTi.M.A2)*(1+(1/(P.NiTi.M.N3+1))-(1/(P.NiTi.M.N4+1))));
P.NiTi.M.A3=P.NiTi.M.A3a+P.NiTi.M.A3b;

P.NiTi.M.InEn = 0.5*P.NiTi.M.DeltaEntDensity*(P.NiTi.M.MS + P.NiTi.M.A1F) ; % Internal Energy
P.NiTi.M.Y0 = (0.5*P.NiTi.M.DeltaEntDensity*((P.NiTi.M.MS -  P.NiTi.M.A1F))- P.NiTi.M.A3);
P.NiTi.M.n=0.0001;

%Limits of the Thermodynamic Boundary
%Forward Transformation
% Martensite Volume =1
P.NiTi.M.Marone =(1+(0.998^(P.NiTi.M.N1))-((1-0.998)^(P.NiTi.M.N2)));
% Martensite Volume =0
P.NiTi.M.Marzero = (1+(0^(P.NiTi.M.N1))-((1-0)^(P.NiTi.M.N2)));
%Reverse Transformation
% Martensite Volume =1
P.NiTi.M.MaroneR=(1+(0.998^(P.NiTi.M.N3))-((1-0.998)^(P.NiTi.M.N4)));
% Martensite Volume =0
P.NiTi.M.MarzeroR=(1+(0^(P.NiTi.M.N3))-((1-0)^(P.NiTi.M.N4)));

% Martensitic Table Data
P.NiTi.M.DFDE_For   = zeros(1/1e-4,1);
P.NiTi.M.Marvol_For = zeros(1/1e-4,1);
P.NiTi.M.DFDE_Rev   = zeros(1/1e-4,1);
P.NiTi.M.Marvol_Rev = zeros(1/1e-4,1);

n = 1;
%Forward Transformation
for iMatInit = 0:1e-4:1
    
    DFDE = 0.5*P.NiTi.M.A1 *(1+iMatInit^P.NiTi.M.N1 -(1-iMatInit)^P.NiTi.M.N2) + P.NiTi.M.A3;
    
    P.NiTi.M.DFDE_For(n) = DFDE ;
    P.NiTi.M.Marvol_For(n) = iMatInit;
    n= n+1;
    
end

n = 1;
%Reverse Transformation
for iMatInit = 1:-1e-4:0
    
    DFDE = 0.5*P.NiTi.M.A2 *(1+iMatInit^P.NiTi.M.N3 -(1-iMatInit)^P.NiTi.M.N4) - P.NiTi.M.A3;
    
    P.NiTi.M.DFDE_Rev(n) = DFDE ;
    P.NiTi.M.Marvol_Rev(n) = iMatInit;
    n = n+1;
    
end
try
P.NiTi.M.ForOne  = interp1(P.NiTi.M.Marvol_For,P.NiTi.M.DFDE_For,1,'nearest');
P.NiTi.M.ForZero = interp1(P.NiTi.M.Marvol_For,P.NiTi.M.DFDE_For,0,'nearest');
P.NiTi.M.RevOne = interp1(P.NiTi.M.Marvol_Rev,P.NiTi.M.DFDE_Rev,1,'nearest');
P.NiTi.M.RevZero = interp1(P.NiTi.M.Marvol_Rev,P.NiTi.M.DFDE_Rev,0,'nearest');
catch
P.NiTi.M.ForOne  = 0;
P.NiTi.M.ForZero = 0;
P.NiTi.M.RevOne  = 0;
P.NiTi.M.RevZero = 0;
end




%% Initial Values
[O]= MatInitial(P);



