function NiTi = DA_NiTi_Param_01(tOffset)
%% General Parameters
NiTi.Offset       = tOffset;
NiTi.T0           = 273.15 - 10;              % Reference Temperautre [K]
NiTi.Pr           = 0.45;                % Poisson Ratio
NiTi.SurfaceRough = 0.1;                 % Surface Roughness
NiTi.Dens         = 6450;                %  Material Density [kg/m^3]
NiTi.Dens_gmm3    = NiTi.Dens/1e9*1e3; %  Material Density [g/mm^3]
NiTi.LwLim        = 0.002;               % Lower Phase Limit
NiTi.UpLim        = 0.998;               % Upper Phase Limit
NiTi.RPhase       = 1;                   % Material Has R-Phase

%% NiTi Parameters
NiTi.M.CalS	    =	1; % [Pa]
NiTi.M.CPA1     = 460; %[J/kg.K]
NiTi.M.CPM      = 460; %[J/kg.K]

NiTi.M.Hcur	=	0.028625; % [m/m]
NiTi.M.MS	=	320.2662 +NiTi.Offset; % [K]
NiTi.M.MF	=	201.4263+NiTi.Offset; % [K]
NiTi.M.A1S	=	198.0107+NiTi.Offset; % [K]
NiTi.M.A1F	=	334.7257+NiTi.Offset; % [K]

NiTi.M.N1	=	0.32372; % []
NiTi.M.N2	=	0.25021; % []
NiTi.M.N3	=	0.26926; % []
NiTi.M.N4	=	0.17377; % []

NiTi.M.CA1	=	12038593.7707; % [Pa/K]
NiTi.M.EA1	=	41843485430.8893; % [Pa]
NiTi.M.EM	=	61541040619.4657; % [Pa]
NiTi.M.y	=	11.8455; % []
NiTi.M.C_	=	0.28011; % []

NiTi.R.Hcur	=	0.018589; % [m/m]
NiTi.R.RS	=	288.8366+NiTi.Offset; % [K]
NiTi.R.RF	=	189.073+NiTi.Offset; % [K]
NiTi.R.A2S	=	221.7316+NiTi.Offset; % [K]
NiTi.R.A2F	=	364.4776+NiTi.Offset; % [K]

NiTi.R.N1	=	0.13637; % []
NiTi.R.N2	=	0.2643; % []
NiTi.R.N3	=	0.33781; % []
NiTi.R.N4	=	0.28929; % []

NiTi.R.CA2	=	28783182.6325; % [Pa/K]
NiTi.R.EA2	=	64088113011.6215; % [Pa]
NiTi.R.y	=	22.3013; % []
NiTi.R.C_	=	0.59797; % []

NiTi.R.ER = NiTi.M.EA1;
NiTi.M.CM = NiTi.M.CA1;
NiTi.R.CR = NiTi.R.CA2;


NiTi.M.CPM = 460;
NiTi.M.CPA1 = 460;
NiTi.R.CPR = NiTi.M.CPA1;
NiTi.R.CPA2 = NiTi.M.CPA1;
NiTi.Cp       = (NiTi.M.CPA1+NiTi.M.CPM)/2; %[J/kg.K]
% end

if ~isfield(NiTi.M, 'Hcur')
    NiTi.M.Hcur	 =	0.024483; % [m/m]
end
if ~isfield(NiTi.M, 'MS')
    NiTi.M.MS	     =	223; % [K]
    NiTi.M.MF	     =	123; % [K]
end
if ~isfield(NiTi.M, 'A1S')
    NiTi.M.A1S	 =	170; % [K]
    NiTi.M.A1F	 =	293; % [K]
end

if ~isfield(NiTi.M, 'N1')
    NiTi.M.N1	     =	0.4; % []
    NiTi.M.N2	     =	0.18993; % []
    NiTi.M.N3	     =	0.63408; % []
    NiTi.M.N4	     =	0.23816; % []
end

if ~isfield(NiTi.M, 'CA1')
    NiTi.M.CA1	 =	10e6; % [Pa/K]
end

if ~isfield(NiTi.M, 'CM')
    NiTi.M.CM  	 =	10e6; % [Pa/K]
end

if ~isfield(NiTi.M, 'EA1')
    NiTi.M.EA1 	 =	55e9; % [Pa]
end

if ~isfield(NiTi.M, 'EM')
    NiTi.M.EM	     =	45e9; % [Pa]
end

if ~isfield(NiTi.M, 'y')
    NiTi.M.y  	 =	2.5699; % []
end

if ~isfield(NiTi.M, 'C_')
    NiTi.M.C_ 	 =	0.44116; % []
end

if ~isfield(NiTi.M, 'CalS')
    NiTi.M.CalS	 =	1; % [Pa]
end

%R-Phase
if ~isfield(NiTi.R, 'Hcur')
    NiTi.R.Hcur	 =	0.001; % [m/m]
end
if ~isfield(NiTi.R, 'RS')
    NiTi.R.RS	     =	300; % [K]
    NiTi.R.RF	     =	250; % [K]
end
if ~isfield(NiTi.R, 'A2S')
    NiTi.R.A2S	 =	273; % [K]
    NiTi.R.A2F	 =	308; % [K]
end
if ~isfield(NiTi.R, 'N1')
    NiTi.R.N1	     =	0.4; % []
    NiTi.R.N2	     =	0.18993; % []
    NiTi.R.N3	     =	0.63408; % []
    NiTi.R.N4	     =	0.23816; % []
end
if ~isfield(NiTi.R, 'CA2')
    NiTi.R.CA2	 =	20e6; % [Pa/K]
end
if ~isfield(NiTi.R, 'CR')
    NiTi.R.CR  	 =	20e6; % [Pa/K]
end
if ~isfield(NiTi.R, 'EA2')
    NiTi.R.EA2 	 =	60e9; % [Pa]
end
if ~isfield(NiTi.R, 'ER')
    NiTi.R.ER	     =	NiTi.M.EA1; % [Pa]
end
if ~isfield(NiTi.R, 'y')
    NiTi.R.y  	 =	2.5699; % []
end
if ~isfield(NiTi.R, 'C_')
    NiTi.R.C_ 	 =	0.44116; % []
end
if ~isfield(NiTi.R, 'CalS')
    NiTi.R.CalS	 =	1; % [Pa]
end

NiTi.R.ExpanA2    = 11E-6;  % 1/K Thermal Exapansion
NiTi.R.ExpanR     = 11E-6;  % 1/K Thermal Exapansion
NiTi.M.ExpanA1    = 11E-6;  % 1/K Thermal Exapansion
NiTi.M.ExpanM     = 6.6E-6;  % 1/K Thermal Exapansion


NiTi.M.kM  = 8.6;          % Thermal Conductivity in Martensite State [W/(m*K)]
NiTi.M.kA1 = 12;         % Thermal conductivity in Austentite 1 State [W/(m*K)]
NiTi.R.kR  = 12;          % Thermal Conductivity in R-Phase State [W/(m*K)]
NiTi.R.kA2 = 12;         % Thermal conductivity in Austenite 2 State [W/(m*K)]

% PreCalculated channels
%% Martensite - Austenite
% Setup Calcs
NiTi.M.SA1        = 1/NiTi.M.EA1;  % Stress Compliance coefficient (Austenite 1)
NiTi.M.SM         = 1/NiTi.M.EM;  % Stress Compliance coefficient (Martensite)
NiTi.M.Delta_S    = NiTi.M.SM-NiTi.M.SA1;
NiTi.M.DA = NiTi.M.ExpanM - NiTi.M.ExpanA1;
NiTi.M.DC = NiTi.M.CPM - NiTi.M.CPA1;

NiTi.M.Delta_entr = -(2*NiTi.M.CA1*NiTi.M.CM*(NiTi.M.Hcur + (NiTi.M.CalS*NiTi.M.Delta_S)))/(NiTi.Dens*(NiTi.M.CA1+NiTi.M.CM));
NiTi.M.D          =((NiTi.M.CM - NiTi.M.CA1)*(NiTi.M.Hcur + (NiTi.M.CalS*NiTi.M.Delta_S)))/((NiTi.M.CM + NiTi.M.CA1)*NiTi.M.Hcur);


NiTi.M.Mdelta = (NiTi.M.MF-NiTi.M.MS);
NiTi.M.Adelta = (NiTi.M.A1S-NiTi.M.A1F);

NiTi.M.DeltaEntDensity = NiTi.M.Delta_entr*NiTi.Dens;

NiTi.M.A1 = NiTi.M.DeltaEntDensity*NiTi.M.Mdelta;
NiTi.M.A2 = NiTi.M.DeltaEntDensity*NiTi.M.Adelta;

NiTi.M.A3a=((-0.25*NiTi.M.A1)*(1+(1/(NiTi.M.N1+1))-(1/(NiTi.M.N2+1))));
NiTi.M.A3b=((0.25*NiTi.M.A2)*(1+(1/(NiTi.M.N3+1))-(1/(NiTi.M.N4+1))));
NiTi.M.A3=NiTi.M.A3a+NiTi.M.A3b;

NiTi.M.InEn = 0.5*NiTi.M.DeltaEntDensity*(NiTi.M.MS + NiTi.M.A1F) ; % Internal Energy
NiTi.M.Y0 = (0.5*NiTi.M.DeltaEntDensity*((NiTi.M.MS -  NiTi.M.A1F))- NiTi.M.A3);
NiTi.M.n=0.0001;

%Limits of the Thermodynamic Boundary
%Forward Transformation
% Martensite Volume =1
NiTi.M.Marone =(1+(0.998^(NiTi.M.N1))-((1-0.998)^(NiTi.M.N2)));
% Martensite Volume =0
NiTi.M.Marzero = (1+(0^(NiTi.M.N1))-((1-0)^(NiTi.M.N2)));
%Reverse Transformation
% Martensite Volume =1
NiTi.M.MaroneR=(1+(0.998^(NiTi.M.N3))-((1-0.998)^(NiTi.M.N4)));
% Martensite Volume =0
NiTi.M.MarzeroR=(1+(0^(NiTi.M.N3))-((1-0)^(NiTi.M.N4)));

% Martensitic Table Data
NiTi.M.DFDE_For   = zeros(1/1e-4,1);
NiTi.M.Marvol_For = zeros(1/1e-4,1);
NiTi.M.DFDE_Rev   = zeros(1/1e-4,1);
NiTi.M.Marvol_Rev = zeros(1/1e-4,1);

n = 1;
%Forward Transformation
for iMatInit = 0:1e-4:1

    DFDE = 0.5*NiTi.M.A1 *(1+iMatInit^NiTi.M.N1 -(1-iMatInit)^NiTi.M.N2) + NiTi.M.A3;

    NiTi.M.DFDE_For(n) = DFDE ;
    NiTi.M.Marvol_For(n) = iMatInit;
    n= n+1;

end

n = 1;
%Reverse Transformation
for iMatInit = 1:-1e-4:0

    DFDE = 0.5*NiTi.M.A2 *(1+iMatInit^NiTi.M.N3 -(1-iMatInit)^NiTi.M.N4) - NiTi.M.A3;

    NiTi.M.DFDE_Rev(n) = DFDE ;
    NiTi.M.Marvol_Rev(n) = iMatInit;
    n = n+1;

end
try
    NiTi.M.ForOne  = interp1(NiTi.M.Marvol_For,NiTi.M.DFDE_For,1,'nearest');
    NiTi.M.ForZero = interp1(NiTi.M.Marvol_For,NiTi.M.DFDE_For,0,'nearest');
    NiTi.M.RevOne = interp1(NiTi.M.Marvol_Rev,NiTi.M.DFDE_Rev,1,'nearest');
    NiTi.M.RevZero = interp1(NiTi.M.Marvol_Rev,NiTi.M.DFDE_Rev,0,'nearest');
catch
    NiTi.M.ForOne  = 0;
    NiTi.M.ForZero = 0;
    NiTi.M.RevOne  = 0;
    NiTi.M.RevZero = 0;
end


%% R-Phase - Austenite

% Setup Calcs
NiTi.R.SA2 = 1/NiTi.R.EA2;  % Stress Compliance coefficient (Austenite 1)
NiTi.R.SR  = 1/NiTi.R.ER;  % Stress Compliance coefficient (Martensite)
NiTi.R.Delta_S = NiTi.R.SR-NiTi.R.SA2;
NiTi.R.DA = NiTi.R.ExpanR - NiTi.R.ExpanA2;
NiTi.R.DC = NiTi.R.CPR - NiTi.R.CPA2;
NiTi.R.Delta_entr = -(2*NiTi.R.CA2*NiTi.R.CR*(NiTi.R.Hcur + (NiTi.R.CalS*NiTi.R.Delta_S)))/(NiTi.Dens*(NiTi.R.CA2+NiTi.R.CR));
NiTi.R.D =((NiTi.R.CR - NiTi.R.CA2)*(NiTi.R.Hcur + (NiTi.R.CalS*NiTi.R.Delta_S)))/((NiTi.R.CR + NiTi.R.CA2)*NiTi.R.Hcur);


NiTi.R.Rdelta = (NiTi.R.RF-NiTi.R.RS);
NiTi.R.Adelta = (NiTi.R.A2S-NiTi.R.A2F);

NiTi.R.DeltaEntDensity = NiTi.R.Delta_entr*NiTi.Dens;

NiTi.R.A1 = NiTi.R.DeltaEntDensity*NiTi.R.Rdelta;
NiTi.R.A2 = NiTi.R.DeltaEntDensity*NiTi.R.Adelta;

NiTi.R.A3a=((-0.25*NiTi.R.A1)*(1+(1/(NiTi.R.N1+1))-(1/(NiTi.R.N2+1))));
NiTi.R.A3b=((0.25*NiTi.R.A2)*(1+(1/(NiTi.R.N3+1))-(1/(NiTi.R.N4+1))));
NiTi.R.A3=NiTi.R.A3a+NiTi.R.A3b;

NiTi.R.InEn = 0.5*NiTi.R.DeltaEntDensity*(NiTi.R.RS + NiTi.R.A2F) ; % Internal Energy
NiTi.R.Y0 = (0.5*NiTi.R.DeltaEntDensity*((NiTi.R.RS -  NiTi.R.A2F))- NiTi.R.A3);


%Limits of the Thermodynamic Boundary
%Forward Transformation
% R-Phase Volume =1
NiTi.R.Rone =(1+(0.998^(NiTi.R.N1))-((1-0.998)^(NiTi.R.N2)));
% R- Phase Volume =0
NiTi.R.Rzero = (1+(0^(NiTi.R.N1))-((1-0)^(NiTi.R.N2)));
%Reverse Transformation
% Martensite Volume =1
NiTi.R.RoneR=(1+(0.998^(NiTi.R.N3))-((1-0.998)^(NiTi.R.N4)));
% Martensite Volume =0
NiTi.R.RzeroR=(1+(0^(NiTi.R.N3))-((1-0)^(NiTi.R.N4)));


% R -Phase Table Data
NiTi.R.DFDE_For   = zeros(1/1e-4,1);
NiTi.R.Rvol_For   = zeros(1/1e-4,1);
NiTi.R.DFDE_Rev   = zeros(1/1e-4,1);
NiTi.R.Rvol_Rev   = zeros(1/1e-4,1);

n = 1;
%Forward Transformation
for iMatInit = 0:1e-4:1

    DFDE = 0.5*NiTi.R.A1 *(1+iMatInit^NiTi.R.N1 -(1-iMatInit)^NiTi.R.N2) + NiTi.R.A3;

    NiTi.R.DFDE_For(n) = DFDE ;
    NiTi.R.Rvol_For(n) = iMatInit;
    n= n+1;

end

n = 1;
%Reverse Transformation
for iMatInit = 1:-1e-4:0

    DFDE = 0.5*NiTi.R.A2 *(1+iMatInit^NiTi.R.N3 -(1-iMatInit)^NiTi.R.N4) - NiTi.R.A3;

    NiTi.R.DFDE_Rev(n) = DFDE ;
    NiTi.R.Rvol_Rev(n) = iMatInit;
    n = n+1;

end
try
    NiTi.R.ForOne  = interp1(NiTi.R.Rvol_For,NiTi.R.DFDE_For,1,'nearest');
    NiTi.R.ForZero = interp1(NiTi.R.Rvol_For,NiTi.R.DFDE_For,0,'nearest');
    NiTi.R.RevOne = interp1(NiTi.R.Rvol_Rev,NiTi.R.DFDE_Rev,1,'nearest');
    NiTi.R.RevZero = interp1(NiTi.R.Rvol_Rev,NiTi.R.DFDE_Rev,0,'nearest');
catch
    NiTi.R.ForOne  = 0;
    NiTi.R.ForZero = 0;
    NiTi.R.RevOne  = 0;
    NiTi.R.RevZero = 0;
end



