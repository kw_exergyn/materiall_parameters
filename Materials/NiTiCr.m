function [P, O] = NiTiCr(P)
%% NiTiCr
P.NiTi.T0           = 273.15+P.InitTemp;              % Reference Temperautre [K]
P.NiTi.pr           = 0.3;
P.NiTi.Pr           = 0.33;                % Poisson Ratio
P.NiTi.SurfaceRough = 0.1;                 % Surface Roughness
P.NiTi.ForceInt     = P.Core.CoreForce;    % Initial Force [N]
P.NiTi.Dens         = 6450;                %  Material Density [kg/m^3]
P.NiTi.Dens_gmm3    = P.NiTi.Dens/1e9*1e3; %  Material Density [g/mm^3]
P.NiTi.LwLim        = 0.002;               % Lower Phase Limit
P.NiTi.UpLim        = 0.998;               % Upper Phase Limit
P.NiTi.RPhase       = 1;                   % Material Has R-Phase

%% NiTi Parameters

P.NiTi.M.Hcur	=	0.022;
P.NiTi.M.MS	    =	285; % [K]
P.NiTi.M.MF	    =	220; % [K]
P.NiTi.M.A1S	=	230; % [K]
P.NiTi.M.A1F	=	310; % [K]
P.NiTi.M.N1	    =	0.3; % []
P.NiTi.M.N2	    =	0.3; % []
P.NiTi.M.N3	    =	0.1; % []
P.NiTi.M.N4	    =	0.2; % []
P.NiTi.M.CA1	=	13e6; % [Pa/K]
P.NiTi.M.EA1	=	60e9; % [Pa]
P.NiTi.M.EM	    =	55e9; % [Pa]
P.NiTi.M.CPM    =   370; 
P.NiTi.M.CPA1   =   370;
P.NiTi.M.y  	=	2.5699; % []
P.NiTi.M.C_ 	=	0.44116; % []
P.NiTi.M.CM     =   P.NiTi.M.CA1;
P.NiTi.M.CalS	 =	1; % [Pa]

P.NiTi.R.Hcur	=	0.005; % [m/m]
P.NiTi.R.RS	    =	320; % [K]
P.NiTi.R.RF	    =	215; % [K]
P.NiTi.R.A2S	=	220; % [K]
P.NiTi.R.A2F	=	325; % [K]
P.NiTi.R.N1	    =	0.3; % []
P.NiTi.R.N2	    =	0.2; % []
P.NiTi.R.N3	    =	0.2; % []
P.NiTi.R.N4	    =	0.3; % []
P.NiTi.R.CA2	=	20e6; % [Pa/K]
P.NiTi.R.EA2	=	60e9; % [Pa]
P.NiTi.R.CPR    =   P.NiTi.M.CPA1;
P.NiTi.R.CPA2   =   P.NiTi.M.CPA1;
P.NiTi.R.y  	=	2.5699; % []
P.NiTi.R.C_ 	=	0.44116; % []
P.NiTi.R.ER     =   P.NiTi.M.EA1;
P.NiTi.R.CR     =   P.NiTi.R.CA2;
P.NiTi.R.CalS	 =	1; % [Pa]


P.NiTi.R.ExpanA2    = 11E-6;  % 1/K Thermal Exapansion
P.NiTi.R.ExpanR     = 11E-6;  % 1/K Thermal Exapansion
P.NiTi.M.ExpanA1    = 11E-6;  % 1/K Thermal Exapansion
P.NiTi.M.ExpanM     = 6.6E-6; % 1/K Thermal Exapansion


P.NiTi.M.kM  = 8.6;        % Thermal Conductivity in Martensite State [W/(m*K)]
P.NiTi.M.kA1 = 18;         % Thermal conductivity in Austentite 1 State [W/(m*K)]
P.NiTi.R.kR  = 18;         % Thermal Conductivity in R-Phase State [W/(m*K)]
P.NiTi.R.kA2 = 18;         % Thermal conductivity in Austenite 2 State [W/(m*K)]
    
% PreCalculated channels
%% Martensite - Austenite
% Setup Calcs
P.NiTi.M.SA1        = 1/P.NiTi.M.EA1;  % Stress Compliance coefficient (Austenite 1)
P.NiTi.M.SM         = 1/P.NiTi.M.EM;  % Stress Compliance coefficient (Martensite)
P.NiTi.M.Delta_S    = P.NiTi.M.SM-P.NiTi.M.SA1;
P.NiTi.M.DA = P.NiTi.M.ExpanM - P.NiTi.M.ExpanA1;
P.NiTi.M.DC = P.NiTi.M.CPM - P.NiTi.M.CPA1;

P.NiTi.M.Delta_entr = -(2*P.NiTi.M.CA1*P.NiTi.M.CM*(P.NiTi.M.Hcur + (P.NiTi.M.CalS*P.NiTi.M.Delta_S)))/(P.NiTi.Dens*(P.NiTi.M.CA1+P.NiTi.M.CM));
P.NiTi.M.D          =((P.NiTi.M.CM - P.NiTi.M.CA1)*(P.NiTi.M.Hcur + (P.NiTi.M.CalS*P.NiTi.M.Delta_S)))/((P.NiTi.M.CM + P.NiTi.M.CA1)*P.NiTi.M.Hcur);


P.NiTi.M.Mdelta = (P.NiTi.M.MF-P.NiTi.M.MS);
P.NiTi.M.Adelta = (P.NiTi.M.A1S-P.NiTi.M.A1F);

P.NiTi.M.DeltaEntDensity = P.NiTi.M.Delta_entr*P.NiTi.Dens;

P.NiTi.M.A1 = P.NiTi.M.DeltaEntDensity*P.NiTi.M.Mdelta;
P.NiTi.M.A2 = P.NiTi.M.DeltaEntDensity*P.NiTi.M.Adelta;

P.NiTi.M.A3a=((-0.25*P.NiTi.M.A1)*(1+(1/(P.NiTi.M.N1+1))-(1/(P.NiTi.M.N2+1))));
P.NiTi.M.A3b=((0.25*P.NiTi.M.A2)*(1+(1/(P.NiTi.M.N3+1))-(1/(P.NiTi.M.N4+1))));
P.NiTi.M.A3=P.NiTi.M.A3a+P.NiTi.M.A3b;

P.NiTi.M.InEn = 0.5*P.NiTi.M.DeltaEntDensity*(P.NiTi.M.MS + P.NiTi.M.A1F) ; % Internal Energy
P.NiTi.M.Y0 = (0.5*P.NiTi.M.DeltaEntDensity*((P.NiTi.M.MS -  P.NiTi.M.A1F))- P.NiTi.M.A3);
P.NiTi.M.n=0.0001;

%Limits of the Thermodynamic Boundary
%Forward Transformation
% Martensite Volume =1
P.NiTi.M.Marone =(1+(0.998^(P.NiTi.M.N1))-((1-0.998)^(P.NiTi.M.N2)));
% Martensite Volume =0
P.NiTi.M.Marzero = (1+(0^(P.NiTi.M.N1))-((1-0)^(P.NiTi.M.N2)));
%Reverse Transformation
% Martensite Volume =1
P.NiTi.M.MaroneR=(1+(0.998^(P.NiTi.M.N3))-((1-0.998)^(P.NiTi.M.N4)));
% Martensite Volume =0
P.NiTi.M.MarzeroR=(1+(0^(P.NiTi.M.N3))-((1-0)^(P.NiTi.M.N4)));

% Martensitic Table Data
P.NiTi.M.DFDE_For   = zeros(1/1e-4,1);
P.NiTi.M.Marvol_For = zeros(1/1e-4,1);
P.NiTi.M.DFDE_Rev   = zeros(1/1e-4,1);
P.NiTi.M.Marvol_Rev = zeros(1/1e-4,1);

n = 1;
%Forward Transformation
for iMatInit = 0:1e-4:1
    
    DFDE = 0.5*P.NiTi.M.A1 *(1+iMatInit^P.NiTi.M.N1 -(1-iMatInit)^P.NiTi.M.N2) + P.NiTi.M.A3;
    
    P.NiTi.M.DFDE_For(n) = DFDE ;
    P.NiTi.M.Marvol_For(n) = iMatInit;
    n= n+1;
    
end

n = 1;
%Reverse Transformation
for iMatInit = 1:-1e-4:0
    
    DFDE = 0.5*P.NiTi.M.A2 *(1+iMatInit^P.NiTi.M.N3 -(1-iMatInit)^P.NiTi.M.N4) - P.NiTi.M.A3;
    
    P.NiTi.M.DFDE_Rev(n) = DFDE ;
    P.NiTi.M.Marvol_Rev(n) = iMatInit;
    n = n+1;
    
end
try
P.NiTi.M.ForOne  = interp1(P.NiTi.M.Marvol_For,P.NiTi.M.DFDE_For,1,'nearest');
P.NiTi.M.ForZero = interp1(P.NiTi.M.Marvol_For,P.NiTi.M.DFDE_For,0,'nearest');
P.NiTi.M.RevOne = interp1(P.NiTi.M.Marvol_Rev,P.NiTi.M.DFDE_Rev,1,'nearest');
P.NiTi.M.RevZero = interp1(P.NiTi.M.Marvol_Rev,P.NiTi.M.DFDE_Rev,0,'nearest');
catch
P.NiTi.M.ForOne  = 0;
P.NiTi.M.ForZero = 0;
P.NiTi.M.RevOne  = 0;
P.NiTi.M.RevZero = 0;
end
    

%% R-Phase - Austenite

% Setup Calcs
P.NiTi.R.SA2 = 1/P.NiTi.R.EA2;  % Stress Compliance coefficient (Austenite 1)
P.NiTi.R.SR  = 1/P.NiTi.R.ER;  % Stress Compliance coefficient (Martensite)
P.NiTi.R.Delta_S = P.NiTi.R.SR-P.NiTi.R.SA2;
P.NiTi.R.DA = P.NiTi.R.ExpanR - P.NiTi.R.ExpanA2;
P.NiTi.R.DC = P.NiTi.R.CPR - P.NiTi.R.CPA2;
P.NiTi.R.Delta_entr = -(2*P.NiTi.R.CA2*P.NiTi.R.CR*(P.NiTi.R.Hcur + (P.NiTi.R.CalS*P.NiTi.R.Delta_S)))/(P.NiTi.Dens*(P.NiTi.R.CA2+P.NiTi.R.CR));
P.NiTi.R.D =((P.NiTi.R.CR - P.NiTi.R.CA2)*(P.NiTi.R.Hcur + (P.NiTi.R.CalS*P.NiTi.R.Delta_S)))/((P.NiTi.R.CR + P.NiTi.R.CA2)*P.NiTi.R.Hcur);


P.NiTi.R.Rdelta = (P.NiTi.R.RF-P.NiTi.R.RS);
P.NiTi.R.Adelta = (P.NiTi.R.A2S-P.NiTi.R.A2F);

P.NiTi.R.DeltaEntDensity = P.NiTi.R.Delta_entr*P.NiTi.Dens;

P.NiTi.R.A1 = P.NiTi.R.DeltaEntDensity*P.NiTi.R.Rdelta;
P.NiTi.R.A2 = P.NiTi.R.DeltaEntDensity*P.NiTi.R.Adelta;

P.NiTi.R.A3a=((-0.25*P.NiTi.R.A1)*(1+(1/(P.NiTi.R.N1+1))-(1/(P.NiTi.R.N2+1))));
P.NiTi.R.A3b=((0.25*P.NiTi.R.A2)*(1+(1/(P.NiTi.R.N3+1))-(1/(P.NiTi.R.N4+1))));
P.NiTi.R.A3=P.NiTi.R.A3a+P.NiTi.R.A3b;

P.NiTi.R.InEn = 0.5*P.NiTi.R.DeltaEntDensity*(P.NiTi.R.RS + P.NiTi.R.A2F) ; % Internal Energy
P.NiTi.R.Y0 = (0.5*P.NiTi.R.DeltaEntDensity*((P.NiTi.R.RS -  P.NiTi.R.A2F))- P.NiTi.R.A3);


%Limits of the Thermodynamic Boundary
%Forward Transformation
% R-Phase Volume =1
P.NiTi.R.Rone =(1+(0.998^(P.NiTi.R.N1))-((1-0.998)^(P.NiTi.R.N2)));
% R- Phase Volume =0
P.NiTi.R.Rzero = (1+(0^(P.NiTi.R.N1))-((1-0)^(P.NiTi.R.N2)));
%Reverse Transformation
% Martensite Volume =1
P.NiTi.R.RoneR=(1+(0.998^(P.NiTi.R.N3))-((1-0.998)^(P.NiTi.R.N4)));
% Martensite Volume =0
P.NiTi.R.RzeroR=(1+(0^(P.NiTi.R.N3))-((1-0)^(P.NiTi.R.N4)));


% R -Phase Table Data
P.NiTi.R.DFDE_For   = zeros(1/1e-4,1);
P.NiTi.R.Rvol_For   = zeros(1/1e-4,1);
P.NiTi.R.DFDE_Rev   = zeros(1/1e-4,1);
P.NiTi.R.Rvol_Rev   = zeros(1/1e-4,1);

n = 1;
%Forward Transformation
for iMatInit = 0:1e-4:1
    
    DFDE = 0.5*P.NiTi.R.A1 *(1+iMatInit^P.NiTi.R.N1 -(1-iMatInit)^P.NiTi.R.N2) + P.NiTi.R.A3;
    
    P.NiTi.R.DFDE_For(n) = DFDE ;
    P.NiTi.R.Rvol_For(n) = iMatInit;
    n= n+1;
    
end

n = 1;
%Reverse Transformation
for iMatInit = 1:-1e-4:0
    
    DFDE = 0.5*P.NiTi.R.A2 *(1+iMatInit^P.NiTi.R.N3 -(1-iMatInit)^P.NiTi.R.N4) - P.NiTi.R.A3;
    
    P.NiTi.R.DFDE_Rev(n) = DFDE ;
    P.NiTi.R.Rvol_Rev(n) = iMatInit;
    n = n+1;
    
end
try
P.NiTi.R.ForOne  = interp1(P.NiTi.R.Rvol_For,P.NiTi.R.DFDE_For,1,'nearest');
P.NiTi.R.ForZero = interp1(P.NiTi.R.Rvol_For,P.NiTi.R.DFDE_For,0,'nearest');
P.NiTi.R.RevOne = interp1(P.NiTi.R.Rvol_Rev,P.NiTi.R.DFDE_Rev,1,'nearest');
P.NiTi.R.RevZero = interp1(P.NiTi.R.Rvol_Rev,P.NiTi.R.DFDE_Rev,0,'nearest');
catch
P.NiTi.R.ForOne  = 0;
P.NiTi.R.ForZero = 0;
P.NiTi.R.RevOne  = 0;
P.NiTi.R.RevZero = 0;    
end

%% Initial Values

[O]= R_PhaseInitial(P);

